/** @format */

import {AppRegistry} from 'react-native';
import Root from './app/root';
import {name as appName} from './app.json';
import bgMessaging from './app/bgMessaging';

AppRegistry.registerComponent(appName, () => Root);
AppRegistry.registerComponent('ReactNativeFirebaseDemo', () => bootstrap);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging); 