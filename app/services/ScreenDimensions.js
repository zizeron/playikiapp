import {Dimensions, PixelRatio} from 'react-native';

const widthPercentage = widthPercent => {
  const screenWidth = Dimensions.get('window').width;
  const elemWidth = parseFloat(widthPercent);
  return PixelRatio.roundToNearestPixel(screenWidth * elemWidth / 100);
};

const heightPercentage = heightPercent => {
  const screenHeight = Dimensions.get('window').height;
  const elemHeight = parseFloat(heightPercent);
  return PixelRatio.roundToNearestPixel(screenHeight * elemHeight / 100);
};

const deviceWidth = ()=>{
  return Dimensions.get('window').width;
}

const deviceHeight = ()=>{
  return Dimensions.get('window').height;
}

const aspectRatio = ()=>{
  return deviceWidth() / deviceHeight();
}

export {
  widthPercentage,
  heightPercentage,
  deviceWidth,
  deviceHeight,
  aspectRatio
};