import firebase from 'react-native-firebase';
import Settings from '../config/settings'
import DataService from './DataService';

export default class OrderService {

    static createNewOrder(user){
        let newOrder = {
            userId     : user.uid,
            beachId    : user.beachId,
            beach      : user.beachName,
            section    : user.section,
            bikeId     : user.bikeId,
            status     : 'pending',
            timeIni    : +(new Date()),
            orderLines : []
        }

        return new Promise((resolve, reject) => {
            firebase.firestore().collection('order').add(newOrder)
            .then(result => {
                newOrder.id = result.id;
                resolve(newOrder)
            })
            .catch(error => reject(error));
        })
    }

    static setOrderUser(order, user){        
        order.userId = user.uid;
        return OrderService.saveOrderToDatabase(order);
    }

    static getUserActiveOrder(user){
        return new Promise((resolve, reject) => {
            firebase.firestore().collection("order")
                .where("userId", "==", user.uid)
                .where("status", "==", 'pending')
                .get()
                .then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        resolve(Object.assign({ id:doc.id }, doc.data() ))
                    });
                })
                .catch(function(error) {
                    reject(error);
                });
        })
    }

    static getUserActiveOrderFollowing(user){
        return new Promise((resolve, reject) => {
            firebase.firestore().collection("order")
                .where("userId", "==", user.uid)
                .where("status", "==", 'on-round')
                .get()
                .then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        resolve(Object.assign({ id:doc.id }, doc.data() ))
                    });
                })
                .catch(function(error) {
                    reject(error);
                });
        })
    }

    static setOrderStatus(order, status) {
        order.status = status;
        return OrderService.saveOrderToDatabase(order);
    }

    static setOrderTimeout(order, qty){
        order.status = 'timeout';
        // return OrderService.saveOrderToDatabase(order);
    }

    static setOrderCancel(order){
        order.status = 'cancel';
        return OrderService.saveOrderToDatabase(order);
    }

    static removeProductFromCart(userData, order, product){
        let deleteProductIndex = -1;

        order.orderLines.forEach((line, i) => {
            if(line.product.id == product.id && 
                (!line.product.selection || line.product.selection.value == product.selection.value)){

                line.qty -= 1
                if(line.qty <= 0){
                    deleteProductIndex = i;
                }
            }
        })
        
        if(deleteProductIndex >= 0){
            order.orderLines.splice(deleteProductIndex, 1)
        }
        OrderService.saveOrderToDatabase(order);

        if(product.totalPVP){
            product.products.forEach((productId) => {
                DataService.unblockStockProduct(userData.bikeId, productId, 1);
            });
            DataService.unblockStockProduct(userData.bikeId, product.selection.value, 1);
            
        } else {
            DataService.unblockStockProduct(userData.bikeId, product.id, 1);
        }
        return order;
    }
    
    static addProductToCart(userData, order, product, qty){
        let exists = false;
        const quantity = qty ? qty : 1;

        return new Promise((resolve, reject) => {
            DataService.blockStockProduct(userData.bikeId, product.id, quantity).then(
                (result) => {
                    if(result){
                        order.orderLines.forEach((line) => {            
                            if(line.product.id == product.id && 
                                (!line.product.selection || line.product.selection.value == product.selection.value)){
                
                                line.qty += quantity;
                                exists = true;
                            }
                        })
                        
                        if(!exists){
                            order.orderLines.push({
                                product: Object.assign({}, product),
                                qty: quantity
                            })
                        }
                        OrderService.saveOrderToDatabase(order);
                        resolve(order);

                    }else{
                        reject('no-stock');
                    }
                },
                (error) => {
                    console.log('​OrderService -> staticaddProductToCart -> error', error);
                }
            );
        });
    }

    static addMenuToCart(userData, order, menu, qty){
        let exists = false;
        const quantity = qty ? qty : 1;

        let promises = [];
        menu.products.forEach((product) => {
            promises.push(DataService.blockStockProduct(userData.bikeId, product, quantity));
        });
        promises.push(DataService.blockStockProduct(userData.bikeId, menu.selection.value, quantity))

        return new Promise((resolve, reject) => {
            Promise.all(promises).then(
                (responses) => {
                    const result = responses.filter((r) => r == false ).length <= 0;
                    if(result) {
                        order.orderLines.forEach((line) => {            
                            if(line.product.id == menu.id && 
                                (!line.product.selection || line.product.selection.value == menu.selection.value)){
                
                                line.qty += quantity;
                                exists = true;
                            }
                        })
                        
                        if(!exists){
                            order.orderLines.push({
                                product: Object.assign({}, menu),
                                qty: quantity
                            })
                        }
                        OrderService.saveOrderToDatabase(order);
                        resolve(order);
                    } else {
                        reject('no-stock');
                    }
                }, 
                (error) => {
                    console.log('​OrderService -> staticaddMenuToCart -> error', error);
                    reject('no-stock');
                }
            )
        });
    }

    static setOrderPayment(order, status, charge){
        order.status = status;

        if(status == 'paid'){
            order.paidAt = +(new Date());
            order.charge = charge;

            order.orderLines.forEach((ol) => {
                if(ol.product.totalPVP){
                    ol.product.products.forEach((productId) => {
                        DataService.unblockStockProduct(order.bikeId, productId, ol.qty);
                        DataService.updateBickeStock(order.bikeId, productId, ol.qty);
                    })
                    DataService.unblockStockProduct(order.bikeId, ol.product.selection.value, ol.qty);
                    DataService.updateBickeStock(order.bikeId, ol.product.selection.value, ol.qty);
                }else{
                    DataService.unblockStockProduct(order.bikeId, ol.product.id, ol.qty);
                    DataService.updateBickeStock(order.bikeId, ol.product.id, ol.qty);
                }
            })
        }

        return OrderService.saveOrderToDatabase(order);
    }

    static saveOrderToDatabase(order){
        return new Promise((resolve, reject) => {
            firebase.firestore().collection("order")
                .doc(order.id)
                .set(order)
                .then(() => {
                    resolve(true);
                })
                .catch((error) => {
                    reject(error);
                });
        })
    }

    static getOrderTotal(order, shipping){
        let total = 0;
        order.orderLines.forEach((line) => {            
            total += parseFloat(line.qty * (line.product.totalPVP ? line.product.totalPVP : line.product.PVP));
        });
        
        if(shipping && shipping>0){
            total += parseFloat(shipping);
        }

        const { coupon } = order;
        if(coupon){
            if(coupon.amount_off){
                total = Math.max(0, parseFloat(total - (coupon.amount_off / 100)));
            }
            if(coupon.percent_off){
                total = Math.max(0, parseFloat(total - (total * coupon.percent_off / 100)));
            }
        }
        return parseFloat(total).toFixed(2);
    }

    static getOrderTimeLeft(order){
        const now = +(new Date());
        let secondsLeft = parseInt((Settings.orderMaxTimeAvailable - (now - order.timeIni)) / 1000);
        
        return {
            minsLeft: Math.floor(secondsLeft / 60),
            secondsLeft: Math.floor(secondsLeft % 60)
        }
    }

    static getOrderItems(order){
        return order ? order.orderLines.reduce((acc, ol) => {return acc + ol.qty},0) : 0;
    }
}