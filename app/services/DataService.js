import firebase from 'react-native-firebase';

export default class DataService {
    static getCollectionRealtime(collection, filter = [], callback) {
        let collRef = firebase.firestore().collection(collection);
    
        filter.forEach((f) => {
          collRef = collRef.where(f.field, f.cond, f.value);
        });
    
        return collRef.onSnapshot((querySnapshot) => {
          let collectionResult = [];
    
          querySnapshot.forEach((d) => {
            const newD = Object.assign({ id: d.id }, d.data());
            collectionResult.push(newD);
          });
    
          callback(collectionResult);
        });
    }

    static getCollection(collection, filter = []){
        return new Promise((resolve, reject) => {
            let collectionResult = [];
            let collRef = firebase.firestore().collection(collection);

            filter.forEach((f) => {
                collRef = collRef.where(f.field, f.cond, f.value)
            })

            collRef.get()
                .then(docs => {
                    docs.forEach((d) => {
                        const newD = Object.assign({ id:d.id }, d.data() );
                        collectionResult.push(newD);
                    })
                    resolve(collectionResult);
                })
                .catch(error => reject(error))
        })
    }

    static getObject(collection, id){
        return new Promise((resolve, reject) => {
            firebase.firestore().collection(collection).doc(id).get()
                .then(doc => {
                    if(doc.exists){
                        const docData = Object.assign(doc.data(),{id: doc.id})
                        resolve(docData);
                    }else{
                        reject('no-object');
                    }
                    
                })
                .catch(error => reject(error))
        })
    }

    static saveObject(collection, id, object){
        return new Promise((resolve, reject) => {
            firebase.firestore().collection(collection).doc(id)
                .set(object, {merge: true})
                .then(() => {
                    resolve(true);
                })
                .catch(error => reject(error))
        })
    }

    static getBikeStock(bikeId){
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('bike').doc(bikeId).get()
                .then(doc => {
                    if(doc.exists){
                        const stock = doc.data().stock;
                        let stockArray = [];

                        Object.keys(stock).forEach((prodId) => {
                            stockArray.push({
                                id: prodId,
                                name: stock[prodId].name,
                                quantity: stock[prodId].quantity,
                                blocked: stock[prodId].blocked
                            })
                        })
                        resolve(stockArray);
                    }else{
                        reject('no-object');
                    }
                    
                })
                .catch(error => reject(error))
        })
    }
    static getUserDeliver(userData){
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('deliver')
                .where('beachId', '==', userData.beachId)
                .where('bikeId', '==', userData.bikeId)
                .get()
                .then(docs => {
                    let deliver = null;

                    docs.forEach((d) => {
                        const del = d.data();
                        if(del.sections.indexOf(userData.section) >= 0 && del.status != 'paused'){
                            deliver = Object.assign(d.data(),{id: d.id})
                        }
                    });

                    if(deliver){
                        resolve(deliver);
                    }else{
                        reject('no-object');
                    }
                })
                .catch(error => reject(error))
        })
    }

    static blockStockProduct(bikeId, productId, quantity){        
        return new Promise((resolve, reject) => {
            DataService.getObject('bike', bikeId).then(
                (bike) => {
                    const productStockBlocked = parseInt(bike.stock[productId].blocked || 0);
                    const productStockBlockedTotal = productStockBlocked + quantity;
    
                    if(productStockBlockedTotal <= bike.stock[productId].quantity){
                        firebase.firestore()
                            .collection('bike')
                            .doc(bikeId)
                            .update({
                                ['stock.'+productId+'.blocked'] : productStockBlockedTotal
                            });
                        resolve(true);
                    }else{
                        resolve(false);
                    }
                },
                (error) => {
                    console.log('unblockStockProduct -> error', error);
                }
            ) 
        })     
    }

    static unblockStockProduct(bikeId, productId, quantity = 1){
        DataService.getObject('bike', bikeId).then(
            (bike) => {
                const productStock = parseInt(bike.stock[productId].blocked || 0);
                let blockedTotal = productStock - quantity;

                if(blockedTotal < 0){
                    blockedTotal = 0;
                }

                firebase.firestore()
                    .collection('bike')
                    .doc(bikeId)
                    .update({
                        ['stock.'+productId+'.blocked'] : blockedTotal
                    })
            },
            (error) => {
                console.log('unblockStockProduct -> error', error);

            }
        )        
    }

    static updateBickeStock(bikeId, productId, quantity = 1){
        DataService.getObject('bike', bikeId).then(
            (bike) => {
                const productStock = parseInt(bike.stock[productId].quantity || 0);
                const productTotal = productStock - quantity;

                if(productTotal < 0){
                    productTotal = 0;
                }
                firebase.firestore()
                    .collection('bike')
                    .doc(bikeId)
                    .update({
                        ['stock.'+productId+'.quantity'] : productTotal
                    })
            },
            (error) => {
                console.log('unblockStockProduct -> error', error);

            }
        )        
    }

    static getProducts(userData, filter){
        return new Promise((resolve, reject) => {
            DataService.getCollection('product', filter).then(
                (products) => {
                    // console.log('​DataService -> staticgetProducts -> products', products);
                    DataService.getBikeStock(userData.bikeId).then(
                        (stock) => {
                        // console.log('​DataService -> staticgetProducts -> stock', stock);
                            let stockProducts = [];

                            products.forEach((prod) => {
                                stock.forEach((stock) => {
                                    if(prod.id == stock.id){
                                        prod.qty = stock.quantity - (stock.blocked || 0);
                                        if(prod.qty > 0){
                                            stockProducts.push(prod);
                                        }
                                    }
                                })
                            })
                            // console.log('​DataService -> staticgetProducts -> products', products);
                            resolve(stockProducts);
                        },
                        (error) => {
                            reject('No hay stock disponible en estos momentos')
                        }
                    )
                },
                (error) => {
                    reject('No hay productos disponibles en estos momentos')
                }
            )
        })
    }

    static getProduct(userData, productId){
        return new Promise((resolve, reject) => {
            DataService.getObject('product', productId).then(
                (product) => {
                    DataService.getBikeStock(userData.bikeId).then(
                        (stock) => {
                            stock.forEach((stock) => {
                                if(product.id == stock.id){
                                    product.qty = stock.quantity - (stock.blocked || 0);
                                }
                            })
                            
                            resolve(product);
                        },
                        (error) => {
                            reject('No hay stock disponible en estos momentos')
                        }
                    )
                },
                (error) => {
                    reject('No hay productos disponibles en estos momentos')
                }
            )
        })
    }

    static getMenuProducts(userData, menuId){
        return new Promise((resolve, reject) => {
            let menuProducts = {
                products: [],
                selectable: []
            }
            DataService.getObject('menu', menuId).then(
                (menu) => {
                    DataService.getBikeStock(userData.bikeId).then(
                        (stock) => {
                            menu.products.forEach((menuProdId) => {
                                stock.forEach((stock) => {
                                    if(menuProdId == stock.id){
                                        menuProducts.products.push({
                                            id: menuProdId,
                                            name: stock.name,
                                            qty: stock.quantity - (stock.blocked || 0)
                                        });
                                    }
                                })
                            })
                            
                            menu.onSelect.forEach((menuProdId) => {
                                stock.forEach((stock) => {
                                    if(menuProdId == stock.id){
                                        menuProducts.selectable.push({
                                            id: menuProdId,
                                            name: stock.name,
                                            qty: stock.quantity - (stock.blocked || 0)
                                        });
                                    }
                                })
                            })
                            
                            resolve(menuProducts);
                        },
                        (error) => {
                            reject('No hay stock disponible en estos momentos')
                        }
                    )
                },
                (error) => {
                    console.log('​DataService -> staticgetMenuProducts -> error', error);
                    reject('No hay productos disponibles en estos momentos')
                }
            )
        })
    }
    
    static getDistance(userLocation, deliverLocation){
        var R = 6378.137; // Radius of earth in KM
        var dLat = deliverLocation.latitude * Math.PI / 180 - userLocation.latitude * Math.PI / 180;
        var dLon = deliverLocation.longitude * Math.PI / 180 - userLocation.longitude * Math.PI / 180;
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(userLocation.latitude * Math.PI / 180) * Math.cos(deliverLocation.latitude * Math.PI / 180) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        return d * 1000; // meters
    }
}