import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
    if(_navigator){
        _navigator.dispatch(
            NavigationActions.navigate({
              routeName,
              params,
            })
          );      
    }else{
        console.warn('No navigator set');
    }
  
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
};