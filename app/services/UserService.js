import firebase from 'react-native-firebase';
import DataService from './DataService';
import { widthPercentage as wp, heightPercentage as hp } from './ScreenDimensions';

export default class UserService {
    static saveUserInfo(userData){        
        if(userData.uid){
            firebase.firestore().collection('users').doc(userData.uid).set(userData,{merge:true})
                .then((result) => {
                    const customer = {
                        email: userData.email,
                        id   : userData.uid
                    }
                    DataService.saveObject('stripe_customers', userData.uid, customer);
                })
                .catch(error => console.log('SAVE USER INFO error: ', error))
        }
    }

    static calculatePlayId(userData){
        if(userData && userData.name){
            return UserService.getPlayID(userData.name);
        } else if(userData && userData.email) {
            return UserService.getPlayID(userData.email);
        }
        return UserService.getPlayID("UNKP");
    }

    static getPlayID(userName){
        const firstWord = userName.split(" ").shift().substring(0,4).toUpperCase();
        const randomNumbers = Math.round(Math.random()*100);

        return firstWord+randomNumbers;
    }

    static addPaymentSource(userData, sourceId, source){
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('stripe_customers')
                .doc(userData.uid)
                .collection('sources')
                .doc(sourceId)
                .set(source)
                .then(() => {
                    resolve(true);
                })
                .catch(error => reject(error))
        })
    }

    static getPaymentSources(userData){
        return new Promise((resolve, reject) => {
            let sources = [];

            firebase.firestore()
                .collection('stripe_customers')
                .doc(userData.uid)
                .collection('sources')
                .get()
                .then(docs => {
                    docs.forEach((d) => {
                        const newD = Object.assign({ id:d.id }, d.data() );
                        sources.push(newD);
                    })
                    resolve(sources);
                })
                .catch(error => reject(error))
        })
    }

    static addPaymentCharge(userData, chargeId, charge){
        return new Promise((resolve, reject) => {
            console.log('​UserService -> staticaddPaymentCharge -> userData.uid', userData.uid);
            console.log('​UserService -> staticaddPaymentCharge -> chargeId', chargeId);
            console.log('​UserService -> staticaddPaymentCharge -> charge', charge);
            firebase.firestore()
                .collection('stripe_customers')
                .doc(userData.uid)
                .collection('charges')
                .doc(chargeId)
                .set(charge)
                .then(() => {
                    resolve(true);
                })
                .catch(error => reject(error))
        })
    }

    static getPaymentCharge(userData, idCharge){
        return new Promise((resolve, reject) => {
            let sources = [];

            firebase.firestore()
                .collection('stripe_customers')
                .doc(userData.uid)
                .collection('charges')
                .doc(idCharge)
                .get()
                .then(doc => {
                    if(doc.exists){
                        const docData = Object.assign(doc.data(),{id: doc.id})
                        resolve(docData);
                    }else{
                        reject('no-object');
                    }
                    
                })
                .catch(error => reject(error))
        })
    }

    static getUserPicture(userData){
        const userDefault = require('../assets/images/other/default-user.jpg');
        let userPicture = userDefault;

        if(userData){
            const { picture } = userData;

            if(picture){
                if(picture.indexOf('facebook.com') >= 0){
                    userPicture = {uri: `${picture}?height=${parseInt(hp(50))}`};
                } else {
                    userPicture = {uri : picture};
                }
            }
        }

        return userPicture;
    }
}