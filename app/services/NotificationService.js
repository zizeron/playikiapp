import firebase from 'react-native-firebase';

const messages = {
    timeout   : 'Tu pedido ha caducado',
    delivered : 'Tu pedido ha sido entregado',
    paid      : 'Tu pedido se ha realizado con éxito'
}

export default class NotificationService {
    static insertNotification(userData, type, order = null){
        if(!userData) return;

        const now = +(new Date());

        return new Promise((resolve, reject) => {
            firebase
                .firestore()
                .collection('users')
                .doc(userData.uid)
                .collection('notification')
                .add({
                    message: messages[type],
                    date: now,
                    order: order
                })
                .then(() => {
                    resolve(true);
                })
                .catch(error => reject(error))
        })
    }
}