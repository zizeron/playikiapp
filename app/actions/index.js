
const setUserData = (userData) => {
    return (dispatch) => {
        return dispatch({type: 'SET_USER_DATA', userData:userData});
    };
}

const disableMenuGesture = (menuDisable) => {
    return (dispatch) => {
        return dispatch({type: 'DISABLE_MENU_GESTURE', menuDisable});
    };
}

const setMenuState = (menuOpen) => {
    return (dispatch) => {
        return dispatch({type: 'SET_MENU_STATE', menuOpen});
    };
}

const setOrder = (order) => {
    return (dispatch) => {
        return dispatch({type: 'SET_ORDER', order});
    };
}


const setOrderFollowing = (order) => {
    return (dispatch) => {
        return dispatch({type: 'SET_ORDER_FOLLOWING', order});
    };
}

const setSettings = (settings) => {
    return (dispatch) => {
        return dispatch({type: 'SET_SETTINGS', settings});
    };
}


export {
    setUserData,
    disableMenuGesture,
    setMenuState,
    setOrder,
    setOrderFollowing,
    setSettings
}
