import React, { Component } from 'react';
import { Provider } from 'react-redux';

import store from './store'; //Import the store
import App from './index' //Import the component file

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}