import React from 'react';
import { Picker } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp} from '../../services/ScreenDimensions';

import Constants from '../../config/styles';


export default class CustomPicker extends React.Component {
    constructor(props){
        super(props);

        this.defaultStyles = {
            picker:{
                backgroundColor: Constants.colors.gray,
                width: wp(70),
                height: hp(6),
                marginLeft: wp(15),
                color: Constants.colors.darkGray,
                padding: 0,
                borderRadius: 100
            },
            pickerItem:{
                height: hp(8),
                marginTop:-1*hp(1),
                fontSize: hp(Constants.fontSize.l),
            }
        }
    }

    render() {
        const propsStyles = {
            picker: {},
            pickerItem: {}
        };

        //INPUT STYLES
        // if(this.props.height) propsStyles.input.height = hp(this.props.height);
        // if(this.props.width) propsStyles.input.width = wp(this.props.width);

        // if(this.props.marginLeft){
        //     propsStyles.input.marginLeft = wp(this.props.marginLeft);

        // }else if(this.props.width){ //Si no le paso el margin left pero modifico el width, lo recalcula centrado
        //     propsStyles.input.marginLeft = wp((100 - this.props.width) / 2);
        // }

        //TEXT STYLES
        // if(this.props.textColor) propsStyles.input.color = this.props.textColor;
        // if(this.props.textSize) propsStyles.input.fontSize = hp(this.props.textSize);

        const pickerStyles = Object.assign(this.defaultStyles.picker, propsStyles.picker);
        const pickerItemStyles = Object.assign(this.defaultStyles.pickerItem, propsStyles.pickerItem);

        return (
            <Picker
                selectedValue={this.props.selectedValue}
                style={pickerStyles}
                itemStyle={pickerItemStyles}
                onValueChange={(itemValue, itemIndex) => this.props.onPickerChange(this.props.field,itemValue)}>
                    {this.props.data.map(p => 
                        <Picker.Item key={p.value} value={p.value} label={p.label} />
                    )}
            </Picker>
        );
    }
}

