import React from 'react';
import {Modal, TouchableWithoutFeedback, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';
import Constants from '../../config/styles';
import Button from '../../components/Buttons/Button';

const notAvailable = require('../../assets/images/icons/productAdded.png');
const userDefault = require('../../assets/images/other/default-user.jpg');

export default function DeliverReady(props){
    const { repartidor } = props
    const {image} = repartidor;

    let userPicture = image ? {uri: image} : userDefault;

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={props.showModal}
            onRequestClose={() => {props.onCloseModal(false)}}
            presentationStyle="overFullScreen"
        >
            <View style={styles.modalMain}>
                <TouchableWithoutFeedback onPress={() => {props.onCloseModal(false)}} >
                    <View style={styles.modalBG}></View>
                </TouchableWithoutFeedback>
                <View style={styles.modalContent}>
                    <Image style={styles.image} source={userPicture}/>
                    <Text style={styles.title}>{repartidor.deliver}</Text>
                    <Text style={styles.desc}>Acaba de llegar con tu pedido</Text>
                    <Text style={styles.play}>Tu PLYIKID es</Text>
                    <Text style={styles.playid}>{repartidor.playikid}</Text>
                    <Button 
                        type="pink"
                        onButtonPress={()=>{props.onCloseModal(true)}} 
                        text={'RECIBIDO'} 
                        marginLeft={0}
                        marginBottom={hp(0.5)}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalMain:{
        // top            : 0,
        // left           : 0,
        // position       : 'absolute',
        // width          : wp(100),
        // height         : hp(100),
        // flex           : 1,
        // justifyContent : 'center',
        // alignItems     : 'center',
        // backgroundColor: 'transparent'
    },
    modalBG:{
        top            : 0,
        left           : 0,
        position       : 'absolute',
        width           : wp(100),
        height          : hp(100),
        backgroundColor : Constants.colorsRGB.darkGray
    },
    modalContent:{
        // position: 'absolute',
        width: wp(80),
        marginTop: hp(15),
        marginLeft: wp(10),
        backgroundColor: Constants.colors.white,
        borderRadius: 10,
        justifyContent : 'space-between',
        alignItems     : 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingTop: wp(10),
        paddingBottom: wp(10),
    },
    image:{
        width: wp(40),
        height: wp(40),
        resizeMode: 'contain',
        marginBottom: hp(2)
    },
    title:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.pink,
        textAlign: 'center',
        marginBottom: hp(1)
    },
    desc:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(3)
    },
    play:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(1)
    },
    playid:{
        fontSize: hp(Constants.fontSize.xl),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginBottom: hp(2)
    }

});