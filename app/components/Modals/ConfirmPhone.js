import React from 'react';
import {Modal, View, StyleSheet, Text, ActivityIndicator } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';

import Constants from '../../config/styles';
import Button from '../../components/Buttons/Button';
import Input from '../../components/Input/Input'


export default class ConfirmPhone extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            code          : '',
            formErrorText : '',
            loading       : false
        }
        
        this.confirmCode = this.confirmCode.bind(this);
    }
  
    confirmCode(){
        const { confirmationResult } = this.props;
        if(!confirmationResult){
          console.log("no confirmationResult");
          return
        }
  
        if(!this.state.code || this.state.code == ''){
          this.setState({loading: false, formErrorText: 'Código no válido'});
          return;
        }
    
        this.setState({loading: true});
        confirmationResult.confirm(this.state.code).then((result) => {
          console.log("Código verificado",result);
          const userId = result._user.uid;
          this.props.onCloseModal(true, userId)
  
        }).catch((error) => {
          console.log('BAD CODE? error: ', error);
          this.setState({loading: false, formErrorText: 'Código no válido'});
          
        });
    }

    render(){
        const { showModal } = this.props;
        const { code, loading, formErrorText } = this.state;

        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={showModal}
                onRequestClose={() => {this.props.onCloseModal(false)}}
                presentationStyle="overFullScreen"
            >
                <View style={styles.modalBG}></View>
                <View style={styles.modalContent}>
                    <Text style={styles.desc}>Introduce el código de confirmación recibido por SMS</Text>
                    <View style={styles.formRowCode}>
                        <Input 
                            value={code} 
                            onInputChange={(field, value)=>{this.setState({code: value})}} 
                            field="code"
                            width={60}
                            marginLeft={0}
                            placeholder="Código"
                        />
                    </View>
                    <View style={styles.formRow}>
                        {formErrorText != '' && <Text style={formErrorText}>{formErrorText}</Text>}
                    </View>
                    <View style={styles.formRow}>
                        {loading 
                            ?  <ActivityIndicator size="small" color={Constants.colors.pink}/>
                            : <Button
                                type="pink"
                                onButtonPress={this.confirmCode} 
                                text={'CONFIRMAR'} 
                                width={60}
                                marginLeft={0}
                            />
                        }
                    </View>
                    <View style={styles.formRow}>
                        <Button
                            type="gray"
                            onButtonPress={() => {this.props.onCloseModal(false)}}
                            text={'VOLVER'} 
                            width={60}
                            marginLeft={0}
                        />
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modalBG:{
        top            : 0,
        left           : 0,
        position       : 'absolute',
        width           : wp(100),
        height          : hp(100),
        backgroundColor : Constants.colorsRGB.darkGray
    },
    modalContent:{
        // position: 'absolute',
        width: wp(80),
        marginTop: hp(15),
        marginLeft: wp(10),
        backgroundColor: Constants.colors.white,
        borderRadius: 10,
        justifyContent : 'space-between',
        alignItems     : 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingTop: wp(10),
        paddingBottom: wp(10),
    },
    image:{
        width: wp(40),
        height: wp(40),
        resizeMode: 'contain',
        marginBottom: hp(2)
    },
    title:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.pink,
        textAlign: 'center',
        marginBottom: hp(1)
    },
    desc:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(3)
    },
    play:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(1)
    },
    playid:{
        fontSize: hp(Constants.fontSize.xl),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginBottom: hp(2)
    },
    formRowCode:{
        marginBottom: hp(3)
    },
    formRow:{
        marginBottom: hp(2)
    },
    formErrorText:{
        color: Constants.colors.pink,
        textAlign:'center'
    }
});