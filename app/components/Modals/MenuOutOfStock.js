import React from 'react';
import { Modal, TouchableWithoutFeedback, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp, aspectRatio } from '../../services/ScreenDimensions';
import Constants from '../../config/styles';
import Button from '../../components/Buttons/Button';

const notAvailable = require('../../assets/images/icons/notAvailable.png');

export default function MenuOutOfStock(props){
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={props.showModal}
            onRequestClose={() => {props.onCloseModal()}}
            presentationStyle="overFullScreen"
        >
            <View style={styles.modalMain}>
                <TouchableWithoutFeedback onPress={() => {props.onCloseModal()}} >
                    <View style={styles.modalBG}></View>
                </TouchableWithoutFeedback>
                <View style={styles.modalContent}>
                    <Image style={styles.image} source={notAvailable}/>
                    <Text style={styles.title}>Menú fuera de stock</Text>
                    <Text style={styles.desc}>De momento no tenemos disponible alguno de los productos que conforman este menú. Vuelve más tarde para encontrarlo.</Text>
                    <Button 
                        type="pink"
                        onButtonPress={props.onCloseModal} 
                        text={'Aceptar'} 
                        marginLeft={0}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalMain:{
        // top            : 0,
        // left           : 0,
        // position       : 'absolute',
        // width          : wp(100),
        // height         : hp(100),
        // flex           : 1,
        // justifyContent : 'center',
        // alignItems     : 'center',
        // backgroundColor: 'transparent'
    },
    modalBG:{
        top            : 0,
        left           : 0,
        position       : 'absolute',
        width           : wp(100),
        height          : hp(100),
        backgroundColor : Constants.colorsRGB.darkGray
    },
    modalContent:{
        // position: 'absolute',
        width: wp(80),
        marginTop: hp(15),
        marginLeft: wp(10),
        backgroundColor: Constants.colors.white,
        borderRadius: 10,
        justifyContent : 'space-between',
        alignItems     : 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingTop: wp(10),
        paddingBottom: wp(10),
    },
    image:{
        width: wp(40),
        height: wp(40),
        resizeMode: 'contain',
        marginBottom: hp(2)
    },
    title:{
        fontSize: hp(Constants.fontSize.xl),
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginBottom: hp(2)
    },
    desc:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(2)
    },
});