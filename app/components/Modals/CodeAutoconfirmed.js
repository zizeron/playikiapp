import React from 'react';
import {Modal, View, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';

import Constants from '../../config/styles';
import Button from '../../components/Buttons/Button';

export default function CodeAutoconfirmed(props) {
    const {showModal, onCloseModal} = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={showModal}
            onRequestClose={onCloseModal}
            presentationStyle="overFullScreen"
        >
            <View style={styles.modalBG}></View>
            <View style={styles.modalContent}>
                <Text style={styles.desc}>Código validado correctamente</Text>
                <View style={styles.formRow}>
                    <Button
                        type="pink"
                        onButtonPress={onCloseModal} 
                        text={'¡OK!'} 
                        width={60}
                        marginLeft={0}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBG:{
        top            : 0,
        left           : 0,
        position       : 'absolute',
        width           : wp(100),
        height          : hp(100),
        backgroundColor : Constants.colorsRGB.darkGray
    },
    modalContent:{
        width: wp(80),
        marginTop: hp(15),
        marginLeft: wp(10),
        backgroundColor: Constants.colors.white,
        borderRadius: 10,
        justifyContent : 'space-between',
        alignItems     : 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingTop: wp(10),
        paddingBottom: wp(10),
    },
    desc:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(3)
    },
    formRow:{
        marginBottom: hp(2)
    }
});