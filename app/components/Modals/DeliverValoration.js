import React from 'react';
import {Modal, TouchableWithoutFeedback, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';
import Constants from '../../config/styles';
import Button from '../../components/Buttons/Button';

const valoracionImg = require('../../assets/images/icons/valoracion.png');
const star = require('../../assets/images/icons/star.png');

export default function DeliverReady(props){

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={props.showModal}
            onRequestClose={() => {props.onCloseModal()}}
            presentationStyle="overFullScreen"
        >
            <View style={styles.modalMain}>
                <TouchableWithoutFeedback onPress={() => {props.onCloseModal(0)}} >
                    <View style={styles.modalBG}></View>
                </TouchableWithoutFeedback>
                <View style={styles.modalContent}>
                    <Image style={styles.image} source={valoracionImg}/>
                    <Text style={styles.title}>Tu pedido ha sido entregado</Text>
                    <Text style={styles.play}>Por favor, valora tu compra con playiki</Text>
                    <View style={styles.stars}>
                        <TouchableWithoutFeedback onPress={() => {props.onCloseModal(1)}} >
                            <Image style={styles.starsItem} source={star}/>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => {props.onCloseModal(2)}} >
                            <Image style={styles.starsItem} source={star}/>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => {props.onCloseModal(3)}} >
                            <Image style={styles.starsItem} source={star}/>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => {props.onCloseModal(4)}} >
                            <Image style={styles.starsItem} source={star}/>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => {props.onCloseModal(5)}} >
                            <Image style={styles.starsItem} source={star}/>
                        </TouchableWithoutFeedback>
                    </View>
                    
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalMain:{
        // top            : 0,
        // left           : 0,
        // position       : 'absolute',
        // width          : wp(100),
        // height         : hp(100),
        // flex           : 1,
        // justifyContent : 'center',
        // alignItems     : 'center',
        // backgroundColor: 'transparent'
    },
    modalBG:{
        top            : 0,
        left           : 0,
        position       : 'absolute',
        width           : wp(100),
        height          : hp(100),
        backgroundColor : Constants.colorsRGB.darkGray
    },
    modalContent:{
        // position: 'absolute',
        width: wp(80),
        marginTop: hp(15),
        marginLeft: wp(10),
        backgroundColor: Constants.colors.white,
        borderRadius: 10,
        justifyContent : 'space-between',
        alignItems     : 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingTop: wp(10),
        paddingBottom: wp(10),
    },
    image:{
        width: wp(40),
        height: wp(40),
        resizeMode: 'contain',
        marginBottom: hp(2)
    },
    title:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.pink,
        textAlign: 'center',
        marginBottom: hp(1)
    },
    desc:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(3)
    },
    play:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        marginBottom: hp(3)
    },
    stars:{
        width: wp(80),
        paddingHorizontal: wp(10),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    starsItem:{
        width: wp(8),
        height: wp(12),
        resizeMode: 'contain',
        overflow:'hidden'
    }

});