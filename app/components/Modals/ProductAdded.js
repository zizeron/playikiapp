import React from 'react';
import {Modal, TouchableWithoutFeedback, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';
import Constants from '../../config/styles';
import Button from '../../components/Buttons/Button';

const notAvailable = require('../../assets/images/icons/productAdded.png');

export default function ProductAddedModal(props){
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={props.showModal}
            onRequestClose={() => {props.onCloseModal()}}
            presentationStyle="overFullScreen"
        >
            <View style={styles.modalMain}>
                <TouchableWithoutFeedback onPress={() => {props.onCloseModal()}} >
                    <View style={styles.modalBG}></View>
                </TouchableWithoutFeedback>
                <View style={styles.modalContent}>
                    <Image style={styles.image} source={notAvailable}/>
                    <Text style={styles.title}>Producto añadido</Text>
                    <Text style={styles.desc}>Recuerda que puedes añadir productos a tu cesta durante los próximos diez minutos para hacer tu pedido</Text>
                    <Button 
                        type="pink"
                        onButtonPress={props.onGoToOrder} 
                        text={'Pedir ahora'} 
                        marginLeft={0}
                        marginBottom={hp(0.5)}
                    />
                    <Button 
                        type="yellow"
                        onButtonPress={() => {props.onCloseModal()}} 
                        text={'Continuar comprando'} 
                        marginLeft={0}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalMain:{
        // top            : 0,
        // left           : 0,
        // position       : 'absolute',
        // width          : wp(100),
        // height         : hp(100),
        // flex           : 1,
        // justifyContent : 'center',
        // alignItems     : 'center',
        // backgroundColor: 'transparent'
    },
    modalBG:{
        top            : 0,
        left           : 0,
        position       : 'absolute',
        width           : wp(100),
        height          : hp(100),
        backgroundColor : Constants.colorsRGB.darkGray
    },
    modalContent:{
        // position: 'absolute',
        width: wp(80),
        marginTop: hp(15),
        marginLeft: wp(10),
        backgroundColor: Constants.colors.white,
        borderRadius: 10,
        justifyContent : 'space-between',
        alignItems     : 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingTop: wp(10),
        paddingBottom: wp(10),
    },
    image:{
        width: wp(40),
        height: wp(40),
        resizeMode: 'contain',
        marginBottom: hp(2)
    },
    title:{
        fontSize: hp(Constants.fontSize.xl),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginBottom: hp(2)
    },
    desc:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginBottom: hp(2)
    },
});