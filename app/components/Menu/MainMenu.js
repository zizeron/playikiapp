import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { View, Text, StyleSheet,Image, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';
import NavigationService from '../../services/NavigationService';
import UserService from '../../services/UserService';

import Constants from '../../config/styles';
import { setMenuState } from '../../actions'; 

import Button from '../Buttons/Button';

const homeImage = require('../../assets/images/other/home_logo.png');
const icons = {
    location      : require('../../assets/images/icons/location.png'),
    home          : require('../../assets/images/icons/home.png'),
    list          : require('../../assets/images/icons/list.png'),
    notifications : require('../../assets/images/icons/notifications.png'),
    help          : require('../../assets/images/icons/help.png'),
    exit          : require('../../assets/images/icons/exit.png')
}


class MainMenu extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            current: 'Products'
        }

        this.onSetMenuState = bindActionCreators(setMenuState, props.dispatch);
        this.signOut = this.signOut.bind(this);
        this.followOrder = this.followOrder.bind(this);
    }

    signOut(){
        const { userData } = this.props;

        if(userData && userData.uid){
            firebase.auth().signOut()
        }else{
            NavigationService.navigate('Home');
        }
        this.onSetMenuState(false);
    }

    goTo(section, login=false){
        const { userData } = this.props;
        this.onSetMenuState(false);

        if (userData.uid || !login) {
            NavigationService.navigate(section);
        } else {
            NavigationService.navigate('Login', {message: 'Para acceder a esta sección debes identificarte', prevScreen: section});
        }
    }

    followOrder(order){
        this.onSetMenuState(false);
        NavigationService.navigate('FollowOrder',{order})
    }

    _getMenuItem(icon, title, onPressAction, iconStyles, itemStyles, link){
        const icStyles = iconStyles ? iconStyles : styles.menuItemIcon;
        const itStyles = itemStyles ? itemStyles : styles.menuItem;

        return (
            <TouchableOpacity onPress={onPressAction}>
                <View style={itStyles}>
                        {icon && <Image style={icStyles} source={icon}/>}
                        <View style={styles.locationTexts}>
                            <Text style={styles.menuItemText}>{title}</Text>
                            {link && <Text style={styles.menuItemLinkText}>Cambiar</Text>}
                        </View>
                </View>
            </TouchableOpacity>
        )
    }

    _getLegalMenuItem(title, onPressAction){

        return (
            <TouchableOpacity onPress={onPressAction}>
                <View style={styles.menuItem}>
                    <View style={styles.locationTexts}>
                        <Text style={styles.menuLegalItemText}>{title}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const { userData, order, orderFollow } = this.props;
        if(!userData) return null;
        
        const userPicture = UserService.getUserPicture(userData);

        return (
            <View style={styles.menuBg}>
                <Image style={styles.logo} source={homeImage}/>
                <View style={styles.menuOptions}>
                    {this._getMenuItem(
                        icons.location,
                        userData.beachName || '',
                        ()=>{this.goTo('SetLocation')},
                        styles.menuItemIcon,
                        styles.locationItem,
                        true)
                    }
                    {this._getMenuItem(
                        icons.home,
                        'Productos',
                        ()=>{this.goTo('Products')})
                    }
                    {this._getMenuItem(
                        icons.list,
                        'Mis pedidos',
                        ()=>{this.goTo('OrderList', true)})
                    }
                    {this._getMenuItem(
                        icons.notifications,
                        'Notificaciones',
                        ()=>{this.goTo('Notifications', true)})
                    }
                    {this._getMenuItem(
                        userPicture,
                        'Configuración',
                        ()=>{this.goTo('Configuration', true)},
                        styles.menuItemIconAvatar)
                    }
                    {this._getMenuItem(
                        icons.help,
                        'Ayuda',
                        ()=>{this.goTo('Faqs')})
                    }
                    {userData.uid && this._getMenuItem(
                        icons.exit,
                        'Salir',
                        this.signOut)
                    }
                    <View style={styles.menuSeparator}></View>
                    {this._getLegalMenuItem(
                        'Política de privacidad',
                        ()=>{this.goTo('Privacidad')})
                    }
                    {this._getLegalMenuItem(
                        'Condiciones generales',
                        ()=>{this.goTo('Condiciones')})
                    }
                </View>
                <View style={styles.bottomButtonCont}>
                    {order && <Button 
                        type="pink"
                        onButtonPress={()=>{this.goTo('Order')}} 
                        text={'Pedido en marcha'} 
                        marginLeft={0}
                    />}
                    {orderFollow && <Button 
                        type="pink"
                        onButtonPress={()=>{this.followOrder(orderFollow)}} 
                        text={'Pedido en marcha'} 
                        marginLeft={0}
                    />}
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading     : state.userDataReducer.loading,
        userData    : state.userDataReducer.userData,
        order       : state.orderReducer.order,
        orderFollow : state.orderFollowingReducer.order
    }
  }
  
export default connect(mapStateToProps)(MainMenu);

const iconSize = 6;

const styles = StyleSheet.create({
    menuBg: {
        backgroundColor : Constants.colors.midGray,
        justifyContent  : 'space-between',
        flex            : 1,
        paddingTop      : wp(7),
        paddingBottom   : wp(5),
        height          : hp(100)
    },
    logo:{
        width      : wp(30),
        height     : wp(40),
        resizeMode : 'contain',
        marginLeft : wp(10),
    },
    menuOptions:{
        paddingLeft : wp(7),
    },
    menuSeparator:{
        height: hp(3),
    },
    menuItem:{
        flexDirection: 'row',
        marginTop: hp(2.5),
        paddingLeft: wp(3),
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    locationItem:{
        paddingLeft: wp(3),
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    menuItemIcon:{
        marginRight: wp(6),
        resizeMode : 'contain',
        width: wp(iconSize),
        height: wp(iconSize),
    },
    menuItemIconAvatar:{
        marginRight: wp(4.5),
        marginLeft: wp(-1.5),
        resizeMode : 'cover',
        width: wp(iconSize+3),
        height: wp(iconSize+3),
        borderRadius: wp(parseInt((iconSize+3)/2)),
    },
    menuItemText:{
        color: Constants.colors.white,
        fontSize: hp(Constants.fontSize.l)
    },
    menuLegalItemText:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.m)
    },
    menuItemLinkText:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.l)
    },
    locationTexts:{
        flexDirection: 'column'
    },
    ordersButton:{

    },
    bottomButtonCont:{
        flexDirection: 'row',
        justifyContent: 'center'
    }
});