import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import Constants from '../../config/styles';
import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';


export default class ModalBack extends React.Component {
    constructor(props){
        super(props);

        this.onMenuClick = this.onMenuClick.bind(this);
    }

    onMenuClick(){
        const { navigation } = this.props;
        navigation.pop()
    }

    render() {
        return (
            <TouchableOpacity onPress={this.onMenuClick}>
                <Text style={styles.text}>Volver</Text>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    text:{
        marginRight: wp(5),
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
    }
});