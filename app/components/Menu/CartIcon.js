import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { TouchableOpacity, ImageBackground, StyleSheet, Text, View} from 'react-native';

import NavigationService from '../../services/NavigationService';
import OrderService from '../../services/OrderService';

import { setOrder } from '../../actions';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';
import { setMenuState } from '../../actions'; 
import Constants from '../../config/styles';
import Order from '../../screens/Order';

const cartIcon = require('../../assets/images/icons/cart.png');

class CartIcon extends React.Component {
    constructor(props){
        super(props);

        this.state={
            orderItems  : 0,
            minsLeft    : 0,
            secondsLeft : 0
        }

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);

        this.onMenuClick = this.onMenuClick.bind(this);
        this.setOrderTimer = this.setOrderTimer.bind(this);
        this.releaseInterval = this.releaseInterval.bind(this);
        this.timeoutOrder = this.timeoutOrder.bind(this);
        
    }

    onMenuClick(){
        NavigationService.navigate('Order')
    }

    setOrderTimer(){
        const { order } = this.props;
        
        if(order){
            const {minsLeft, secondsLeft} = OrderService.getOrderTimeLeft(order);
            
            if(minsLeft > 0 || secondsLeft > 0){
                this.setState({
                    minsLeft , 
                    secondsLeft: secondsLeft< 10 ? `0${secondsLeft}` : secondsLeft,
                    orderItems: OrderService.getOrderItems(order)
                })

                this.timeInterval = setInterval(()=>{
                    const {minsLeft, secondsLeft} = OrderService.getOrderTimeLeft(order);

                    if(minsLeft > 0 || secondsLeft > 0){
                        this.setState({
                            minsLeft , 
                            secondsLeft: secondsLeft< 10 ? `0${secondsLeft}` : secondsLeft,
                            orderItems: OrderService.getOrderItems(order)
                        })
                    }else{
                        this.timeoutOrder(order);
                    }
                },1000);
            }else{
                this.timeoutOrder(order);
            }
        }
    }

    timeoutOrder(order){
        if(order.status != 'pending-payment'){
            OrderService.setOrderTimeout(order);
            this.onSetOrder(null);
        }
        this.releaseInterval();
        this.setState({
            minsLeft: 0 , 
            secondsLeft: 0,
            orderItems: 0
        })
    }

    componentDidMount(){
        if(this.props.order){
            this.setOrderTimer();
        }else{
            
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(!prevProps.order && this.props.order){
            this.setOrderTimer();
        }
        if(prevProps.order && !this.props.order){
            this.setState({ minsLeft: 0, secondsLeft: 0, orderItems: 0 })
            this.releaseInterval();
        }
    }

    render() {
        const {orderItems, minsLeft, secondsLeft} = this.state;

        return (
            <TouchableOpacity onPress={this.onMenuClick}>
                <View style={styles.cartContainer}>
                    <Text style={styles.cartTimer}>{(minsLeft>0 || secondsLeft>0) && minsLeft}{(minsLeft>0 || secondsLeft>0) && `:${secondsLeft}`}</Text>
                    <ImageBackground source={cartIcon} style={styles.icon}>
                        <Text style={styles.cartItems}>{orderItems > 0 && orderItems}</Text>
                    </ImageBackground>
                </View>
            </TouchableOpacity>
        );
    }

    componentWillUnmount(){
        this.releaseInterval()
    }

    releaseInterval(){
        clearInterval(this.timeInterval);
    }
}

function mapStateToProps(state, props) {
    return {
        menuOpen: state.toggleMenuReducer.menuOpen,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(CartIcon);


const styles = StyleSheet.create({
    cartContainer:{
        flexDirection: 'row',
        justifyContent:'flex-end',
        alignItems:'center'
    },
    cartTimer:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.textGray
    },
    icon:{
        width: wp(6),
        height: wp(8),
        marginLeft: wp(2),
        marginRight: wp(3),
        resizeMode: 'contain',
        justifyContent:'center',
        alignItems:'center'
    },
    cartItems:{
        marginTop: wp(2),
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.white
    }
});