import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';
import { setMenuState } from '../../actions'; 

const menuIcon = require('../../assets/images/icons/menu.png');

class MenuIcon extends React.Component {
    constructor(props){
        super(props);

        this.onSetMenuState = bindActionCreators(setMenuState, props.dispatch);
        this.onMenuClick = this.onMenuClick.bind(this);
    }

    onMenuClick(){
        this.onSetMenuState(!this.props.menuOpen);
    }

    render() {
        return (
            <TouchableOpacity onPress={this.onMenuClick}>
                <Image source={menuIcon} style={styles.icon}/>
            </TouchableOpacity>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        menuOpen: state.toggleMenuReducer.menuOpen,
    }
  }
  
  export default connect(mapStateToProps)(MenuIcon);


const styles = StyleSheet.create({
    icon:{
        width: wp(9),
        height: wp(5),
        marginLeft: wp(3),
        resizeMode: 'contain'
    }
});