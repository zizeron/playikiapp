import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import firebase from 'react-native-firebase';

import Constants from '../../config/styles';
import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';

const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
  AccessToken,
} = FBSDK;

export default class FacebookButton extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            loading: false
        }

        this.login = this.login.bind(this);
    }

    login(){
        this.setState({loading: true});
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then((result) => {
            if (result.isCancelled) {
                console.log('You cancelled the sign in.');
            } else {
                AccessToken.getCurrentAccessToken()
                    .then((data) => {
                        const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
                        console.log('​FacebookButton -> login -> credential', credential);
                        
                        firebase.auth().signInAndRetrieveDataWithCredential(credential)
                        .then(() => {
                            
                            console.log('​FacebookButton -> login -> success', );
                            this.props.onLoginSucess();
                        })
                        .catch((error) => {
                            console.log('====================== ERROR: ', error.code);
                            this.setState({loading: false});
                        });
                    });
            }
          },
          (error) => {
            console.log('Sign in error', error);
            this.setState({loading: false});
          }
        );

    }

    render(){
        const { loading } = this.state;
        return (
            <View>
                {loading ? 
                    <ActivityIndicator size="small" color={Constants.colors.pink}/>:
                    <TouchableOpacity onPress={this.login}>
                        <View style={styles.button}> 
                            <Text style={styles.buttonText}>Continue with Facebook</Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button:{
        backgroundColor:Constants.colors.fbBlue,
        paddingTop: hp(2),
        paddingBottom: hp(2),
        paddingLeft: wp(3),
        paddingRight: wp(3),
        width: wp(70),
        marginLeft: wp(15),
        borderRadius: 5,
    },
    buttonText:{
        textAlign: 'center',
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.white,

    }
});