import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp} from '../../services/ScreenDimensions';

import Constants from '../../config/styles';


export default class Button extends React.Component {
    constructor(props){
        super(props);

        this.defaultStyles = {
            button:{
                backgroundColor : Constants.colors.gray,
                width           : wp(70),
                height          : hp(6),
                marginLeft      : wp(15),
                padding         : 0,
                justifyContent  : 'center',
                alignItems      : 'center',
                borderRadius    : 100
            },
            text:{
                fontSize      : hp(Constants.fontSize.l),
                color         : Constants.colors.pink,
                textTransform : 'uppercase'
            }
        }
    }

    render() {
        let propsStyles = {
            button: {},
            text: {}
        };

        //BUTTON STYLES
        if(this.props.type === 'dark'){
            propsStyles.button.backgroundColor = Constants.colors.darkGray;
        
        }else if(this.props.type === 'pink'){
            propsStyles.button.backgroundColor = Constants.colors.pink;
            propsStyles.text.color = Constants.colors.white;
        
        }else if(this.props.type === 'gray'){
            propsStyles.button.backgroundColor = Constants.colors.textGray;
            propsStyles.text.color = Constants.colors.white;

        }else if(this.props.type === 'yellow'){
            propsStyles.button.backgroundColor = Constants.colors.yellow;
            propsStyles.text.color = Constants.colors.white;
        }else{
            propsStyles.button.backgroundColor = Constants.colors.gray;
        }

        if(this.props.height) propsStyles.button.height = hp(this.props.height);
        if(this.props.width) propsStyles.button.width = wp(this.props.width);

        if(typeof this.props.marginLeft !== 'undefined'){
            propsStyles.button.marginLeft = wp(this.props.marginLeft);

        }else if(this.props.width){ //Si no le paso el margin left pero modifico el width, lo recalcula centrado
            propsStyles.button.marginLeft = wp((100 - this.props.width) / 2);
        }

        if(typeof this.props.marginBottom !== 'undefined'){
            propsStyles.button.marginBottom = wp(this.props.marginBottom);
        }

        //TEXT STYLES
        if(this.props.textColor) propsStyles.text.color = this.props.textColor;
        if(this.props.textSize) propsStyles.text.fontSize = hp(this.props.textSize);

        if(this.props.textUppercase === false) propsStyles.text.textTransform = 'none';

        const buttonDefaultStyles = Object.assign({},this.defaultStyles.button);
        const textDefaultStyles = Object.assign({},this.defaultStyles.text);

        const buttonStyles = Object.assign(buttonDefaultStyles, propsStyles.button);
        const textStyles = Object.assign(textDefaultStyles, propsStyles.text);

        return (
            <TouchableOpacity style={buttonStyles} onPress={this.props.onButtonPress} title={this.props.text}>
                <Text style={textStyles}>{this.props.text.toUpperCase()}</Text>
            </TouchableOpacity>
        );
    }
}
