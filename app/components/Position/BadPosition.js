import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import { 
    widthPercentage as wp, 
    heightPercentage as hp,
    aspectRatio
} from '../../services/ScreenDimensions';
import NavigationService from '../../services/NavigationService';
import Constants from '../../config/styles';
import Settings from '../../config/settings';

import Button from '../../components/Buttons/Button';

const mapMarker = require('../../assets/images/icons/localiza/localiza.png');
const notAvailable = require('../../assets/images/icons/notAvailable.png');

export default class BadPosition extends React.Component {
    static navigationOptions = {
        title: 'Tu Posición',
        headerStyle: {backgroundColor: Constants.colors.gray},
      };

    constructor(props){
        super(props);

        this.getMapConfig = this.getMapConfig.bind(this);
        this.setLocation = this.setLocation.bind(this);
        this.outOfBeach = this.outOfBeach.bind(this);
        this.outOfSection = this.outOfSection.bind(this);
    }

    getMapConfig(){
        const { userData, nearestBeach } = this.props;

        if(userData && userData.location && nearestBeach && nearestBeach.coordinates){
            let mapCoordinates = [
                ...nearestBeach.coordinates, 
                {
                    lat:userData.location.latitude, 
                    lng: userData.location.longitude
                }
            ];

            let minX = mapCoordinates[0].lat;
            let maxX = mapCoordinates[0].lat;
            let minY = mapCoordinates[0].lng;
            let maxY = mapCoordinates[0].lng;

            mapCoordinates.forEach((c) => {
                minX = Math.min(minX, c.lat);
                maxX = Math.max(maxX, c.lat);
                minY = Math.min(minY, c.lng);
                maxY = Math.max(maxY, c.lng);
            })

            let midX = (minX + maxX) / 2;
            let midY = (minY + maxY) / 2;
            let deltaX = maxX - minX;
            let deltaY = maxY - minY;


            return {
                latitude: midX,
                longitude: midY,
                latitudeDelta: deltaX + (50 * Settings.mapDelta),
                longitudeDelta: deltaY+ (50 * Settings.mapDelta)
            }
        }else{
            return {
                latitude       : Settings.mapDefaulLatitude,
                longitude      : Settings.mapDefaulLongitude,
                latitudeDelta  : Settings.mapDelta,
                longitudeDelta : Settings.mapDelta * aspectRatio()
            }
        }
    }

    setLocation(){
        NavigationService.navigate('SetLocation')
    }

    outOfBeach(){
        const { userData, nearestBeach } = this.props;
        const userName = userName && userData.name ? ` ${userData.name}` : '';
        const initialRegion = this.getMapConfig();
        const location = userData && userData.location ? userData.location : {};
        
        return (
            <ScrollView contentContainerStyle={styles.mainView}>
              <View style={styles.titleContainer}>
                <Text style={styles.title}>¡LO SENTIMOS{userName.toUpperCase()}!</Text>
              </View>
              <View style={styles.subtitleContainer}>
                <Text style={styles.subtitle}>¡Playiki aún no está disponible en esta playa!</Text>
              </View>
              <Image style={styles.img} source={notAvailable} />
              <View style={styles.textContainer}>
                <Text style={styles.text}>Estamos ampliando el número de playas donde ofrecemos nuestro servicio. Mantente alerta, dentro de poco podrás utilizar Playiki en esta playa.</Text>
              </View>
              <View style={styles.subtitleContainer}>
                <Text style={styles.subtitle}>La playa más cercana con cobertura Playiki es: {nearestBeach.name}</Text>
              </View>
              <MapView
                    style={styles.map}
                    initialRegion={initialRegion}
                    mapType="hybrid"
                >
                    <Marker
                        image={mapMarker}
                        key={'user-location'}
                        style={{ width: 40, height: 40 }}
                        coordinate={location}
                    />
                </MapView>
                <View style={styles.actions}>
                    <Button 
                        type="pink"
                        onButtonPress={this.setLocation} 
                        text={'Cambiar posición'} 
                        marginLeft={0}
                    />
                </View>
            </ScrollView>
        )
    }

    outOfSection(){
        const { userData, nearestBeach } = this.props;
        if (!userData) return null;
        const userName = userData && userData.name ? ` ${userData.name}` : '';
        const initialRegion = this.getMapConfig();
        const location = userData && userData.location ? userData.location : {};
        
        return (
            <ScrollView contentContainerStyle={styles.mainView}>
              <View style={styles.titleContainer}>
                <Text style={styles.title}>¡LO SENTIMOS{userName.toUpperCase()}!</Text>
              </View>
              <View style={styles.subtitleContainer}>
                <Text style={styles.subtitle}>¡Playiki aún no está disponible en esta parte de la playa!</Text>
              </View>
              <Image style={styles.img} source={notAvailable} />
              <View style={styles.textContainer}>
                <Text style={styles.text}>Intenta moverte unos metros o ajustar tu ubicación de forma más precisa.</Text>
              </View>
              <View style={styles.subtitleContainer}>
                <Text style={styles.subtitle}></Text>
              </View>
              <MapView
                    style={styles.map}
                    initialRegion={initialRegion}
                    mapType="hybrid"
                >
                    <Marker
                        image={mapMarker}
                        key={'user-location'}
                        coordinate={location}
                        style={{ width: 40, height: 40 }}
                    />
                </MapView>
                <View style={styles.actions}>
                    <Button 
                        type="pink"
                        onButtonPress={this.setLocation} 
                        text={'Cambiar posición'} 
                        marginLeft={0}
                    />
                </View>
            </ScrollView>
        )
    }

    render(){
        const { insideBeach, insideSection } = this.props;

        if(!insideBeach) return this.outOfBeach();

        return this.outOfSection();
    }
}

const styles = StyleSheet.create({
    mainView:{
        backgroundColor: Constants.colors.white,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingBottom: wp(5),
    },
    titleContainer:{
        marginBottom: hp(2),
        marginTop: hp(3)
    },
    title:{
        fontSize: hp(Constants.fontSize.xl),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center'
    },
    subtitleContainer:{
        marginBottom: hp(1),
        paddingHorizontal: wp(5)
    },
    subtitle:{
        fontSize: hp(Constants.fontSize.ll),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center'
    },
    img:{
        width: wp(28),
        resizeMode: 'contain'
    },
    textContainer:{
        marginBottom: hp(2)
    },
    text:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.textGray,
        textAlign: 'center'
    },
    map:{
        width: wp(90),
        height: hp(25),
    },
    actions:{
        marginTop: hp(5),
        marginBottom: hp(5),
    }
})