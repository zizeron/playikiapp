import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';

import MapView, { Marker } from 'react-native-maps';

import { 
    widthPercentage as wp, 
    heightPercentage as hp,
    aspectRatio
} from '../../services/ScreenDimensions';
import NavigationService from '../../services/NavigationService';
import UserService from '../../services/UserService';

import Constants from '../../config/styles';
import Settings from '../../config/settings';

import Button from '../../components/Buttons/Button';

const mapMarker = require('../../assets/images/icons/localiza/localiza.png');


export default class GoodPosition extends React.Component {
    constructor(props){
        super(props);

        this.goToProducts = this.goToProducts.bind(this);
    }

    goToProducts(){
        NavigationService.navigate('Products')
    }

    render(){
        const { userData, hasBike } = this.props;
        if (!userData) return null;
        const userName = userData.name ? ` ${userData.name}` : '';
        // const playid = UserService.calculatePlayId(userData);

        return (
            <ScrollView contentContainerStyle={styles.mainView}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>¡GENIAL{userName.toUpperCase()}!</Text>
                </View>
                <View style={styles.subtitleContainer}>
                    <Text style={styles.subtitle}>¡Te tenemos localizado!</Text>
                </View>
                <View style={styles.textContainer}>
                    {hasBike ? 
                        <Text style={styles.text}>Recuerda actualizar tu localización si cambias de lugar en la playa.</Text> :
                        <Text style={styles.text}>Estamos preparando los productos disponibles en tu zona, podrás pedirlos en breve.</Text>
                    }
                </View>
                <MapView
                    style={styles.map}
                    initialRegion={{
                        latitude: userData.location.latitude,
                        longitude: userData.location.longitude,
                        latitudeDelta: Settings.mapDelta,
                        longitudeDelta: Settings.mapDelta * aspectRatio(),
                    }}
                    mapType="hybrid"
                >
                    <Marker
                        image={mapMarker}
                        key={'user-location'}
                        style={{ width: 40, height: 40 }}
                        coordinate={userData.location}
                        draggable
                    />
                </MapView>
                <View style={styles.subtitleContainer}>
                    {/* <Text style={styles.subtitle}>Tu PLAYID es {playid}</Text> */}
                </View>
                <View style={styles.actions}>
                    <Button 
                        type="pink"
                        onButtonPress={this.goToProducts} 
                        text={'HACER UN PEDIDO'} 
                        marginLeft={0}
                    />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    mainView:{
        backgroundColor: Constants.colors.white,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingBottom: wp(5),
    },
    titleContainer:{
        marginBottom: hp(5),
        marginTop: hp(5)
    },
    title:{
        fontSize: hp(Constants.fontSize.xl),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center'
    },
    subtitleContainer:{
        marginBottom: hp(4)
    },
    subtitle:{
        fontSize: hp(Constants.fontSize.ll),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center'
    },
    img:{
        width: wp(28),
        resizeMode: 'contain'
    },
    textContainer:{
        marginBottom: hp(3)
    },
    text:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center',
        paddingHorizontal: wp(10)
    },
    map:{
        width: wp(90),
        height: hp(25),
        marginBottom: hp(8),
    },
    actions:{
        marginTop: hp(4),
        marginBottom: hp(5),
    }
    
})