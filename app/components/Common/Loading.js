import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';

import Constants from '../../config/styles';

export default function Loading(props){
    return (
        <View style={styles.main}> 
            <ActivityIndicator size="large" color={Constants.colors.pink}/>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        width: wp(100),
        height: hp(100),
        justifyContent: 'center',
        alignItems: 'center'
    }
});