import React from 'react';
import { View, StyleSheet, ActivityIndicator, Image, Text } from 'react-native';
import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';

import Constants from '../../config/styles';
const userDefault = require('../../assets/images/other/default-user.jpg');

export default function Repartidor(props){
    const { image, deliver, phone, timeEst, playikid, items } = props.repartidor;
    let userPicture = image ? {uri: image} : userDefault;

    return (
        <View style={styles.main}> 
            <Image style={styles.image} source={userPicture}/>
            <View style={styles.info}>
                <View style={styles.infoItem}>
                    <Text style={styles.infoItemText}>REPARTIDOR: <Text style={styles.infoItemTextPink}>{deliver}</Text></Text>
                </View>
                {/* <View style={styles.infoItem}>
                    <Text style={styles.infoItemText}>MÓVIL: <Text style={styles.infoItemTextPink}>{phone}</Text></Text>
                </View> */}
                {timeEst && <View style={styles.infoItem}>
                    <Text style={styles.infoItemText}>TIEMPO ESTIMADO: <Text style={styles.infoItemTextPink}>{timeEst}</Text></Text>
                </View>}
                {items && <View style={styles.infoItem}>
                    <Text style={styles.infoItemText}>PEDIDO: <Text style={styles.infoItemTextPink}>{items} ITEMS</Text></Text>
                </View>}
                <View>
                    <Text style={styles.infoItemText}>TU PLAYIKID: <Text style={styles.infoItemTextPink}>{playikid}</Text></Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        width: wp(90),
        padding: wp(3),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Constants.colors.white,
        borderWidth: 1,
        borderColor: Constants.colors.textGray,
        borderRadius: wp(3),
        shadowColor: Constants.colors.textGray,
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 5,
        shadowOpacity: 0.5,
    },
    image:{
        width: wp(20),
        height: wp(20),
        resizeMode: 'cover',
        borderRadius: wp(10),
        borderWidth: 1,
        borderColor: Constants.colors.gray,
        overflow: 'hidden',
    },
    info:{
        marginLeft: wp(5)
    },
    infoItem:{
        marginBottom: hp(0.6)
    },
    infoItemText:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300'
    },
    infoItemTextPink:{
        color: Constants.colors.pink,
        fontWeight: '300'
    }
});