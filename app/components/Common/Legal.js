import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { widthPercentage as wp, heightPercentage as hp } from '../../services/ScreenDimensions';

import NavigationService from '../../services/NavigationService';

import Constants from '../../config/styles';

export default function Legal(props){
    return (
        <View style={styles.main}> 
            <Text style={styles.paragraph}>Los datos que te solicitamos son necesarios para poder realizar tus pedidos en Playiki,pero te agradecemos muchísimo tu confianza al incorporarlos. Queremos que sepas yes nuestro deber informarte que tus datos van a estar seguros porque cumplimos elReglamento General de Protección de Datos, y te adjuntamos esta información quedebes saber:</Text>
            <Text style={styles.paragraph}>- Los datos de carácter personal que nos proporciones rellenando el presente formulario serán tratados por Playiki Click SL como responsable de esta app.</Text>
            <Text style={styles.paragraph}>- La finalidad de la recogida y tratamiento de los datos personales que te solicitamos es para poder servir y enviar los pedidos o servicios que quieras solicitar en la app, ya sea directamente por Playiki o a través de terceras partes.</Text>
            <Text style={styles.paragraph}>- Al marcar en la casilla y hacer click en confirmar, estás dando tu legítimo consentimiento para que tus datos sean tratados conforme a las finalidades descritas en la <Text onPress={()=>{NavigationService.navigate('Legal');}} style={styles.underlined}>política de privacidad</Text></Text>
            <Text style={styles.paragraph}>- Como usuario e interesado te informo que los datos que nos facilitas estarán ubicados en los servidores de Firebase (proveedor tecnológico de Playiki) dentro de la UE. Ver política de privacidad de Firebase (https://firebase.google.com/support/privacy/?hl=es-419)</Text>
            <Text style={styles.paragraph}>Podrás ejercer tus derechos de acceso, rectificación, limitación y suprimir los datos escribiéndonos a info@playiki.com así como el derecho a presentar una reclamación ante una autoridad de control.</Text>
            <Text style={styles.paragraph}>Puedes consultar la información adicional y detallada sobre Protección de Datos en: <Text onPress={()=>{NavigationService.navigate('Legal');}} style={styles.underlined}>este enlace</Text>, así como consultar nuestra política de privacidad.</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        width: wp(90),
        marginHorizontal: wp(5),
    },
    paragraph:{
        fontSize: hp(Constants.fontSize.m),
        marginBottom: hp(1),
        color: Constants.colors.textGray
    },
    underlined:{
        textDecorationLine: 'underline'
    }
});

