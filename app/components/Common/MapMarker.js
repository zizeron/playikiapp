import React from 'react';
import { StyleSheet, Image } from 'react-native';

const mapMarker = require('../../assets/images/icons/userLocation.png');

export default function MapMarker(props){
    return (
        <Image 
            source={mapMarker} 
            style={styles.markerIcon}
            onLoad={props.onLoad}
            onLayout={props.onLoad}
        />
    )
}

const styles = StyleSheet.create({
    markerIcon:{
        width: 30, 
        height: 30
    },
});