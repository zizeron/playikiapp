import React from 'react';
import RNPickerSelect from 'react-native-picker-select';

import { widthPercentage as wp, heightPercentage as hp} from '../../services/ScreenDimensions';

import Constants from '../../config/styles';

import Input from './Input';

export default class Select extends React.Component {
    constructor(props){
        super(props);

        this.defaultStyles = {
            inputIOS:{
                width: wp(70),
                height: hp(7),
                marginLeft: wp(0),
                marginTop:hp(0),
                fontSize: hp(Constants.fontSize.l),
                paddingVertical: wp(2),
                paddingHorizontal: wp(4),
                borderWidth: 0,
                borderRadius: 100,
                backgroundColor: Constants.colors.gray,
                color: Constants.colors.darkGray,
                textAlign: 'center'
            },
            icon:{
                right: wp(20),
                top: 20
            }
        }
    }

    render() {
        const propsStyles = {
            inputIOS: {},
            icon: {}
        };

        let translateY = -10;

        //INPUT STYLES
        if(this.props.width) propsStyles.inputIOS.width = wp(this.props.width);
        if(this.props.height) {
            propsStyles.inputIOS.height = hp(this.props.height);
            if(this.props.height == 4){
                translateY = -16
            }
        }

        if(typeof this.props.paddingVertical !== 'undefined'){
            propsStyles.inputIOS.paddingVertical = wp(this.props.paddingVertical);
        }

        if(typeof this.props.marginTop !== 'undefined'){
            propsStyles.inputIOS.marginTop = wp(this.props.marginTop);
        }

        if(typeof this.props.marginLeft !== 'undefined'){
            propsStyles.inputIOS.marginLeft = wp(this.props.marginLeft);

        }else if(this.props.width){ //Si no le paso el margin left pero modifico el width, lo recalcula centrado
            propsStyles.inputIOS.marginLeft = wp((100 - this.props.width) / 2);
        }

        //TEXT STYLES
        if(this.props.textColor) propsStyles.inputIOS.color = this.props.textColor;
        if(this.props.textSize) propsStyles.inputIOS.fontSize = hp(this.props.textSize);

        //ICON
        if(typeof this.props.iconRight !== 'undefined') propsStyles.icon.right = wp(this.props.iconRight)
        if(this.props.iconTop) propsStyles.icon.top = this.props.iconTop

        const inputDefaultStyles = Object.assign({},this.defaultStyles.inputIOS)
        const inputStyles = Object.assign(inputDefaultStyles, propsStyles.inputIOS);
        
        const iconDefaultStyles = Object.assign({},this.defaultStyles.icon)
        const iconStyles = Object.assign(iconDefaultStyles, propsStyles.icon);

        const inputStylesAndroid = Object.assign({
            height:hp(6),
            width: wp(70),
            marginLeft: wp(15),
            backgroundColor:Constants.colors.gray,
            borderRadius: 100,
            
        }, propsStyles.inputIOS);

        return (
            <RNPickerSelect
                placeholder={this.props.placeholder}
                items={this.props.items}
                onValueChange={(value) => { this.props.onValueChange(value) }}
                style={{
                    inputIOS: inputStyles, 
                    inputAndroid:{marginBottom: hp(2), transform: [
                        { scaleX: 0.8 }, 
                        { scaleY: 0.8 },
                        { translateY: translateY }
                     ]}, 
                    pickerItemStyle:{fontSize: hp(Constants.fontSize.xl),},
                    underline: {borderTopWidth: 0},
                    viewContainer: inputStylesAndroid,
                    icon: iconStyles
                }}
                value={this.props.value}
            />
        );
    }
}

