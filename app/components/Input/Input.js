import React from 'react';
import { TextInput, Text, StyleSheet } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp} from '../../services/ScreenDimensions';

import Constants from '../../config/styles';


export default class Input extends React.Component {
    constructor(props){
        super(props);

        this.defaultStyles = {
            input:{
                backgroundColor: Constants.colors.gray,
                width: wp(70),
                height: hp(6),
                marginLeft: wp(15),
                fontSize: hp(Constants.fontSize.l),
                color: Constants.colors.darkGray,
                textAlign: 'center',
                padding: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 100
            }
        }
    }

    render() {
        const propsStyles = {
            input: {}
        };

        //INPUT STYLES
        if(this.props.height) propsStyles.input.height = hp(this.props.height);
        if(this.props.width) propsStyles.input.width = wp(this.props.width);

        if(typeof this.props.marginLeft !== 'undefined'){
            propsStyles.input.marginLeft = wp(this.props.marginLeft);

        }else if(this.props.width){ //Si no le paso el margin left pero modifico el width, lo recalcula centrado
            propsStyles.input.marginLeft = wp((100 - this.props.width) / 2);
        }

        //TEXT STYLES
        if(this.props.textColor) propsStyles.input.color = this.props.textColor;
        if(this.props.textSize) propsStyles.input.fontSize = hp(this.props.textSize);

        const inputDefaultStyles = Object.assign({},this.defaultStyles.input)
        const inputStyles = Object.assign(inputDefaultStyles, propsStyles.input);
        
        return (
            <TextInput 
                style={inputStyles} 
                onChangeText={(value)=>{this.props.onInputChange(this.props.field, value)}} 
                value={this.props.value} 
                secureTextEntry={this.props.type === 'password'}
                placeholder={this.props.placeholder}
                placeholderTextColor={this.defaultStyles.input.color}
            />
        );
    }
}

