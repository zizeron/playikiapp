import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';
import SideMenu from 'react-native-side-menu';
import SplashScreen from 'react-native-splash-screen';
import stripe from 'tipsi-stripe';
import { AsyncStorage } from "react-native";
import AppIntroSlider from 'react-native-app-intro-slider';

import { widthPercentage as wp, heightPercentage as hp } from './services/ScreenDimensions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setUserData, setMenuState, setOrder, setOrderFollowing } from './actions'; 

import DataService from './services/DataService';
import UserService from './services/UserService';
import OrderService from './services/OrderService';
import NavigationService from './services/NavigationService';
import RootStack from './config/routes';
import Constants from './config/styles';
import MainMenu from './components/Menu/MainMenu';
import DeliverReady from './components/Modals/DeliverReady';
import DeliverValoration from './components/Modals/DeliverValoration';

stripe.setOptions({
  publishableKey: 'pk_live_Xs7011XMPZMbl3kuqXF9uA2g', // pk_test_1iMKCEQzySXYGXUHcx651lvg pk_live_Xs7011XMPZMbl3kuqXF9uA2g
})

const slides = [
  {
    key: 'step_1',
    title: 'Geolocalízate',
    text: 'Al entrar, la app te solicitará geolocalizarte para obtener tu posición en la playa. Puedes moverte por el mapa y modificar el lugar que te indica tu GPS para mayor precisión.',
    image: require('./assets/images/presentation/presentation_1.png'),
    backgroundColor: '#75ADE7'
  },
  {
    key: 'step_2',
    title: 'Elige tus productos',
    text: 'Navega por las diferentes categorías de productos y selecciona aquellos que quieres pedir. Te los reservamos durante 10 minutos.',
    image: require('./assets/images/presentation/presentation_2.png'),
    backgroundColor: '#F8A71A'
  },
  {
    key: 'step_3',
    title: 'Paga on line',
    text: 'Paga tu pedido de manera on line y segura a través de la app, con tu tarjeta de crédito. Rápido y sencillo. Playiki no acepta pagos en efectivo.',
    image: require('./assets/images/presentation/presentation_3.png'),
    backgroundColor: '#B765DB'
  },
  {
    key: 'step_4',
    title: 'Recibe tu compra',
    text: 'En menos de media hora, te haremos llegar tu pedido a tu lugar en la playa. Puedes realizar el seguimiento en tiempo real de la ubicación del Player (repartidor).',
    image: require('./assets/images/presentation/presentation_4.png'),
    backgroundColor: '#529E6C'
  },
];

const playikiLogo = require('./assets/images/presentation/logo_presentation.png')

class App extends Component {
    constructor(props) {
      super(props);

      this.menuOpen = false;

      this.state = {
          hideDeliverModal: false,
          showValoracion: false,
          orderFollowing: null,
          showIntro: false
      }

      this.deliverLocationHandler = null;
      this.orderHandler = null;
      this.prevOrderStatus = 'on-round';

      this.onSetUserData = bindActionCreators(setUserData, props.dispatch);
      this.onSetMenuState = bindActionCreators(setMenuState, props.dispatch);
      this.onSetOrder = bindActionCreators(setOrder, props.dispatch);
      this.onSetOrderFollowing = bindActionCreators(setOrderFollowing, props.dispatch);

      this.onCloseDeliverModal = this.onCloseDeliverModal.bind(this);
      this.onCloseValoracionModal = this.onCloseValoracionModal.bind(this);
      this.checkNotifications = this.checkNotifications.bind(this);
      this.locateDeliver = this.locateDeliver.bind(this);
      this.shouldShowIntro = this.shouldShowIntro.bind(this);
      this.hideIntro = this.hideIntro.bind(this);
    }

    checkNotifications(userData){
      this.messageListener = firebase.messaging().onMessage((message) => {
      });

      firebase.messaging().hasPermission()
        .then(enabled => {
          if (enabled) {
            firebase.messaging().getToken()
              .then(fcmToken => {
                if (fcmToken) {
                  userData.fcmToken = fcmToken;
                  UserService.saveUserInfo(userData);
                } else {
                  // user doesn't have a device token yet
                } 
              });
          } else {
            firebase.messaging().requestPermission()
              .then(() => {
                console.log('​App -> checkNotifications -> accepted' );
              })
              .catch(error => {
                console.log('​App -> checkNotifications -> rejected' );
              });
          } 
        });
    }

    async shouldShowIntro(){
      try {
        const value = await AsyncStorage.getItem('VIEWED_INTRO');
        if (value) return false;
        else return true;

      } catch (error) {
        console.log('TCL: App -> catch -> error', error); 
      }
    }

    async hideIntro(){
      try {
        await AsyncStorage.setItem('VIEWED_INTRO', 'true');
        
      } catch (error) {
        console.log('TCL: catch -> error', error);
      }
      this.setState({showIntro:false});
    }

    async componentDidMount(){
      if(await this.shouldShowIntro()){
        this.setState({showIntro:true});
      }

      this.authHandler = firebase.auth().onAuthStateChanged((user) => {
        let db = firebase.firestore();
        if (user) {
          db.collection('users').doc(user.uid).get().then((doc) => {
            let userData = {};
            let reduxUser = this.props.userData;

            if (!doc.exists) { //Si no existe es porque el usuario se está registrando con FB
              const provider = user.providerData && user.providerData[0] ? user.providerData[0] : {};
              console.log('​App -> componentDidMount -> provider', reduxUser);

              if(provider.providerId === "facebook.com"){
                userData = {
                  uid      : user.uid,
                  name     : user.displayName,
                  playid   : UserService.getPlayID(user.displayName),
                  email    : user.email,
                  picture  : user.photoURL,
                  provider : provider.providerId,
                  hasRole  : false
                }
              } else { //User signup with phone 
                userData = {
                  uid      : user.uid,
                  name     : reduxUser.name,
                  playid   : UserService.getPlayID(reduxUser.name),
                  email    : reduxUser.email,
                  provider : provider.providerId,
                  hasRole  : false
                }
              }

              UserService.saveUserInfo(userData);
            }else{
              userData = doc.data(); //Si el usuario se registró con el móvil, se creó al confirmarlo
            }

            this.checkNotifications(userData);
            const prevUserData = this.props.userData;
            this.onSetUserData(Object.assign(userData, prevUserData));
            
            if(this.props.redirectAfterlogin){
              NavigationService.navigate('Welcome');
            }

            //Revisamos si tiene un pedido abierto
            OrderService.getUserActiveOrder(userData).then(
              (order) => {
                this.onSetOrder(order);
              },
              (error) => {
                console.log('​App -> componentDidMount -> error', error);
              }
            );

            OrderService.getUserActiveOrderFollowing(userData).then(
              (order) => {
                console.log('​App -> componentDidMount -> getUserActiveOrderFollowing', order);
                this.setState({orderFollowing: order});
                this.onSetOrderFollowing(order);
              },
              (error) => {
                console.log('​App -> componentDidMount -> error', error);
              }
            )
          })
        }else{
          this.onSetUserData(null); 
          NavigationService.navigate('Home');
        }
        setTimeout(()=>{ SplashScreen.hide(); },200);
      });
    }

    locateDeliver(){    
      const { orderFollow, userData } = this.props;

      DataService.getUserDeliver(userData).then(
        (deliver) => {
          this.deliverLocationHandler = firebase.firestore()
            .collection('users')
            .doc(deliver.deliverId)
            .onSnapshot((doc) => {
                const data = doc.data();
                if(data && data.location){
                    this.setState({
                        deliver,
                        deliverLocation: {
                            latitude: parseFloat(data.location.latitude),
                            longitude: parseFloat(data.location.longitude)
                        }
                    })
                }
            });

            this.orderHandler = firebase.firestore()
              .collection('order')
              .doc(orderFollow.id)
              .onSnapshot((doc) => {
                  const data = doc.data();
                  if(data){
                      if(this.prevOrderStatus !== data.status && data.status === 'delivered'){
                        this.prevOrderStatus = 'delivered';
                        this.setState({ showValoracion: true })
                      }
                  }
              });
            
        },
        (error) => {
            console.log('​PaymentConfirmScreen -> componentDidMount -> error', error);
            
        }
      )   
    }
  
    render(){
      const { deliverLocation, deliver, hideDeliverModal, showValoracion, showIntro } = this.state;
      const { orderFollow, userData } = this.props;

      if(showIntro){
        return (
          <AppIntroSlider
            slides={slides}
            renderItem={this._renderItem}
            hideNextButton={true}
            doneLabel="Hecho"
            onDone={this.hideIntro}
            onSkip={this.hideIntro}
            dotStyle={{backgroundColor: Constants.colors.darkGray}}
          />
        );
      }
      
      let showModal = false;
      let distance = 1000;
      let charge = {};
      let repartidor = {};

      if(orderFollow && userData && userData.uid){
        if(!this.deliverLocationHandler){
          this.locateDeliver();
        }else{
          if(userData.location && deliverLocation){
            distance = DataService.getDistance(userData.location, deliverLocation);
          }
          
          if(distance < 20){
              showModal = true;
          }  
          
          charge = orderFollow.charge;  
          repartidor = {
              deliver: deliver ? deliver.deliver : '', 
              image: deliver ? deliver.image : '',
              phone: deliver ? deliver.phone : '', 
              items: OrderService.getOrderItems(orderFollow), 
              playikid: charge && charge.playId ? charge.playId : ''
          }
        } 
      } 

      const menu = <MainMenu />;
      return (
        <SideMenu 
          menu={menu} 
          openMenuOffset={wp(80)}
          isOpen={this.props.menuOpen} 
          disableGestures={this.props.menuDisabled}
          onChange={(isOpen) => {
            if(isOpen !== this.props.menuOpen){
              this.onSetMenuState(isOpen);
            }
          }}
        >
            <RootStack ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}/>
            <DeliverReady 
                showModal={showModal && !hideDeliverModal} 
                onCloseModal={this.onCloseDeliverModal}
                repartidor={repartidor}
            />
            <DeliverValoration
                showModal={showValoracion} 
                onCloseModal={this.onCloseValoracionModal}
            />
        </SideMenu>
      )
    }

    _renderItem = props => (
      <View
        style={[styles.mainContent, {
          paddingTop: props.topSpacer,
          paddingBottom: props.bottomSpacer,
          width: props.width,
          height: props.height,
          backgroundColor: props.backgroundColor
        }]}
      >
        <TouchableOpacity style={styles.playikiSkip} onPress={this.hideIntro}><Text>Saltar</Text></TouchableOpacity>
        <Image style={styles.playikiLogo} source={playikiLogo}></Image>
        <Image style={styles.image} source={props.image}></Image>
        <View>
          <Text style={styles.title}>{props.title}</Text>
          <Text style={styles.text}>{props.text}</Text>
        </View>
      </View>
    );

    onCloseDeliverModal(confirmed){
      const { orderFollow, orderFollowing } = this.props;
      
      if(confirmed){
        let order = orderFollowing ? orderFollowing : orderFollow;
        order.userConfirmed = true;
        OrderService.saveOrderToDatabase(order);
      }
      this.setState({hideDeliverModal:true})
    }

    onCloseValoracionModal(value){
        const { orderFollowing, deliver } = this.state;
        const { userData, orderFollow } = this.props;

        let order = orderFollowing ? orderFollowing : orderFollow;

        if (order && deliver) {
          if(!order.ratings) {
              order.ratings = {}
          }
          order.ratings[userData.uid] = value;
          order.status = 'delivered';
          DataService.saveObject('order', order.id, order);

          if(!deliver.ratings){
              deliver.ratings = {}
          }
          deliver.ratings[order.id] = value;
          DataService.saveObject('deliver', deliver.id, deliver);
        }

        this.setState({showValoracion:false, orderFollowing: null}, ()=>{
          if (this.orderHandler) this.orderHandler();
          this.onSetOrderFollowing(null);
          this.prevOrderStatus = 'on-round';
          NavigationService.navigate('Products');
        })
    }
    
    componentWillUnmount(){
      this.authHandler();
      this.messageListener();

      if (this.deliverLocationHandler) this.deliverLocationHandler();
      if (this.orderHandler) this.orderHandler();
    }
}

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image:{
    width: wp(75),
    height: wp(75),
    resizeMode: 'contain',
    marginBottom: hp(2)
},
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: hp(Constants.fontSize.xxl),
    fontWeight: "600",
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
  playikiLogo:{
    position:'absolute',
    top:hp(5),
    left:wp(6),
    width:wp(30),
    height: hp(4),
    resizeMode: 'contain',
  },
  playikiSkip:{
    position:'absolute',
    top:hp(5),
    right:wp(6),
    color: Constants.colors.darkGray,
    fontSize: hp(Constants.fontSize.ll),
    fontWeight: "300",
  }
});

function mapStateToProps(state, props) {
  return {
      loading            : state.userDataReducer.loading,
      userData           : state.userDataReducer.userData,
      menuOpen           : state.toggleMenuReducer.menuOpen,
      menuDisabled       : state.disableMenuReducer.menuDisabled,
      redirectAfterlogin : state.settingsReducer.redirectAfterlogin,
      orderFollow        : state.orderFollowingReducer.order
  }
}

export default connect(mapStateToProps)(App);
