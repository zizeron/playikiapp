import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { ScrollView, TouchableOpacity, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import OrderService from '../services/OrderService';
import DataService from '../services/DataService';
import Constants from '../config/styles';

import { setOrder } from '../actions'

import ProductAdded from '../components/Modals/ProductAdded';
import MenuIcon from '../components/Menu/MenuIcon';
import CartIcon from '../components/Menu/CartIcon';

const submenuBack = require('../assets/images/icons/productBack.png');
const submenuRefresh = require('../assets/images/icons/refresh.png');
const iconPlus = require('../assets/images/icons/plus.png');

class MenusScreen extends React.Component {
    static navigationOptions = {
        title: 'Productos',
        headerLeft: MenuIcon,
        headerStyle: {backgroundColor: Constants.colors.white},
        headerRight: <CartIcon />
    };
    constructor(props){
        super(props);

        this.state = {
            menus: [],
            productAdded: false,
            error: ''
        }
        this.addingProduct = false;

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);

        this.goBack = this.goBack.bind(this);
        // this.addMenuToCart = this.addMenuToCart.bind(this);
        this.closeProductAdded = this.closeProductAdded.bind(this);
        this.goToOrder = this.goToOrder.bind(this);
        this.loadData = this.loadData.bind(this);
    }    

    componentDidMount(){
        this.loadData()
    }

    loadData(){
        const { userData } = this.props;
        
        if(!userData.bikeId && !userData.beachId){
            this.setState({error: 'No tenemos productos disponibles en tu zona, por favor actualiza tu ubicación'});
            return;
        }

        if(!userData.bikeId && userData.beachId && userData.beachId !== ''){
            this.setState({error: 'Estamos preparando los productos disponibles en tu zona, podrás pedirlos en breve.'});
            return;
        }

        DataService.getCollection('menu').then(
            (result)=>{
                this.setState({ menus: result });
            },
            (error)=>{
                console.log('BEACHES ERROR: ', error);
            }
        );
    }

    goBack(){
        this.props.navigation.navigate('Products')
    }

    enterMenu(menu, menuName){
        this.props.navigation.navigate('MenuDetail',{ menu, menuName });
    }

    closeProductAdded(){
        this.setState({productAdded: false});
    }

    goToOrder(){
        this.props.navigation.navigate('Order');
        this.setState({productAdded: false});
    }

    // addMenuToCart(menu){
    //     if(this.addingProduct) return;

    //     let { order, userData } = this.props;
    //     this.addingProduct = true;

    //     if(!order){
    //         order = OrderService.createNewOrder(userData).then(
    //             (order) => {
    //                 this.onSetOrder(OrderService.addProductToCart(order, menu));
    //                 this.addingProduct = false;
    //             },
    //             (error) => {
    //                 console.log('​MenusScreen -> addMenuToCart -> error', error);
    //                 this.addingProduct = false;
    //             }
    //         )
    //     }else{
    //         this.onSetOrder(OrderService.addProductToCart(order, menu));
    //         this.addingProduct = false;
    //     }
    //     this.setState({productAdded : true})
    // }

    render() {
        const { productAdded, menus, error } = this.state;

        return (
            <ScrollView contentContainerStyle={styles.productsView}>
                <ProductAdded 
                    showModal={productAdded} 
                    onCloseModal={this.closeProductAdded}
                    onGoToOrder={this.goToOrder}
                />
                <View style={styles.submenu}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Image source={submenuBack} style={styles.submenuIcon}/>
                    </TouchableOpacity>
                    <Text style={styles.submenuText}>Menús Exclusivos</Text>
                    <TouchableOpacity onPress={this.loadData}>
                        <Image source={submenuRefresh} style={styles.submenuIcon}/>
                    </TouchableOpacity>
                </View>
                {error != '' && <Text style={styles.errorText}>{error}</Text>}
                {
                    menus.map((menu, i) => {
                        const menuImage = menu.image && menu.image[0] ? {uri: menu.image[0]} : null;
                        return (
                            <View style={styles.menuItem} key={i} >
                                <TouchableOpacity onPress={()=>{ this.enterMenu(menu.id, menu.name) }}>
                                    <Image source={menuImage} style={styles.menuImg}/>
                                </TouchableOpacity>
                                <View style={styles.menuInfo}>
                                    <View style={styles.menuTxt}>
                                        <TouchableOpacity key={i} onPress={()=>{ this.enterMenu(menu.id, menu.name) }}>
                                            <Text style={styles.menuTitle}>{menu.name}</Text>
                                            <Text style={styles.menuDesc}>{menu.desc}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.menuButtons}>
                                        <View style={styles.menuPrice}>
                                            <Text style={styles.menuPriceText}>{menu.totalPVP} €</Text>
                                        </View>
                                        {/* <TouchableOpacity onPress={()=>{ this.addMenuToCart(menu) }} style={styles.menuAdd}>
                                            <Image style={styles.menuAddImg} source={iconPlus}/>
                                        </TouchableOpacity> */}
                                    </View>
                                </View>
                            </View>
                        );
                    })
                }
                
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(MenusScreen);


const styles = StyleSheet.create({
    productsView:{
        backgroundColor: Constants.colors.white,
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingBottom: hp(5),
        paddingTop: hp(10),
        justifyContent: 'center',
    },
    submenu:{
        backgroundColor: Constants.colors.lightGray,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: wp(100),
        marginLeft: wp(-4),
        marginBottom: wp(4),
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingBottom: wp(2),
        paddingTop: hp(2),
    },
    submenuIcon:{
        width: wp(7),
        height: wp(7),
        resizeMode: 'contain'
    },
    submenuText:{
        textAlign: 'center',
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.darkGray,
    },
    menuItem:{
        marginBottom: hp(4)
    },
    menuImg:{
        width:wp(92),
        height: hp(30),
        resizeMode: 'cover',
        borderRadius: 10
    },
    menuInfo:{
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: hp(1.5),
    },
    menuTxt:{
        flex: 1,
        marginRight: wp(4),
    },
    menuTitle:{
        fontSize: hp(Constants.fontSize.ll),
        fontWeight: '300',
        color: Constants.colors.darkGray,
    },
    menuDesc:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        flexWrap: 'wrap',
        color: Constants.colors.textGray,
    },
    menuButtons:{
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    menuPrice:{
        padding: wp(5),
        backgroundColor: Constants.colors.pink,
        borderRadius: hp(5),
        marginRight: wp(2),
    },
    menuPriceText:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.white,
    },
    menuAdd:{
        padding: wp(4),
        borderRadius: wp(15),
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constants.colors.lightBlue,
    },
    menuAddImg:{
        width: wp(6),
        height: wp(6),
    },
    errorText:{
        textAlign: 'center',
        fontSize: hp(Constants.fontSize.m),
        marginVertical: hp(10),
        paddingHorizontal: wp(20),
        width: wp(100),
        color: Constants.colors.textGray,
    },
});