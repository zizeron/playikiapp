import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { ScrollView, TouchableOpacity, View, Image, StyleSheet, Text, FlatList } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import OrderService from '../services/OrderService';
import DataService from '../services/DataService';
import Constants from '../config/styles';

import { setOrder } from '../actions'

import Select from '../components/Input/Select';
import Loading from '../components/Common/Loading';
import MenuIcon from '../components/Menu/MenuIcon';
import CartIcon from '../components/Menu/CartIcon';
import ProductAdded from '../components/Modals/ProductAdded';
import ProductOutOfStock from '../components/Modals/ProductOutOfStock';

const submenuBack = require('../assets/images/icons/productBack.png');
const submenuRefresh = require('../assets/images/icons/refresh.png');
const iconPlus = require('../assets/images/icons/plus.png');


class CategoryScreen extends React.Component {
    static navigationOptions = {
        title: 'Productos',
        headerLeft: MenuIcon,
        headerStyle: {backgroundColor: Constants.colors.white},
        headerRight: <CartIcon />
    };
    constructor(props){
        super(props);

        this.state = {
            products          : [],
            productAdded      : false,
            loading           : false,
            error             : '',
            productOutOfStock : false,
            categoryFilter    : 'todos',
            categoryFilters   : []
        }

        this.addingProduct = false;

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);

        this.closeProductAdded = this.closeProductAdded.bind(this);
        this.goToOrder = this.goToOrder.bind(this);
        this.goBack = this.goBack.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.enterProduct = this.enterProduct.bind(this);
        this.loadData = this.loadData.bind(this);
        this.closeProductOutOfStock = this.closeProductOutOfStock.bind(this);
        this.addProductToOrder = this.addProductToOrder.bind(this);
    }    

    componentDidMount(){
        this.loadData();
    }

    loadData(){
        this.setState({loading: true, error: ''});
        const { userData } = this.props;
        this.catName = this.props.navigation.getParam('categoryName', '');

        if(!userData.bikeId && !userData.beachId){
            this.setState({loading: false, error: 'No tenemos productos disponibles en tu zona, por favor actualiza tu ubicación'});
            return;
        }
        if(!userData.bikeId && userData.beachId && userData.beachId !== ''){
            this.setState({loading: false, error: 'Estamos preparando los productos disponibles en tu zona, podrás pedirlos en breve.'});
            return;
        }

        const cat = this.props.navigation.getParam('category', null);
        const filter = {field: 'category', cond: '==', value: cat};

        DataService.getProducts(userData,[filter]).then(
            (result)=>{
                let categoryFilters = [{label:'Todos', value:'todos'}];

                result.forEach((prod) => {
                    if(!categoryFilters.some((f) => f.value == prod.subcategory)) {
                        categoryFilters.push({label: prod.subcategory, value: prod.subcategory})
                    }
                });
                this.setState({ products: result, loading: false, categoryFilters });
            },
            (error)=>{
                this.setState({ error, loading: false });
                console.log('No products: ', error);
            }
        );
    }

    goBack(){
        this.props.navigation.navigate('Products')
    }

    addToCart(product){
        if(this.addingProduct) return;

        let { order, userData } = this.props;
        this.addingProduct = true;

        if(!order){
            order = OrderService.createNewOrder(userData).then(
                (order) => {
                    this.addProductToOrder(userData, order, product);
                },
                (error) => {
                    console.log('​MenusScreen -> addMenuToCart -> error', error);
                    this.addingProduct = false;
                }
            )
        }else{
            this.addProductToOrder(userData, order, product);
        }
        this.loadData();
        this.setState({productAdded : true})        
    }

    addProductToOrder(userData, order, product){
        OrderService.addProductToCart(userData, order, product).then(
            (order) => {
                this.onSetOrder(order);
                this.addingProduct = false;
            },
            (error) => {
                console.log('​CategoryScreen -> addToCart -> error', error);
                if(error == 'no-stock'){
                    this.setState({productOutOfStock: true});
                }
            }
        );
    }

    enterProduct(product, productName){
        this.props.navigation.navigate('ProductDetail',{ product, productName });
    }

    closeProductAdded(){
        this.setState({productAdded: false});
    }
    closeProductOutOfStock(){
        this.setState({productOutOfStock: false});
    }

    goToOrder(){
        this.props.navigation.navigate('Order');
        this.setState({productAdded: false});
    }

    render() {
        const { productAdded, productOutOfStock, products, loading, error, categoryFilters, categoryFilter } = this.state;

        let dataProducts = products;
        if(categoryFilter && categoryFilter !== 'todos'){
            dataProducts = products.filter(p => p.subcategory == categoryFilter);
        }

        return (
            <ScrollView contentContainerStyle={styles.productsView}>
                <ProductAdded 
                    showModal={productAdded} 
                    onCloseModal={this.closeProductAdded}
                    onGoToOrder={this.goToOrder}
                />
                <ProductOutOfStock 
                    showModal={productOutOfStock} 
                    onCloseModal={this.closeProductOutOfStock}
                />
                <View style={styles.submenu}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Image source={submenuBack} style={styles.submenuIcon}/>
                    </TouchableOpacity>
                    <Text style={styles.submenuText}>{this.catName}</Text>
                    <TouchableOpacity onPress={this.loadData}>
                        <Image source={submenuRefresh} style={styles.submenuIcon}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.categoryFilter}>
                    {categoryFilters.length > 0 &&  <Select 
                        placeholder={{}}
                        items={categoryFilters}
                        onValueChange={(value) => {
                            this.setState({ categoryFilter: value });
                        }}
                        value={categoryFilter}
                    />}
                </View>
                {loading ? 
                    <Loading /> :
                    <View style={styles.categoryItems}>
                        {error != '' && <Text style={styles.errorText}>{error}</Text>}
                        <FlatList 
                            removeClippedSubviews={true}
                            data={dataProducts}
                            numColumns={2}
                            contentContainerStyle={{paddingTop: wp(2)}}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item }) => {
                                const prodImage = item.image && item.image[0] && {uri : item.image[0]};

                                return (
                                    <View key={item.id} style={styles.prodItem}>
                                        <TouchableOpacity onPress={()=>{this.enterProduct(item.id, item.name)}}>
                                            <Image source={prodImage} style={styles.prodImg}/>
                                        </TouchableOpacity>
                                        <View style={styles.prodInfo}>
                                            <TouchableOpacity style={styles.prodTitleContainer} onPress={()=>{this.enterProduct(item.id, item.name)}}>
                                                <Text style={styles.prodTitle}>{item.name}</Text>
                                            </TouchableOpacity>
                                            <View style={styles.prodButtons}>
                                                {item.qty > 0 && 
                                                    <TouchableOpacity style={styles.prodAdd} onPress={()=>{this.addToCart(item)}}>
                                                        <Image style={styles.prodAddImg} source={iconPlus}/>
                                                    </TouchableOpacity>
                                                }
                                            </View>
                                        </View>
                                        {item.qty <= 3 && 
                                            <View style={styles.stockAlert}>
                                                {item.qty > 0 && <Text style={styles.stockAlertText}>Menos de 3 ud.</Text>}
                                                {item.qty <= 0 && <Text style={styles.stockAlertText}>No disp.</Text>}
                                            </View>    
                                        }
                                        <View style={styles.prodPrice}>
                                            <Text style={styles.prodPriceText}>{item.PVP}€</Text>
                                        </View>
                                    </View>
                                );
                            }}
                        />
                    </View>
                }
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(CategoryScreen);


const styles = StyleSheet.create({
    productsView:{
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(5),
        paddingTop: hp(10),
        justifyContent: 'center',
    },
    submenu:{
        backgroundColor: Constants.colors.lightGray,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: wp(100),
        marginBottom: wp(4),
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingBottom: wp(2),
        paddingTop: hp(2),
    },
    submenuIcon:{
        width: wp(7),
        height: wp(7),
        resizeMode: 'contain'
    },
    submenuText:{
        textAlign: 'center',
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.darkGray,
    },
    categoryFilter:{
        marginTop: hp(1),
        marginBottom: hp(2),
    },
    categoryItems:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    errorText:{
        textAlign: 'center',
        fontSize: hp(Constants.fontSize.m),
        marginVertical: hp(10),
        paddingHorizontal: wp(20),
        width: wp(100),
        color: Constants.colors.textGray,
    },
    prodItem:{
        width: wp(42),
        marginLeft: wp(4),
        marginRight: wp(4),
        marginBottom: hp(7)
    },
    prodImg:{
        backgroundColor: Constants.colors.lightGray,
        width:wp(42),
        height: hp(22),
        resizeMode: 'cover',
        borderRadius: 10
    },
    prodInfo:{
        width: wp(42),
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: hp(2),
    },
    prodTitleContainer:{
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    prodTitle:{
        fontSize: hp(Constants.fontSize.l),
        width: wp(30),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        marginRight: wp(2),
    },
    prodButtons:{
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    prodPrice:{
        paddingHorizontal: wp(4),
        paddingVertical: wp(2),
        backgroundColor: Constants.colors.pink,
        borderRadius: hp(5),
        marginRight: wp(2),
        position:'absolute',
        top: hp(22) - wp(10),
        left: wp(1),
    },
    prodPriceText:{
        fontSize: hp(Constants.fontSize.lm),
        color: Constants.colors.white,
    },
    prodAdd:{
        padding: wp(2),
        borderRadius: wp(15),
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constants.colors.lightBlue,
    },
    prodAddImg:{
        width: wp(5),
        height: wp(5),
    },
    stockAlert:{
        position:'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        top: wp(2),
        left: -1 * wp(2),
        padding: wp(2),
        width: wp(14),
        height: wp(14),
        borderRadius: wp(7),
        backgroundColor: Constants.colors.red
    },
    stockAlertText:{
        fontSize: hp(Constants.fontSize.xs),
        color: Constants.colors.white,
        textAlign: 'center'
    }
});