import React from 'react';
import { connect } from 'react-redux';
import { ScrollView, TouchableOpacity, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import Constants from '../config/styles';

import MenuIcon from '../components/Menu/MenuIcon';
import CartIcon from '../components/Menu/CartIcon';

const menusImg = require('../assets/images/temp/menus.png');
const comidaImg = require('../assets/images/temp/comida.png');
const proteccionImg = require('../assets/images/temp/proteccion.png');
const ocioImg = require('../assets/images/temp/ocio.png');
const logoImg = require('../assets/images/other/logo.png');

class ProductsScreen extends React.Component {
    static navigationOptions = {
        title: 'Productos',
        headerLeft: MenuIcon,
        headerStyle: {backgroundColor: Constants.colors.white},
        headerRight: <CartIcon />
    };
    constructor(props){
        super(props);

        this.goToMenus = this.goToMenus.bind(this);
        this.goToCategory = this.goToCategory.bind(this);
    }

    goToMenus(){
        this.props.navigation.navigate('Menus');
    }

    goToCategory(category, categoryName){
        this.props.navigation.navigate('Category',{ category, categoryName });
    }


    render() {
        return (
            <ScrollView contentContainerStyle={styles.productsView}>
                <TouchableOpacity onPress={this.goToMenus} style={styles.menus}>
                    <Image source={menusImg} style={styles.menusImg}/>
                    <Text style={styles.menusTxt}>MENÚS EXCLUSIVOS</Text>
                </TouchableOpacity>
                {/* <Image source={logoImg} style={styles.logoImg}/> */}
                <View style={styles.otherProducts}>
                    <TouchableOpacity onPress={()=>this.goToCategory('food_and_drinks', 'Food and Drinks')} style={styles.otherProductsItem}>
                        <Image source={comidaImg} style={styles.otherProductsItemImg}/>
                        <Text style={styles.menusTxt}>COMIDA Y REFRESCOS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.goToCategory('protection', 'Protección e Higiene')} style={styles.otherProductsItem}>
                        <Image source={proteccionImg} style={styles.otherProductsItemImg}/>
                        <Text style={styles.menusTxt}>PROTECCIÓN E HIGIENE</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.otherProductsCenter}>
                    <TouchableOpacity onPress={()=>this.goToCategory('ocio', 'Ocio')} style={styles.otherProductsItem}>
                        <Image source={ocioImg} style={styles.otherProductsItemImg}/>
                        <Text style={styles.menusTxt}>OCIO</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(ProductsScreen);


const styles = StyleSheet.create({
    productsView:{
        backgroundColor: Constants.colors.lightGray,
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingBottom: wp(4),
        paddingTop: hp(13),
        justifyContent: 'center',
    },
    menus:{

    },
    menusImg:{
        width:wp(92),
        height: hp(30),
        resizeMode: 'cover',
        borderRadius: 5
    },
    menusTxt:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginTop: hp(1),
        flexWrap: 'wrap'
    },
    logoImg:{
        width: wp(30),
        height: wp(35),
        resizeMode: 'contain',
        marginTop: hp(3),
        marginBottom: hp(3),
        marginLeft: wp(31),
    },
    otherProducts:{
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        paddingHorizontal: wp(5),
        marginTop: hp(1),
    },
    otherProductsItem:{
        width: wp(30),
        flexDirection: 'column',
        justifyContent: 'center',
    },
    otherProductsItemImg:{
        width: wp(30),
        height: wp(30),
        resizeMode: 'contain',
    },
    otherProductsCenter:{
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
});