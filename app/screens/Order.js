import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { ScrollView, TouchableOpacity, View, Image, StyleSheet, Text } from 'react-native';

import { setOrder } from '../actions';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import OrderService  from '../services/OrderService'
import DataService from '../services/DataService';

import Constants from '../config/styles';
import Settings from '../config/settings';
import Button from '../components/Buttons/Button';
import Input from '../components/Input/Input';
import ProductOutOfStock from '../components/Modals/ProductOutOfStock';

const shipping = require('../assets/images/icons/shipping.png');
const iconMinus = require('../assets/images/icons/minus_red.png');
const iconPlus = require('../assets/images/icons/plus_blue.png');
const location = require('../assets/images/icons/location.png');


class OrderScreen extends React.Component {
    static navigationOptions = {
        title: 'Tu pedido',
        headerStyle: {backgroundColor: Constants.colors.gray},
    };
    constructor(props){
        super(props);

        this.state = {
            minsLeft          : 0,
            secondsLeft       : 0,
            orderStatus       : 'pending',
            beach             : {},
            productOutOfStock : false,
            couponId          : '',
            coupon            : null,
            couponError       : ''
        }

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);

        this.timeoutOrder = this.timeoutOrder.bind(this);
        this.addProductToCart = this.addProductToCart.bind(this);
        this.removeProductFromCart = this.removeProductFromCart.bind(this);
        this.getOrderTotal = this.getOrderTotal.bind(this);
        this.pay = this.pay.bind(this);
        this.closeProductOutOfStock = this.closeProductOutOfStock.bind(this);
        this.checkCoupon = this.checkCoupon.bind(this);
    }  

    componentDidUpdate(prevProps){
        const { userData, order } = this.props;
        const prevUser = prevProps.userData;

        if(userData && userData.uid && !prevUser.uid){
            OrderService.setOrderUser(order, userData)
            this.onSetOrder(order);
            this.pay();
        }
    }

    componentDidMount(){
        const { order, userData } = this.props;
        
        if(order){
            const {minsLeft, secondsLeft} = OrderService.getOrderTimeLeft(order);
            
            if(minsLeft > 0 || secondsLeft > 0){
                this.setState({
                    minsLeft , 
                    secondsLeft: secondsLeft< 10 ? `0${secondsLeft}` : secondsLeft 
                })

                this.timeInterval = setInterval(()=>{
                    const {minsLeft, secondsLeft} = OrderService.getOrderTimeLeft(order);

                    if(minsLeft <= 0 && secondsLeft <= 0){
                        this.timeoutOrder(order);
                    }else{
                        this.setState({
                            minsLeft , 
                            secondsLeft: secondsLeft< 10 ? `0${secondsLeft}` : secondsLeft 
                        })
                    }
                },1000);
            }else{
                this.timeoutOrder(order, true);
            }

            DataService.getObject('beach', userData.beachId).then(
                (result) => {
                    this.setState({beach: result});
                },
                (error) => {
                    console.log('​OrderScreen -> componentDidMount -> error', error);
                }
            )
        }
    }

    timeoutOrder(order, force){
        this.setState({ orderStatus:'timeout' })
        OrderService.setOrderTimeout(order);
        this.onSetOrder(null);
        clearInterval(this.timeInterval);
    }

    pay(){
        const { userData, order } = this.props;
        const { couponId, coupon } = this.state;

        console.log('​OrderScreen -> pay -> order', order);
        OrderService.setOrderStatus(order, 'pending-payment').then(
            (result) => {
                if(userData.uid && order.userId){
                    this.props.navigation.navigate('Payment', {total: this.getOrderTotal(), couponId: (coupon ? couponId : '') });
                }else{
                    this.props.navigation.navigate('Login',{prevScreen: 'Order', message: 'Para finalizar tu pedido debes hacer login o registrarte', total: this.getOrderTotal()});
                }
            },
            (error) => {
                console.log('​OrderScreen -> pay -> error', error);
            }
        );
    }

    addProductToCart(product){
        const { order, userData } = this.props;

        let orderFunction = OrderService.addProductToCart;
        if(product.totalPVP){ //ES MENU
            orderFunction = OrderService.addMenuToCart;
        }

        orderFunction(userData, order, product).then(
            (order) => {
                this.onSetOrder(order);
                this.addingProduct = false;
            },
            (error) => {
                console.log('​CategoryScreen -> addToCart -> error', error);
                if(error == 'no-stock'){
                    this.setState({productOutOfStock: true});
                }
            }
        );
    }

    removeProductFromCart(product){
        const { order, userData } = this.props;
        const newOrder = OrderService.removeProductFromCart(userData, order, product);
        if(!newOrder.orderLines || newOrder.orderLines.length <= 0){
            OrderService.setOrderCancel(order);
            this.onSetOrder(null);
            clearInterval(this.timeInterval);
        } else {
            this.onSetOrder(newOrder);
        }
    }

    getOrderTotal(){
        const { order } = this.props;
        const { beach } = this.state;

        if(order && beach){
            return OrderService.getOrderTotal(order, beach.shippingFee);
        }

        return '-';
    }

    closeProductOutOfStock(){
        this.setState({productOutOfStock: false});
    }

    async checkCoupon(){
        const { couponId } = this.state;
        const { order } = this.props;

        if (couponId === '') {
            this.setState({couponError: 'Cupón no válido'});
            return;
        }

        this.setState({couponError: '', coupon: null});

        const coupon = await fetch(`https://api.stripe.com/v1/coupons/${couponId}`,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-forsm-urlencoded',
                'Authorization':'Bearer sk_live_qyz4gJ7WDl8Lik2AXMpukrvQ' //pk_test_1iMKCEQzySXYGXUHcx651lvg
            }
        })
        .then((response) => response.json())
        .then((responseJson) => responseJson)
        .catch((error) => {
            this.setState({couponError: 'Cupón no válido'});
            console.error(error);
        });

        if(coupon.error || !coupon.valid) {
            this.setState({couponError: 'Cupón no válido'});
            order.coupon = null;
        }else{
            this.setState({couponError: '', coupon});
            order.coupon = {
                amount_off: coupon.amount_off,
                id: coupon.id,
                percent_off: coupon.percent_off,
            }
        }
        this.onSetOrder(order);
    }

    render() {
        const {minsLeft, secondsLeft, orderStatus, beach, productOutOfStock, coupon, couponId, couponError} = this.state;
        const { order } = this.props;
        
        if(orderStatus === 'timeout'){
            return (               
                <View style={styles.noOrder}>
                     <Text  style={styles.orderTimeoutTitle}>¡Pedido caducado!</Text>
                     <Text  style={styles.orderTimeoutDesc}>Lo sentimos, han pasado los 5 minutos disponibles para realizar tu pedido. Inténtalo de nuevo.</Text>
                 </View>
             );

        }else if(!order){
            return (
                <View style={styles.noOrder}>
                    <Text  style={styles.noOrderText}>No has añadido productos a tu pedido</Text>
                </View>
            );            
        }else{
            return (
                <ScrollView contentContainerStyle={styles.orderView}>
                    <ProductOutOfStock 
                        showModal={productOutOfStock} 
                        onCloseModal={this.closeProductOutOfStock}
                    />
                    {order.orderLines.map((ordLine, i) => {
                        const prodImg = {uri: ordLine.product.image[0]}
                        return (
                            <View style={styles.orderItem} key={i}>
                                <Image source={prodImg} style={styles.orderItemPicture}/>
                                <View style={styles.orderItemInfo}>
                                    <Text style={styles.orderItemTitle}>{ordLine.product.name}</Text>
                                    {ordLine.product.selection && <Text style={styles.orderItemSelection}>{ordLine.product.selection.label}</Text>}
                                    <Text style={styles.orderItemPrice}>{ordLine.product.PVP || ordLine.product.totalPVP }€</Text>
                                    <View style={styles.orderItemButtons}>
                                        <TouchableOpacity onPress={()=>{ this.removeProductFromCart(ordLine.product) }}>
                                            <View style={styles.orderItemMinus}>
                                                <Image source={iconMinus} style={styles.orderItemIcon}/>
                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.orderitemQty}>{ordLine.qty}</Text>
                                        {ordLine.qty < 20 && 
                                            <TouchableOpacity onPress={()=>{ this.addProductToCart(ordLine.product) }}>
                                                <View style={styles.orderItemPlus}>
                                                    <Image source={iconPlus} style={styles.orderItemIcon}/>
                                                </View>
                                            </TouchableOpacity>
                                        }
                                    </View>
                                </View>
                            </View>
                        )
                    })} 
                    <View style={styles.orderItem}>
                        <Image source={shipping} style={styles.orderItemShipping}/>
                        <View style={styles.orderItemInfo}>
                            <Text style={styles.orderItemTitle}>Gastos de envío</Text>
                            <View style={styles.shippingBeach}>
                                <Text style={styles.shippingBeachName}>{beach ? beach.name : ''}</Text>
                                <Image style={styles.shippingBeachIcon} source={location} />
                            </View>
                            <Text style={styles.orderItemPrice}>{beach ? beach.shippingFee: 1}€</Text>
                        </View>
                    </View>
                    {couponError != '' && <Text style={styles.couponError}>{couponError}</Text>}
                    <View style={styles.couponForm}>
                        <Input 
                            value={couponId} 
                            onInputChange={(f,v) => {this.setState({couponId: v.toUpperCase()})}} 
                            field="couponId"
                            placeholder="Cupón de descuento"
                            width={45}
                            marginLeft={0}
                        />
                        <Button 
                            type="pink"
                            onButtonPress={this.checkCoupon} 
                            text={'VALIDAR'} 
                            marginLeft={0}
                            width={30}
                        />  
                    </View>
                    <View style={styles.orderSummary}>
                        <Text style={styles.orderSummaryItem}>TOTAL: <Text style={styles.orderSummaryItemValue}>{this.getOrderTotal()}€</Text></Text>
                        <Text style={styles.orderSummaryItem}>TIEMPO RESTANTE: <Text style={styles.orderSummaryItemValue}>{minsLeft}:{secondsLeft}</Text></Text>
                        <View style={styles.orderSummaryButton}>
                            <Button 
                                type="pink"
                                onButtonPress={this.pay} 
                                text={'FINALIZAR'} 
                                marginLeft={0}
                            />
                        </View>
                    </View>
                </ScrollView>
            );
        }
        
        
    }

    componentWillUnmount(){
        clearInterval(this.timeInterval);
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(OrderScreen);


const styles = StyleSheet.create({
    noOrder: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(10),
    },
    noOrderText:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center'
    },
    orderTimeoutTitle:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center',
        marginBottom: hp(1)
    },
    orderTimeoutDesc:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.m),
        textAlign: 'center'
    },
    orderView:{
        backgroundColor: Constants.colors.white,
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingBottom: hp(5),
        paddingTop: hp(16),
        justifyContent: 'center',
    },
    orderItem:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: hp(5)
    },
    orderItemPicture:{
        width: wp(30),
        height: wp(30),
        borderRadius: wp(15),
        marginRight: wp(5),
        borderWidth: 1,
        borderColor: Constants.colors.textGray,
    },
    orderItemShipping:{
        width: wp(15),
        height: wp(16),
        marginLeft: wp(7),
        marginRight: wp(13),
        resizeMode: 'contain'
    },
    shippingBeach:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    shippingBeachIcon:{
        width: wp(6),
        height: wp(6),
        marginLeft: wp(2),
        resizeMode: 'contain'
    },
    shippingBeachName:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.m)
    },
    orderItemInfo:{
        flexDirection:'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: wp(60)
    },
    orderItemTitle:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.l),
        marginBottom: hp(0.5)
    },
    orderItemSelection:{
        color: Constants.colors.textGray,
        fontSize: hp(Constants.fontSize.m),
        marginBottom: hp(0.5)
    },
    orderItemPrice:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.ll),
        marginBottom: hp(2)
    },
    orderItemButtons:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    orderItemMinus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightRed,
        borderRadius: wp(4)
    },
    orderitemQty:{
        fontSize: hp(Constants.fontSize.l),
        marginRight: wp(5),
        marginLeft: wp(5),
    },
    orderItemPlus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightBlue,
        borderRadius: wp(4)
    },
    orderSummary:{
        marginTop: hp(3),
        flexDirection:'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    orderSummaryItem:{
        fontSize: hp(Constants.fontSize.xl),
        marginBottom: hp(2),
        color: Constants.colors.darkGray
    },
    orderSummaryButton:{
        marginTop: hp(2),  
    },
    orderSummaryItemValue:{
        color: Constants.colors.pink
    },
    couponError:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.ll),
        marginBottom: hp(1),
        textAlign: 'center',
    },
    couponForm:{
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: wp(80),
        marginLeft: wp(5),
        marginBottom: hp(1)
    }
});