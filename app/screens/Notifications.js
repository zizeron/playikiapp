import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, View, Image, ImageBackground, StyleSheet, Text, ScrollView } from 'react-native';
import firebase from 'react-native-firebase';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import DataService from '../services/DataService';
import Constants from '../config/styles';

import MenuIcon from '../components/Menu/MenuIcon';
import Loading from '../components/Common/Loading';

const bag = require('../assets/images/icons/bag.png');

class NotificationsScreen extends React.Component {
    static navigationOptions = {
        title: 'Notificaciones',
        headerLeft: MenuIcon,
        headerStyle: {backgroundColor: Constants.colors.gray},
    };

    constructor(props){
        super(props);

        this.state = {
            notifications: [],
            loading: true
        }
    }

    componentDidMount(){
        const { userData } = this.props;
        console.log('​NotificationsScreen -> componentDidMount -> userData', userData);

        firebase.firestore()
            .collection('users')
            .doc(userData.uid)
            .collection('notification')
            .onSnapshot((querySnapshot) => {
                let notifications = [];

                querySnapshot.forEach((d) => {
                    notifications.push(Object.assign({ id:d.id }, d.data() ))
                });
               
                notifications = notifications.sort((a,b) => {
                    return b.date - a.date;
                })
                console.log('​NotificationsScreen -> componentDidMount -> notifications', notifications);

                this.setState({notifications, loading: false});
            });
    }

    render() {
        const { notifications, loading } = this.state;

        if(loading) return <Loading />;

        if(notifications.length <= 0){
            return (
                <View style={styles.noNotifications}>
                    <Text  style={styles.noOrderText}>No tienes notificaciones</Text>
                </View>
            ); 
        }

        return (
            <ScrollView contentContainerStyle={styles.mainView}>
                {notifications.map((o, i) => {
                    const orderDate = new Date(o.date);
                    const orderMonth = orderDate.getMonth()<9 ? `0${orderDate.getMonth()+1}`:orderDate.getMonth()+1;
                    const orderDateText = `${orderDate.getDate()}/${orderMonth}/${orderDate.getFullYear()}`;

                    let orderId = `PEDIDO ${orderDate.getDate()}${orderMonth}${o.id.slice(-4)}`;

                    let orderStatus = '';
                    let statusColor = styles.noStatus;
                    if(o.order){
                        if(o.order.status == 'paid'){
                            orderStatus = 'Pedido en curso';
                            statusColor = styles.orderStatusCurso;
                            
                        } else if(o.order.status == 'delivered'){
                            orderStatus = 'Pedido entregado';
                            statusColor = styles.orderStatusEntregado;
                            
                        } else if(o.order.status == 'cancelled'){
                            orderStatus = 'Pedido cancelado';
                            statusColor = styles.orderStatusCancel;
                        }
                    }else{
                        orderId = 'REGISTRO'
                    }

                    return (
                        <View key={i} style={styles.item}>
                            <View style={styles.itemTop}>
                                <View style={styles.itemTopLeft}>
                                    <Image style={styles.iconBag} source={bag}/>
                                    <Text style={styles.itemTextBig}>{orderDateText}</Text>
                                </View>
                                <Text style={styles.itemTextBig}>{orderId}</Text>
                            </View>
                            <View style={styles.itemBottom}>
                                <Text style={styles.itemTextSmall}>{o.message}</Text>
                                <View style={statusColor}>
                                    {o.order && <Text style={styles.orderStatusText}>{orderStatus}</Text>}
                                </View>
                            </View>
                        </View>
                    );
                })}
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(NotificationsScreen);


const styles = StyleSheet.create({
    noNotifications:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(1),
    },
    noOrderText:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center'
    },
    mainView:{
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(5),
        paddingTop: hp(14),
        paddingHorizontal: wp(2),
        justifyContent: 'center',
    },
    item:{
        borderWidth: 2,
        borderRadius: 15,
        borderColor: Constants.colors.gray,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp(2),
        backgroundColor: Constants.colors.white,
        shadowColor: Constants.colors.textGray,
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 2,
        shadowOpacity: 0.5,
    },
    itemTop:{
        width: wp(95),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: wp(2),
        paddingHorizontal: wp(4),
        borderBottomWidth: 1,
        borderColor: Constants.colors.gray,
    },
    itemTopLeft:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconBag:{
        width: wp(10),
        height: wp(10),
        marginRight: wp(3)
    },
    itemTextBig:{
        fontSize: hp(Constants.fontSize.ll),
        fontWeight: '300',
        color: Constants.colors.darkGray,
        textTransform: 'uppercase'
    },
    itemTextSmall:{
        fontSize: hp(Constants.fontSize.ml),
        color: Constants.colors.darkGray,
        fontWeight: '300',
    },
    itemBottom:{
        width: wp(95),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: wp(2),
        paddingHorizontal: wp(4),
    },
    orderStatusCurso:{
        width: wp(25),
        backgroundColor: Constants.colors.orderGreen,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    orderStatusEntregado:{
        width: wp(25),
        backgroundColor: Constants.colors.orderPink,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    orderStatusCancel:{
        width: wp(25),
        backgroundColor: Constants.colors.orderRed,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    noStatus:{
        width: wp(1),
        paddingHorizontal: wp(1),
        paddingVertical: wp(5),
        borderRadius: 100
    },
    orderStatusText:{
        textAlign:'center',
        color: Constants.colors.white,
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300'
    }
});