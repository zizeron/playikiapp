import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { ScrollView, TouchableOpacity, View, Image, StyleSheet, Text,ImageBackground  } from 'react-native';
import firebase from 'react-native-firebase';

import { setOrder, setOrderFollowing } from '../actions';
import OrderService from '../services/OrderService';
import DataService from '../services/DataService';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';

import Constants from '../config/styles';
import Button from '../components/Buttons/Button';
import MenuIcon from '../components/Menu/MenuIcon';
import Loading from '../components/Common/Loading';
import Repartidor from '../components/Common/Repartidor';

const confirmIcon = require('../assets/images/icons/paymentConfirm.png');
const notAvailable = require('../assets/images/icons/notAvailable.png');
const userDefault = require('../assets/images/other/default-user.jpg');

class PaymentConfirmScreen extends React.Component {
    static navigationOptions = {
        title: 'Confirmación de pago',
        headerStyle: {backgroundColor: Constants.colors.gray},
        headerLeft: MenuIcon,
    };

    constructor(props){
        super(props);
        
        this.state = {
            loading: true,
            chargeId      : props.navigation.getParam('chargeId', 'error'),
            status        : props.navigation.getParam('status', 'error'),
            statusMessage : props.navigation.getParam('statusMessage', 'error'),
            charge        : null,
            deliver       : null
        }

        this.order = props.navigation.getParam('order', null);

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);
        this.onSetOrderFollowing = bindActionCreators(setOrderFollowing, props.dispatch);
        
        this.goToOrder = this.goToOrder.bind(this);
    }

    componentDidMount(){
        const { userData } = this.props;
        const { status, chargeId } = this.state;

        if(status !== 'success' || !chargeId){
            this.setState({loading: false});
        }else{
            firebase.firestore()
                .collection('stripe_customers')
                .doc(userData.uid)
                .collection('charges')
                .doc(chargeId)
                .onSnapshot((doc) => {
                    const data = doc.data();
                    if(data.paid == true){
                        OrderService.setOrderPayment(this.order, 'paid', data);
                        this.setState({
                            status  : 'success',
                            charge  : data,
                            loading : false,
                            order   : this.order
                        });
                        this.onSetOrderFollowing(this.order);
                        this.onSetOrder(null);
                    
                    }else if(data.error){
                        this.setState({
                            status  : 'error',
                            loading : false,
                            statusMessage: 'Ocurrió un error inesperado'
                        });
                    }
                });

            DataService.getUserDeliver(userData).then(
                (deliver) => {
                    this.setState({deliver})
                },
                (error) => {
                    console.log('​PaymentConfirmScreen -> componentDidMount -> error', error);
                    
                }
            )
        }
    }

    goToOrder(){
        const { order } = this.state;
        this.props.navigation.navigate('ViewOrder',{orderId: order.id});
    }

    render(){
        const { userData } = this.props;
        const { status, statusMessage, order, loading, charge, deliver} = this.state;
        const orderItems = OrderService.getOrderItems(order)

        if(loading || !userData) return <Loading />;
        
        const userName = userData.name ? ` ${userData.name}` : '';
        const paymentSucess = status == 'success';

        let userPicture = userData.picture ? {uri: `${userData.picture}?height=${parseInt(hp(50))}`} : userDefault;

        const repartidor = {
            deliver: deliver ? deliver.deliver : '', 
            phone: deliver ? deliver.phone : '', 
            timeEst: '20 MIN', 
            playikid: charge && charge.playId ? charge.playId : ''
        }

        return(
            <ScrollView contentContainerStyle={styles.mainView}>
                {paymentSucess ? 
                    <Image style={styles.iconImg} source={confirmIcon}/> :
                    <Image style={styles.iconImg} source={notAvailable}/>
                }
                <Image style={styles.userAvatar} source={userPicture}/>
                <Text style={styles.title}>
                    {paymentSucess ? `¡Bravo${userName}!` : 'Error en el pago'}
                </Text>
                <View style={styles.subtitle}>
                    <Text style={styles.subtitleText}>
                        {paymentSucess ? 
                            'Tu pedido se ha realizado correctamente' : 
                            (statusMessage || 'El pago no se ha podido realizar')
                        }
                    </Text>
                </View>
                {paymentSucess && 
                    <View>
                        <View style={styles.sendInfo}>
                            <Text style={styles.subtitleText}>Envío a</Text>
                            <TouchableOpacity>
                                <Text style={styles.beachText}>{userData.beachName}</Text>
                            </TouchableOpacity>
                            <View style={styles.sendInfoBeach}>
                                <TouchableOpacity onPress={this.goToOrder}>
                                    <Text style={styles.subtitleText}>Pedido: <Text style={styles.beachText}>{orderItems} items VER</Text></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.repartidorInfo}>
                            <Repartidor repartidor={repartidor} />
                        </View>
                    </View>
                }
                <View style={styles.button}>
                    {paymentSucess ? 
                        <Button 
                            type="pink"
                            onButtonPress={()=>{this.props.navigation.navigate('FollowOrder',{order})}} 
                            text={'SEGUIR EL PEDIDO'} 
                        /> : 
                        <Button 
                            type="pink"
                            onButtonPress={()=>{this.props.navigation.goBack()}} 
                            text={'VOLVER A INTENTARLO'} 
                        />
                    }
                </View>
            </ScrollView>
        );
    }
}


function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(PaymentConfirmScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(5),
        paddingTop: hp(9),
        justifyContent: 'center',
    },
    iconImg:{
        width: wp(34),
        height: wp(34),
        resizeMode: 'contain',
        marginTop: hp(5),
        marginLeft: wp(33)
    },
    userAvatar:{
        width: wp(20),
        height: wp(20),
        borderRadius: wp(10),
        resizeMode: 'contain',
        marginTop: -1 * hp(5),
        marginLeft: wp(40)
    },
    title:{
        fontSize: hp(Constants.fontSize.xll),
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginTop: hp(2)
    },
    subtitle:{
        marginTop: hp(2)
    },
    subtitleText:{
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.textGray,
        textAlign: 'center',
    },
    sendInfo:{
        marginTop: hp(3)
    },
    repartidorInfo:{
        marginVertical: hp(2),
        marginHorizontal: wp(5),
    },
    sendInfoBeach:{
        marginTop: hp(2)
    },
    beachText:{
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.pink,
        textAlign: 'center',
    },
    button:{
        marginTop: hp(3)
    }
})