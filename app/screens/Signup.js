import React from 'react';
import firebase from 'react-native-firebase';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { View, Text, Image, StyleSheet, TouchableOpacity, ActivityIndicator, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CheckBox from 'react-native-check-box'

import {setSettings, setUserData} from '../actions/index';

import UserService from '../services/UserService';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';

import Constants from '../config/styles';
import phonePrefixes from '../config/phonePrefixes';
import Input from '../components/Input/Input';
import Select from '../components/Input/Select';
import Button from '../components/Buttons/Button';
import FacebookButton from '../components/Buttons/FacebookButton';
import Legal from '../components/Common/Legal';
import ModalBack from '../components/Menu/ModalBack';
import ConfirmPhone from '../components/Modals/ConfirmPhone'
import CodeAutoconfirmed from '../components/Modals/CodeAutoconfirmed';

const signupBg = require('../assets/images/bg-images/signup.png');

class SignupScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({ 
        title: 'Identifícate',
        headerLeft: null,
        headerRight: <ModalBack navigation={navigation}/>,
        headerTransparent: true,
        headerStyle:{
            borderBottomWidth: 0
        },
        gesturesEnabled: false,
        headerBackTitle: null,
        headerTitleStyle: {
          fontSize: hp(Constants.fontSize.l),
          fontWeight: '300',
        },
    })

    constructor(props){
        super(props);

        this.state = {
            name         : '',
            email        : '',
            phone        : '',
            phonePrefix  : '+34',
            formError    : false,
            formErrorText: '',
            accepted     : false,
            loading      : false,
            showModal    : false,
            showModalAutoconf : false,
        }

        this.onSetUserData = bindActionCreators(setUserData, props.dispatch);
        this.onSetSettings = bindActionCreators(setSettings, props.dispatch);

        this.onCloseDeliverModal = this.onCloseDeliverModal.bind(this);
        this.updateLoginField = this.updateLoginField.bind(this);
        this.signup = this.signup.bind(this);
        this.goToLegal = this.goToLegal.bind(this);
        this.goBack = this.goBack.bind(this);
        this.setUser = this.setUser.bind(this);
        this.onCloseAutoconfModal = this.onCloseAutoconfModal.bind(this);
    }

    componentDidMount(){
        this.onSetSettings({redirectAfterlogin: false});
    }

    updateLoginField(field, value){
        let prevState = this.state;
        prevState[field] = value;

        this.setState(prevState);
    }

    signup(){
        this.setState({formError: false, formErrorText: ''});

        const { name, email, phone, phonePrefix, accepted } = this.state;

        if(!email || !phone || !name){
            this.setState({formError: true, formErrorText: 'Email, nombre y teléfono son obligatorios'});
            return
        }

        if(!accepted){
            this.setState({formError: true, formErrorText: 'Debes leer y aceptar la política de privacidad'});
            return
        }

        this.setState({loading: true});
        firebase.auth()
            .verifyPhoneNumber(phonePrefix+phone)
            .on('state_changed', (phoneAuthSnapshot) => {
                console.log('​enter -> phoneAuthSnapshot', phoneAuthSnapshot);
                switch (phoneAuthSnapshot.state) {
                    case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
                        console.log('code sent');
                        if(Platform.OS === 'ios'){
                            this.showConfirmModal(phonePrefix+phone);
                        }
                        break;
                    case firebase.auth.PhoneAuthState.ERROR: // or 'error'
                        console.log('verification error');
                        console.log(phoneAuthSnapshot.error);
                        break;
                    case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
                        console.log('auto verify on android timed out');
                        this.showConfirmModal(phonePrefix+phone);
                        break;
                    case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
                        console.log('auto verified on android');
                        const { verificationId, code } = phoneAuthSnapshot;
                        const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);

                        this.setUser();
                        firebase.auth().signInAndRetrieveDataWithCredential(credential);
                        this.setState({loading: false}, () =>{
                            this.goBack();
                        });

                        break;
                }
            }, (error) => {
                console.log(error);
                console.log(error.verificationId);
            }, (phoneAuthSnapshot) => {
                console.log(phoneAuthSnapshot);
            });
    }

    showConfirmModal(phone){
        firebase.auth()
            .signInWithPhoneNumber(phone)
            .then((confirmationResult) => {
                this.setState({showModal: true, loading: false, confirmationResult})
            }).catch((error) => {
                console.log('NO SMS SENT', error)
                this.setState({loading: false});
            });
    }

    setUser(){
        const { name, email, phone, phonePrefix } = this.state;
        const { userData } = this.props;

        let newUserData = Object.assign({}, {
            name,
            email,
            phone,
            phonePrefix,
            hasRole : false,
            playid  : UserService.getPlayID(email)
        }, userData);

        this.onSetUserData(newUserData);
    }

    onCloseDeliverModal(confirm, userId) {
        const { name, email, phone, phonePrefix } = this.state;
        const { userData } = this.props;

        if(confirm){
            firebase.firestore().collection('users').doc(userId).get().then((doc) => {
                let newUserData = Object.assign({}, {
                    name,
                    email,
                    phone,
                    phonePrefix,
                    uid     : userId,
                    hasRole : false,
                    playid  : UserService.getPlayID(email)
                }, userData);

                UserService.saveUserInfo(newUserData);
                this.onSetUserData(newUserData);
                this.setState({loading: false, showModalAutoconf: true, showModal: false});
            });
        } else {
            this.setState({showModal: false})
        }
    }
    goBack(){
        let prevScreen = this.props.navigation.getParam('prevScreen', null);
        if(!prevScreen){
            prevScreen = 'Products'
        }
        this.props.navigation.navigate(prevScreen);
    }

    goToLegal(){
        this.props.navigation.navigate('Legal');
    }

    onCloseAutoconfModal(){
        this.setState({showModalAutoconf: false});
        this.goBack();
    }

    render() {
        const { accepted, loading, confirmationResult, showModal, showModalAutoconf } = this.state;

        return (
            <KeyboardAwareScrollView contentContainerStyle={styles.mainView}>
                <Image style={styles.bgImage} source={signupBg}></Image>
                <View style={styles.formRow}>
                    <Input 
                        value={this.state.name} 
                        onInputChange={this.updateLoginField} 
                        field="name"
                        placeholder="Nombre"
                    />
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={this.state.email} 
                        onInputChange={this.updateLoginField} 
                        field="email"
                        placeholder="Email"
                    />
                </View>
                <View style={styles.formRow}>
                    <Select 
                        placeholder={{}}
                        items={phonePrefixes}
                        onValueChange={(value) => {
                            this.setState({
                                phonePrefix: value,
                            });
                        }}
                        value={this.state.phonePrefix}
                    />
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={this.state.phone} 
                        onInputChange={this.updateLoginField} 
                        field="phone"
                        placeholder="Teléfono móvil"
                    />
                </View>
                {/* <View style={styles.formRow}>
                    <Input 
                        value={this.state.password} 
                        onInputChange={this.updateLoginField} 
                        field="password" 
                        placeholder="Contraseña"
                        type="password"/>
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={this.state.confPassword} 
                        onInputChange={this.updateLoginField} 
                        field="confPassword" 
                        placeholder="Confirmar contraseña"
                        type="password"/>
                </View> */}
                <View style={styles.formRowCheck}>
                    <CheckBox
                        style={styles.checkBox}
                        onClick={()=>{this.setState({accepted : !accepted})}}
                        isChecked={accepted}
                        checkBoxColor={Constants.colors.textGray}
                    />
                    <TouchableOpacity onPress={this.goToLegal}>
                        <Text style={styles.legalText}>He leído y estoy de acuerdo con la Política de privacidad</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.formError}>
                    <Text style={styles.formErrorText}>&nbsp;{this.state.formErrorText}&nbsp;</Text>
                </View>
                <View style={styles.formRowButton}>
                {loading ? 
                    <ActivityIndicator size="small" color={Constants.colors.pink}/>:
                    <Button 
                        type="pink"
                        onButtonPress={this.signup} 
                        text={'ACEPTAR'} 
                    />
                }
                </View>
                <View style={styles.formRowOr}>
                    <Text style={styles.bottomLinkText}>O si lo prefieres</Text>
                </View>
                <View style={styles.formRowLast}>
                    <FacebookButton
                        onLoginSucess={()=>{ 
                            this.goBack();
                        }}
                    />
                </View>
                <Legal />
                <ConfirmPhone 
                    showModal={showModal} 
                    confirmationResult={confirmationResult}
                    onCloseModal={this.onCloseDeliverModal}
                />
                <CodeAutoconfirmed
                    showModal={showModalAutoconf} 
                    onCloseModal={this.onCloseAutoconfModal}
                />
            </KeyboardAwareScrollView>
        );
    }
}


function mapStateToProps(state, props) {
    return {
        order   : state.orderReducer.order,
        userData: state.userDataReducer.userData,
    }
}
  
export default connect(mapStateToProps)(SignupScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        paddingTop: hp(20),
        
    },
    bgImage: {
        resizeMode: 'cover',
        position: 'absolute',
        top: 0,
        width: wp(100),
        height: hp(70),
    },
    formRow:{
        marginBottom: hp(3)
    },
    formRowCheck:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginHorizontal: wp(15),
        width: wp(65),
    },
    checkbox:{
        padding: 10
    },
    legalText:{
        color: Constants.colors.textGray,
        fontSize: hp(Constants.fontSize.s),
        marginLeft: wp(2),
    },
    formRowButton:{
        marginBottom: hp(2),
    },
    formRowLast:{
        marginBottom: hp(4)
    },
    formRowOr:{
        marginBottom: hp(2)
    },
    bottomLinkText:{
        fontSize:hp(Constants.fontSize.l),
        color: Constants.colors.darkGray,
        textAlign:'center'
    },
    bottomLinkTextPink:{
        color : Constants.colors.pink
    },
    formError: {
        paddingTop: hp(2),
        paddingBottom: hp(2)
    },
    formErrorText: {
        color: Constants.colors.pink,
        textAlign:'center'
    },
});