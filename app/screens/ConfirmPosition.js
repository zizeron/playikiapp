import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { ImageBackground, View, Text, StyleSheet } from 'react-native';
import firebase from 'react-native-firebase';

import UserService from '../services/UserService';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import Constants from '../config/styles';
import { setUserData, disableMenuGesture } from '../actions'

import GoodPosition from '../components/Position/GoodPosition';
import BadPosition from '../components/Position/BadPosition';

import Button from '../components/Buttons/Button';
import Input from '../components/Input/Input';

const homeBg = require('../assets/images/bg-images/home.jpg');

class ConfirmPositionScreen extends React.Component {
    static navigationOptions = {
      title: 'Tu Posición',
      headerStyle: {backgroundColor: Constants.colors.gray},
    };

    constructor(props){
      super(props);

      this.state = {
        
      }
      
      this.onSetUserData = bindActionCreators(setUserData, props.dispatch);
      this.onDisableMenuGesture = bindActionCreators(disableMenuGesture, props.dispatch);
    }

    componentDidMount(){
      this.onDisableMenuGesture(false);
    }


    render() {
      const { userData, navigation } = this.props;
      const hasBike = navigation.getParam('hasBike');
      const insideBeach = navigation.getParam('insideBeach');
      const insideSection = navigation.getParam('insideSection');
      const nearestBeach = navigation.getParam('nearestBeach')

      return (
        <View style={styles.mainView}>
          {insideBeach && insideSection ? 
            <GoodPosition userData={ userData } hasBike={ hasBike } /> : 
            <BadPosition 
              userData      = { userData }
              nearestBeach  = { nearestBeach }
              insideBeach   = { insideBeach }
              insideSection = { insideSection }
              hasBike       = { hasBike }
            />
          }
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    mainView:{
      backgroundColor: '#fff',
      justifyContent: 'space-between',
      flex:1,
      height:hp(100),
      marginTop: hp(10),
    },
    
  });

  function mapStateToProps(state, props) {
    return {
      userData: state.userDataReducer.userData
    }
  }

  export default connect(mapStateToProps)(ConfirmPositionScreen);