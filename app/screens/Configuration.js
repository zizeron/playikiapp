import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { Platform, View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'rn-fetch-blob'

import UserService from '../services/UserService';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';

import { setUserData } from '../actions'; 

import Constants from '../config/styles';
import phonePrefixes from '../config/phonePrefixes';
import Input from '../components/Input/Input';
import Select from '../components/Input/Select';
import CustomPicker from '../components/CustomPicker';
import Button from '../components/Buttons/Button';

const userDefault = require('../assets/images/other/default-user.jpg');

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

phonePrefixes.unshift({label: 'Selecciona', value: ''})

const uploadImage = (uri, uid, self, mime = 'image/jpg') => {
    return new Promise((resolve, reject) => {
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
        let uploadBlob = null
        const storageRef = firebase.storage().ref('users').child(uid+'.jpg')

        self.setState({uploadProgress: 0, uploadError: ''});

        fs.readFile(uploadUri, 'base64')
        .then((data) => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then((blob) => {
            uploadBlob = blob
            console.log('​uploadImage -> uploadBlob', uploadBlob);
            var uploadTask = storageRef.put(uploadBlob._ref);

            uploadTask.on('state_changed', function(snapshot){
                var progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                if(progress < 100){
                    self.setState({uploadProgress: progress});
                }
                
            }, function(error) {
                reject('Ocurrió un error subiendo la foto, por favor intenta de nuevo');

            }, function(snapshot) {
                console.log('​uploadImage -> snapshot', snapshot);
                if(snapshot.downloadURL){
                    resolve(snapshot.downloadURL);
                }else{
                    console.log('​uploadImage -> uploadTask', uploadTask);
                    if(uploadTask && uploadTask.ref && uploadTask.ref.getDownloadURL){
                        uploadTask.ref.getDownloadURL().then(
                            (downloadURL) => {
                                console.log('​uploadImage -> downloadURL', downloadURL);
                                if(downloadURL) {
                                    resolve(downloadURL);
                                }else{
                                    reject('La foto no es válida o no se ha cargado correctamente');
                                }
                            },
                            (error) => {
                                console.log('​uploadImage -> error', error);
                                reject('La foto no es válida o no se ha cargado correctamente');
                            }
                        );
                    }
                }
            });
        //   return imageRef.put(blob, { contentType: mime })
        })
        // .then(() => {
        //   uploadBlob.close()
        //   return imageRef.getDownloadURL()
        // })
        // .then((url) => {
        //   resolve(url)
        // })
        // .catch((error) => {
        //   reject(error)
        // })
    })
  }

class ConfigurationScreen extends React.Component {
    static navigationOptions = {
        title: 'Tus datos',
        headerStyle: {backgroundColor: Constants.colors.gray},
    };

    constructor(props){
        super(props);

        const { userData } = props;

        let phone = userData.phone;
        let phonePrefix = userData.phonePrefix;
        if(phone && !phonePrefix){
            phonePrefixes.forEach((pPfx) => {
                if(pPfx.value && phone.indexOf(pPfx.value) == 0){
                    console.log('​ConfigurationScreen -> constructor -> pPfx', pPfx);
                    phone = phone.split(pPfx.value)[1];
                    phonePrefix = pPfx.value;
                }
            });
        }

        this.state = {
            uid            : userData.uid,
            picture        : userData.picture,
            email          : userData.email,
            phone          : phone,
            phonePrefix    : phonePrefix,
            name           : userData.name,
            lastname       : userData.lastname,
            formError      : false,
            formErrorText  : '',
            formSaved      : false,
            uploadProgress : -1,
            uploadError    : ''
        }

        this.onSetUserData = bindActionCreators(setUserData, props.dispatch);

        this.updateLoginField = this.updateLoginField.bind(this);
        this.saveUserData = this.saveUserData.bind(this);
        this.getPicture = this.getPicture.bind(this);
    }

    updateLoginField(field, value){
        let prevState = this.state;
        prevState[field] = value;

        this.setState(prevState);
    }

    saveUserData(){
        const { userData } = this.props;
        this.setState({formError: false, formErrorText: '', formSaved: false});
        const { uid, email, phone, phonePrefix, name, lastname, picture } = this.state;

        if(!email || !phone || !phonePrefix || !name || !lastname){
            this.setState({formError: true, formErrorText: 'Todos los campos son obligatorios'});
            return
        }

        const newUserData = Object.assign(userData, { uid, email, phone, phonePrefix, name, lastname, picture })

        UserService.saveUserInfo(newUserData);
        this.onSetUserData(newUserData);
        this.setState({formSaved: true})
    }

    getPicture(){
        var ImagePicker = require('react-native-image-picker');
        const options = {
            title: 'Selecciona una foto',
            storageOptions: {
              skipBackup: true,
              path: 'images',
              noData: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }
            else {          
              uploadImage(response.uri, this.state.uid, this)
                .then(
                    (url) => {
                        console.log('​ConfigurationScreen -> getPicture -> url', url);
                        this.setState({ picture: url, uploadProgress:-1 });
                    },
                    (error) => {
                        this.setState({ uploadError: error, uploadProgress:-1 });
                    });
              // You can also display the image using data:
              // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          
              
            }
          });
    }

    render() {
        const { 
            picture, 
            email, 
            phone, 
            phonePrefix, 
            name, 
            lastname, 
            formError, 
            formSaved, 
            formErrorText ,
            uploadError,
            uploadProgress
        } = this.state;

        const { userData } = this.props;
        const userPicture = UserService.getUserPicture(Object.assign(userData,{picture}))

        return (
            <KeyboardAwareScrollView contentContainerStyle={styles.mainView}>
                <View style={styles.mainImageContainer}>
                    <TouchableOpacity onPress={this.getPicture}>
                        {uploadError ? 
                            <Text style={styles.uploadErrorText}>{uploadError}</Text> :
                            (uploadProgress >= 0 ? 
                                <Text style={styles.uploadProgressText}>{uploadProgress}%</Text> :
                                <Image style={styles.mainImage} source={userPicture} />)
                        }

                    </TouchableOpacity>
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={name} 
                        onInputChange={this.updateLoginField} 
                        field="name"
                        placeholder="Nombre"
                    />
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={lastname} 
                        onInputChange={this.updateLoginField} 
                        field="lastname"
                        placeholder="Apellidos"
                    />
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={email} 
                        onInputChange={this.updateLoginField} 
                        field="email"
                        placeholder="Email"
                    />
                </View>
                <View style={styles.formRow}>
                    <Select 
                        placeholder={{}}
                        items={phonePrefixes}
                        onValueChange={(value) => {
                            console.log('​ConfigurationScreen -> render -> value', value);
                            this.setState({
                                phonePrefix: value,
                            });
                        }}
                        value={phonePrefix}
                    />
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={phone} 
                        onInputChange={this.updateLoginField} 
                        field="phone"
                        placeholder="Teléfono móvil"
                    />
                </View>
                <View style={styles.formError}>
                    {formSaved ? 
                        <Text style={styles.formSavedText}>Datos guardados correctamente</Text>:
                        <Text style={styles.formErrorText}>&nbsp;{formErrorText}&nbsp;</Text>
                    }
                </View>
                <View style={styles.formRowButton}>
                    <Button 
                        type="pink"
                        onButtonPress={this.saveUserData} 
                        text={'GUARDAR'} 
                    />
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
}

export default connect(mapStateToProps)(ConfigurationScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        paddingTop: hp(14),
    },
    bgImage: {
        resizeMode: 'cover',
        position: 'absolute',
        top: 0,
        width: wp(100),
        height: hp(70),
    },
    mainImageContainer:{
        width: wp(50),
        height: wp(50),
        marginLeft:wp(25),
        marginBottom: hp(3)
    },
    mainImage:{
        width: wp(50),
        height: wp(50),
        resizeMode: 'cover',
        overflow: 'hidden',
        borderRadius: wp(25),
        borderWidth: 0,
    },
    formRow:{
        marginBottom: hp(2)
    },
    formRowCheck:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginHorizontal: wp(15),
        width: wp(65),
    },
    checkbox:{
        padding: 10
    },
    legalText:{
        color: Constants.colors.textGray,
        fontSize: hp(Constants.fontSize.s),
        marginLeft: wp(2),
    },
    formRowButton:{
        marginBottom: hp(2),
    },
    formRowLast:{
        marginBottom: hp(4)
    },
    formRowOr:{
        marginBottom: hp(2)
    },
    bottomLinkText:{
        fontSize:hp(Constants.fontSize.l),
        color: Constants.colors.darkGray,
        textAlign:'center'
    },
    bottomLinkTextPink:{
        color : Constants.colors.pink
    },
    formError: {
        paddingTop: hp(2),
        paddingBottom: hp(2)
    },
    formErrorText: {
        color: Constants.colors.pink,
        textAlign:'center'
    },
    formSavedText: {
        color: Constants.colors.textGray,
        textAlign:'center'
    },
    uploadErrorText:{
        color: Constants.colors.textGray,
        fontSize:hp(Constants.fontSize.m),
        textAlign:'center',
        width: wp(40),
        paddingLeft: wp(10),
        marginTop: wp(20),
    },
    uploadProgressText:{
        color: Constants.colors.pink,
        fontSize:hp(Constants.fontSize.ll),
        textAlign:'center',
        width: wp(40),
        paddingLeft: wp(10),
        marginTop: wp(20),
    }
});