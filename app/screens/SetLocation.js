import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { View, StyleSheet, Text } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import { setUserData } from '../actions'; 

import UserService from '../services/UserService';
import DataService from '../services/DataService';
import { widthPercentage as wp, heightPercentage as hp, aspectRatio } from '../services/ScreenDimensions';
import Constants from '../config/styles';
import Settings from '../config/settings';

import Button from '../components/Buttons/Button';

const mapMarker = require('../assets/images/icons/localiza/localiza.png');

var inside = require('point-in-polygon');

class SetLocationScreen extends React.Component {
    static navigationOptions = {
        title: 'Tu Posición',
        headerLeft: null,
        headerStyle: {backgroundColor: Constants.colors.gray},
    };
    constructor(props){
        super(props);

        let iniLocation = props.userData && props.userData.location 
            ? props.userData.location 
            : {latitude: Settings.mapDefaulLatitude, longitude: Settings.mapDefaulLongitude};

        this.state = {
            location: iniLocation
        }
        this.beaches = [];

        this.onSetUserData = bindActionCreators(setUserData, props.dispatch);

        this.updatePosition = this.updatePosition.bind(this);
        this.confirmPosition = this.confirmPosition.bind(this);
        this._isBeachOpen = this._isBeachOpen.bind(this);
    }

    componentDidMount(){
        DataService.getCollection('beach').then(
            (result)=>{
                this.beaches = result;
            },
            (error)=>{
                console.log('BEACHES ERROR: ', error);
            }
        );
    }

    updatePosition(e){
        const location = {
            latitude: e.nativeEvent.coordinate.latitude,
            longitude: e.nativeEvent.coordinate.longitude,
            date: +(new Date())
        }
        // console.log('​SetLocationScreen -> updatePosition -> location', location);

        this.setState({location});
    }

    confirmPosition(){
        const { location } = this.state;
        let userData = Object.assign(this.props.userData || {} , { location });

        const userLocation = [location.latitude, location.longitude];

        let insideBeach = false;
        let insideSection = false;
        let beachClosed = false;
        
        userData.beachId = null,
        userData.beachName = '';

        this.beaches.forEach((beach) => {
            let beachCoordinates =[];
            beach.coordinates.forEach((beachCoord) => {
                beachCoordinates.push([
                    beachCoord.lat, 
                    beachCoord.lng
                ])
            });

            if(inside(userLocation, beachCoordinates)){
                insideBeach = true;
                userData.beachId = beach.id;
                userData.beachName = beach.name;

                if(!this._isBeachOpen(beach)){
                    beachClosed = true;
                    return;
                }

                const beachSections = Object.keys(beach.sections);

                beachSections.forEach((sectionId) => {
                    let sectionCoordinates = [];
                    beach.sections[sectionId].forEach((beachSectionCoord) => {
                        sectionCoordinates.push([
                            beachSectionCoord.lat, 
                            beachSectionCoord.lng
                        ])
                    })

                    if(inside(userLocation, sectionCoordinates)){
                        insideSection = true;
                        userData.section = sectionId;

                        const bikeFilter = [
                            {field: 'beachId', cond: '==', value: userData.beachId},
                            // {field: 'sections', cond: 'array-contains', value: userData.section}
                        ]
                        DataService.getCollection('bike', bikeFilter).then(
                            (result) => {
                                userData.bikeId = null;

                                if(result && result.length > 0){
                                    result.forEach((bike) => {
                                        if(bike.sections.indexOf(userData.section) >= 0){
                                            userData.bikeId = result[0].id
                                        }
                                    })
                                } else {
                                    userData.bikeId = null;
                                }
                                this.saveUserInfo(userData, insideBeach, insideSection);
                            },
                            (error) => {
                                this.saveUserInfo(userData, insideBeach, insideSection);
                                console.log('​SetLocationScreen -> confirmPosition -> error', error);
                            }
                        )
                    }
                })
            }
        });

        if(!insideSection  || !insideBeach){
            this.saveUserInfo(userData, insideBeach, insideSection, beachClosed);
        }
    }

    _isBeachOpen(beach){
        if(beach.state === false){
            return false;
        }

        if(beach.openTime && beach.closeTime){
            const openTime = beach.openTime.split(":");
            const closeTime = beach.closeTime.split(":");

            if(openTime.length == 2 && closeTime.length == 2){
                const openDate = new Date();
                openDate.setHours(openTime[0]);
                openDate.setMinutes(openTime[1]);

                const closeDate = new Date();
                closeDate.setHours(closeTime[0]);
                closeDate.setMinutes(closeTime[1]);
                
                const now = new Date();
                
                if(+now < +openDate || +now > +closeDate){
                    return false;
                }
            }
        }

        return true;
    }

    saveUserInfo(userData, insideBeach, insideSection, beachClosed){
        this.onSetUserData(null);
        setTimeout(()=>{ //Temp solution to refresh beach on menu
            this.onSetUserData(userData);
            UserService.saveUserInfo(userData);
            if (beachClosed) {
                this.props.navigation.navigate('BeachNotAvailable');
            } else {
                this.props.navigation.navigate('ConfirmPosition',{hasBike: userData.bikeId, insideBeach, insideSection, nearestBeach: this.beaches[0] });
            }
        },0)
    }

    render() {
        const { location } = this.state;

        return (
            <View style={styles.mainView}>
                <MapView
                    style={styles.map}
                    initialRegion={{
                        latitude: location.latitude,
                        longitude: location.longitude,
                        latitudeDelta: Settings.mapDelta,
                        longitudeDelta: Settings.mapDelta * aspectRatio(),
                    }}
                    onMarkerDragEnd={(e) => this.updatePosition(e)}
                    onPress={(e) => this.updatePosition(e)}
                    mapType="hybrid"
                >
                    <Marker
                        image={mapMarker}
                        key={'user-location'}
                        coordinate={location}
                        anchor={{x:0.5, y: 0.5}}
                        draggable
                    />
                </MapView>
                <View style={styles.actions}>
                    <Text style={styles.actionsText}>Muévete por el mapa y toca en tu localización aproximada</Text>
                    <Button 
                        type="pink"
                        onButtonPress={this.confirmPosition} 
                        text={'Confirmar'} 
                        marginLeft={0}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(SetLocationScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        flex:1,
        height:hp(100),
        marginTop: hp(10),
    },
    map: {
        width: wp(100),
        height: hp(65),
    },
    markerIcon:{
        width: 30, 
        height: 30
    },
    actions:{
        height:hp(30),
        paddingHorizontal: wp(5),
        paddingVertical: wp(5),
        flexDirection: 'column',
        alignItems:'center',
        justifyContent: 'flex-start',
    },
    // actionsTitle:{
    //     fontSize: hp(Constants.fontSize.xl),
    //     color: Constants.colors.darkGray,
    //     textAlign: 'center',
    //     marginBottom: hp(3),
    //     paddingHorizontal: wp(10)
    // },
    actionsText:{
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.darkGray,
        textAlign: 'center',
        marginBottom: hp(3),
        paddingHorizontal: wp(10)
    }
});