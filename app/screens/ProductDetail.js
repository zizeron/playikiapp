import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { ScrollView, TouchableOpacity, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import OrderService from '../services/OrderService';
import DataService from '../services/DataService';
import Constants from '../config/styles';

import { setOrder } from '../actions'

import CartIcon from '../components/Menu/CartIcon';
import Loading from '../components/Common/Loading';
import ProductAdded from '../components/Modals/ProductAdded';
import Button from '../components/Buttons/Button';
import ProductOutOfStock from '../components/Modals/ProductOutOfStock';


const iconMinus = require('../assets/images/icons/minus_red.png');
const iconPlus = require('../assets/images/icons/plus_blue.png');

class ProductDetailScreen extends React.Component{
    static navigationOptions = ({navigation})=>{
        return {
            title: navigation.state.params && navigation.state.params.productName ? navigation.state.params.productName : 'Producto',
            headerStyle: {
                backgroundColor: 'transparent',
                borderBottomWidth: 0
            },
            headerRight: <CartIcon />,
            headerTransparent: true,
            headerBackground: <View style={{ flex: 1, backgroundColor: Constants.colorsRGB.gray }} />
        }
    };

    constructor(props){
        super(props)

        this.state = {
            product           : null,
            qty               : 1,
            productAdded      : false,
            productOutOfStock : false
        }

        this.addingProduct = false;
        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);


        this.increaseQty = this.increaseQty.bind(this);
        this.decreaseQty = this.decreaseQty.bind(this);
        this.closeProductAdded = this.closeProductAdded.bind(this);
        this.goToOrder = this.goToOrder.bind(this);
        this.addToOrder = this.addToOrder.bind(this);
        this.closeProductOutOfStock = this.closeProductOutOfStock.bind(this);
        this.addProductToOrder = this.addProductToOrder.bind(this);
    }

    componentDidMount(){
        this.loadData();
    }

    loadData(){
        const productId = this.props.navigation.getParam('product', '');
        let { userData } = this.props;

        DataService.getProduct(userData, productId).then(
            (result) => {                
                this.setState({product: result});
            },
            (error) => {
                console.log('​componentDidMount -> error', error);
            }
        )
    }

    increaseQty(){
        const { qty, product } = this.state;
        if(qty < product.qty){
            this.setState({ qty: qty + 1 });
        }else{
            this.setState({ productOutOfStock: true });
        }
    }

    decreaseQty(){
        const { qty } = this.state;

        if(qty > 1){
            this.setState( { qty : qty-1 } );
        }
    }

    closeProductAdded(){
        this.setState({productAdded: false});
    }

    closeProductOutOfStock(){
        this.setState({productOutOfStock: false});
    }


    goToOrder(){
        this.props.navigation.navigate('Order');
        this.setState({productAdded: false});
    }

    addToOrder(){
        if(this.addingProduct) return;

        let { order, userData } = this.props;
        const { product, qty } = this.state;
        this.addingProduct = true;

        if(!order){
            order = OrderService.createNewOrder(userData).then(
                (order) => {
                    this.addProductToOrder(userData, order, product, qty)
                },
                (error) => {
                    console.log('​MenusScreen -> addMenuToCart -> error', error);
                    this.addingProduct = false;
                }
            )
        }else{
            this.addProductToOrder(userData, order, product, qty)
        }
    }

    addProductToOrder(userData, order, product, qty){
        OrderService.addProductToCart(userData, order, product, qty).then(
            (order) => {
                this.onSetOrder(order);
                this.addingProduct = false;
                this.loadData();
                this.setState({productAdded : true})        
            },
            (error) => {
                console.log('​CategoryScreen -> addToCart -> error', error);
                if(error == 'no-stock'){
                    this.setState({productOutOfStock: true});
                }
            }
        );
    }

    render(){
        const { product, qty, productAdded, productOutOfStock } = this.state;

        if(!product) return <Loading/>

        return (
            <ScrollView contentContainerStyle={styles.mainView}>
                <ProductAdded 
                    showModal={productAdded} 
                    onCloseModal={this.closeProductAdded}
                    onGoToOrder={this.goToOrder}
                />
                <ProductOutOfStock 
                    showModal={productOutOfStock} 
                    onCloseModal={this.closeProductOutOfStock}
                />
                <Image style={styles.productImage} source={{uri: product.image[0]}}/>
                <Text style={styles.productPrice}>{`${product.PVP} €`}</Text>
                <Text style={styles.productDesc}>{product.desc}</Text>
                {product.qty > 0 ? 
                    <View style={styles.addProduct}>
                        <View style={styles.orderQty}>
                            <TouchableOpacity onPress={this.decreaseQty}>
                                <View style={styles.orderItemMinus}>
                                    <Image source={iconMinus} style={styles.orderItemIcon}/>
                                </View>
                            </TouchableOpacity>
                            <View style={styles.orderItemQty}>
                                <Text style={styles.orderItemQtyText}>{qty}</Text>
                            </View>
                            <TouchableOpacity onPress={this.increaseQty}>
                                <View style={styles.orderItemPlus}>
                                    <Image source={iconPlus} style={styles.orderItemIcon}/>
                                </View> 
                            </TouchableOpacity>
                        </View>
                        <Button 
                            type="pink"
                            onButtonPress={this.addToOrder} 
                            text={`Añadir - ${qty * product.PVP} €`} 
                            marginLeft={0}
                        />
                    </View> :
                    <Text style={styles.productUnavailable}>No disponible actualmente</Text>
                }
                {product.qty <= 3 && 
                    <View style={styles.stockAlert}>
                        {product.qty > 0 && <Text style={styles.stockAlertText}>Menos de 3 ud.</Text>}
                        {product.qty <= 0 && <Text style={styles.stockAlertText}>No disp.</Text>}
                    </View>    
                }
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
export default connect(mapStateToProps)(ProductDetailScreen);

const styles = StyleSheet.create({
    mainView:{
        width: wp(100),
        height: hp(100),
        paddingHorizontal: wp(4),
        paddingTop: hp(12),
        paddingBottom: hp(5),
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Constants.colors.white
    },
    productPrice:{
        position:'absolute',
        top: hp(12),
        right: wp(4),
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.xl),
    },
    productImage:{
        width: wp(92),
        height: wp(92),
        borderRadius: wp(46),
        resizeMode: 'cover',
        marginBottom: hp(2),
        backgroundColor: Constants.colors.gray
    },
    productDesc:{
        color: Constants.colors.textGray,
        width: wp(70),
        fontSize: hp(Constants.fontSize.m),
        marginBottom: hp(3),
        textAlign: 'center'
    },
    addProduct:{
        alignItems: 'center'
    },
    orderQty:{
        width: wp(50),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: hp(3),
    },
    orderItemMinus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightRed,
        borderRadius: wp(4)
    },
    orderItemQty:{
        marginRight: wp(3),
        marginLeft: wp(3),
        width: wp(15),
        height: wp(15),
        backgroundColor: Constants.colors.gray,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    orderItemQtyText:{
        fontSize: hp(Constants.fontSize.xl),
    },
    productUnavailable:{
        marginTop: hp(2),
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center',
    },
    orderItemPlus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightBlue,
        borderRadius: wp(4)
    },
    stockAlert:{
        position:'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        top: hp(12),
        left: wp(4),
        padding: wp(2),
        width: wp(14),
        height: wp(14),
        borderRadius: wp(7),
        backgroundColor: Constants.colors.red
    },
    stockAlertText:{
        fontSize: hp(Constants.fontSize.xs),
        color: Constants.colors.white,
        textAlign: 'center'
    }
})