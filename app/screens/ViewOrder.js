import React from 'react';
import { connect } from 'react-redux';
import { ScrollView, View, Image, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import DataService from '../services/DataService';
import OrderService from '../services/OrderService';

import Constants from '../config/styles';

import Loading from '../components/Common/Loading'

const shipping = require('../assets/images/icons/shipping.png');
const location = require('../assets/images/icons/location.png');


class ViewOrderScreen extends React.Component {
    static navigationOptions = {
        title: 'Tu pedido',
        headerStyle: {backgroundColor: Constants.colors.gray},
    };
    constructor(props){
        super(props);

        this.state = {
            order: null,
        }
    }  

    componentDidMount(){
        const orderId = this.props.navigation.getParam('orderId', null);
        const { userData } = this.props;
        
        if(orderId){
            DataService.getObject('order', orderId).then(
                (result) => {
                    this.setState({order: result});
                },
                (error) => {
                    console.log('​ViewOrderScreen -> componentDidMount -> error', error);
                }
            )

            DataService.getObject('beach', userData.beachId).then(
                (result) => {
                    this.setState({beach: result});
                },
                (error) => {
                    console.log('​ViewOrderScreen -> componentDidMount -> error', error);
                }
            )
        }
    }

    getOrderTotal(){
        const { order, beach } = this.state;

        if(order && beach){
            return OrderService.getOrderTotal(order, beach.shippingFee)
        }

        return '-';
    }

    render() {
        const { order, beach } = this.state;
        console.log('​ViewOrderScreen -> render -> beach', beach);
        if(!order || !beach) return <Loading />;

        const orderDate = new Date(order.timeIni);

        return (
            <ScrollView contentContainerStyle={styles.orderView}>
                <Text style={styles.orderDate}>{`${orderDate.getDate()}/${orderDate.getMonth()+1}/${orderDate.getFullYear()}`}</Text>
                {order.orderLines.map((ordLine, i) => {
                    const prodImg = {uri: ordLine.product.image[0]}
                    console.log('​ViewOrderScreen -> render -> ordLine.product', ordLine.product);
                    return (
                        <View style={styles.orderItem} key={i}>
                            <Image source={prodImg} style={styles.orderItemPicture}/>
                            <View style={styles.orderItemInfo}>
                                <Text style={styles.orderItemTitle}>{ordLine.product.name}</Text>
                                {ordLine.product.selection && <Text style={styles.orderItemSelection}>{ordLine.product.selection.label}</Text>}
                                <Text style={styles.orderItemPrice}>{ordLine.product.PVP || ordLine.product.totalPVP }€</Text>
                            </View>
                        </View>
                    )
                })} 
                <View style={styles.orderItem}>
                    <Image source={shipping} style={styles.orderItemShipping}/>
                    <View style={styles.orderItemInfo}>
                        <Text style={styles.orderItemTitle}>Gastos de envío</Text>
                        <View style={styles.shippingBeach}>
                            <Text style={styles.shippingBeachName}>{beach.name}</Text>
                            <Image style={styles.shippingBeachIcon} source={location} />
                        </View>
                        <Text style={styles.orderItemPrice}>{beach.shippingFee}€</Text>
                    </View>
                </View>
                <View style={styles.orderSummary}>
                        <Text style={styles.orderSummaryItem}>TOTAL: <Text style={styles.orderSummaryItemValue}>{this.getOrderTotal()}€</Text></Text>
                    </View>
            </ScrollView>
        );
    }

    componentWillUnmount(){
        clearInterval(this.timeInterval);
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(ViewOrderScreen);


const styles = StyleSheet.create({
    noOrder: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(10),
    },
    noOrderText:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center'
    },
    orderDate:{
        width: wp(100),
        fontSize: hp(Constants.fontSize.xl),
        color: Constants.colors.darkGray,
        textAlign:'center',
        marginBottom: hp(3),
    },
    orderTimeoutTitle:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center',
        marginBottom: hp(1)
    },
    orderTimeoutDesc:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.m),
        textAlign: 'center'
    },
    orderView:{
        backgroundColor: Constants.colors.white,
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingBottom: hp(5),
        paddingTop: hp(16),
        justifyContent: 'center',
    },
    orderItem:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: hp(5)
    },
    orderItemPicture:{
        width: wp(30),
        height: wp(30),
        borderRadius: wp(15),
        marginRight: wp(5),
        borderWidth: 1,
        borderColor: Constants.colors.textGray,
    },
    orderItemShipping:{
        width: wp(15),
        height: wp(16),
        marginLeft: wp(7),
        marginRight: wp(13),
        resizeMode: 'contain'
    },
    shippingBeach:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    shippingBeachIcon:{
        width: wp(6),
        height: wp(6),
        marginLeft: wp(2),
        resizeMode: 'contain'
    },
    shippingBeachName:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.m)
    },
    orderItemInfo:{
        flexDirection:'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    orderItemTitle:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.l),
        marginBottom: hp(0.5)
    },
    orderItemSelection:{
        color: Constants.colors.textGray,
        fontSize: hp(Constants.fontSize.m),
        marginBottom: hp(0.5)
    },
    orderItemPrice:{
        color: Constants.colors.pink,
        fontSize: hp(Constants.fontSize.ll),
        marginBottom: hp(2)
    },
    orderItemButtons:{
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    orderItemMinus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightRed,
        borderRadius: wp(4)
    },
    orderitemQty:{
        fontSize: hp(Constants.fontSize.l),
        marginRight: wp(5),
        marginLeft: wp(5),
    },
    orderItemPlus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightBlue,
        borderRadius: wp(4)
    },
    orderSummary:{
        marginTop: hp(3),
        flexDirection:'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    orderSummaryItem:{
        fontSize: hp(Constants.fontSize.xl),
        marginBottom: hp(2),
        color: Constants.colors.darkGray
    },
    orderSummaryButton:{
        marginTop: hp(2),  
    },
    orderSummaryItemValue:{
        color: Constants.colors.pink
    }
});