import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';

import { 
    widthPercentage as wp, 
    heightPercentage as hp
} from '../services/ScreenDimensions';
import Constants from '../config/styles';

const notAvailable = require('../assets/images/icons/notAvailable.png');

class BeachNotAvailableScreen extends React.Component {
    static navigationOptions = {
        title: 'Horario',
        headerStyle: {backgroundColor: Constants.colors.gray},
      };

    constructor(props){
        super(props);
    }

    render(){
        const { userData } = this.props;
        if (!userData) return null;
        const userName = userData && userData.name ? ` ${userData.name}` : '';
        
        return (
            <ScrollView contentContainerStyle={styles.mainView}>
                <View style={styles.mainContent}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>¡LO SENTIMOS{userName.toUpperCase()}!</Text>
                    </View>
                    <View style={styles.subtitleContainer}>
                        <Text style={styles.subtitle}>¡Estamos fuera de horario de servicio en esta playa!</Text>
                    </View>
                    <Image style={styles.img} source={notAvailable} />
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>Nuestro horario de servicio cubre desde la mañana a la tarde*, por lo que mantente alerta, en ese periodo de tiempo podrás pedir con Playiki en esta playa.</Text>
                    </View>
                    </View>
                <View style={styles.subtitleContainer}>
                    <Text style={styles.textSmall}>*El horario de servicio de pedidos suele ser de 11 a 18 horas, aunque puede variar en algunas playas dependiendo de sus características y de otros factores.</Text>
                </View>
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(BeachNotAvailableScreen);

const styles = StyleSheet.create({
    mainView:{
        backgroundColor: Constants.colors.pink,
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingBottom: wp(5),
        paddingTop: hp(15),
    },
    mainContent:{
        alignItems: 'center',
    },
    titleContainer:{
        marginBottom: hp(2),
        marginTop: hp(3)
    },
    title:{
        fontSize: hp(Constants.fontSize.xlm),
        fontWeight: '500',
        color: Constants.colors.darkGray,
        textAlign: 'center'
    },
    subtitleContainer:{
        marginBottom: hp(1),
        paddingHorizontal: wp(5)
    },
    subtitle:{
        fontSize: hp(Constants.fontSize.ll),
        fontWeight: '300',
        color: Constants.colors.white,
        textAlign: 'center',
        lineHeight: hp(Constants.lineHeight.ll)
    },
    img:{
        width: wp(28),
        resizeMode: 'contain'
    },
    textContainer:{
        marginBottom: hp(2)
    },
    text:{
        fontSize: hp(Constants.fontSize.ll),
        color: Constants.colors.white,
        textAlign: 'center',
        lineHeight: hp(Constants.lineHeight.ll)
    },
    textSmall:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.white,
        textAlign: 'left'
    }
})