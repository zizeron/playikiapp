import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, View, Image, StyleSheet, Text, ScrollView } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import DataService from '../services/DataService';
import Constants from '../config/styles';

import MenuIcon from '../components/Menu/MenuIcon';
import Loading from '../components/Common/Loading';

const bag = require('../assets/images/icons/bag.png');

class OrderListScreen extends React.Component {
    static navigationOptions = {
        title: 'Pedidos',
        headerLeft: MenuIcon,
        headerStyle: {backgroundColor: Constants.colors.gray},
    };

    constructor(props){
        super(props);

        this.state = {
            orders: [],
            loading: true
        }

        this.dbHandler = null;

        this.goToOrder = this.goToOrder.bind(this);
        this.goToFollowOrder = this.goToFollowOrder.bind(this);
        this.enterOrder = this.enterOrder.bind(this)
    }

    componentWillUnmount(){
        if(this.dbHandler) this.dbHandler();
    }

    componentDidMount(){
        const { userData } = this.props;

        if(!userData.uid){
            this.setState({ loading: false });
            return;
        }
        const filter = {field: 'userId', cond: '==', value: userData.uid};

        this.dbHandler = DataService.getCollectionRealtime('order',[filter], (result) => {
            result = result.sort((a,b) => {
                return b.timeIni - a.timeIni;
            })

            this.setState({ orders: result, loading: false });
        })
    }

    goToOrder(order){
        if (order.status == 'pending') {
            this.props.navigation.navigate('Order');
        } else {
            this.props.navigation.navigate('ViewOrder',{orderId: order.id});
        }
    }

    goToFollowOrder(order){
        this.props.navigation.navigate('FollowOrder',{order});
    }

    enterOrder(order){
        if(order.status == 'paid' || order.status == 'on-round'){
            this.goToFollowOrder(order)
        }else{
            this.goToOrder(order);
        }
    }

    render() {
        const { orders, loading } = this.state;

        if(loading) return <Loading />;

        if(orders.length <= 0){
            return (
                <View style={styles.noNotifications}>
                    <Text  style={styles.noOrderText}>Todavía no has realizado ningún pedido</Text>
                </View>
            ); 
        }

        return (
            <ScrollView contentContainerStyle={styles.mainView}>
                {orders.map((o, i) => {
                    const orderDate = new Date(o.timeIni);
                    const orderMonth = orderDate.getMonth()<9 ? `0${orderDate.getMonth()+1}`:orderDate.getMonth()+1;
                    const orderDateText = `${orderDate.getDate()}/${orderMonth}/${orderDate.getFullYear()}`;

                    const orderId = `PEDIDO ${orderDate.getDate()}${orderMonth}${o.id.slice(-4)}`;

                    if(o.orderLines.length == 0) return null;
                    
                    let orderContent = '';
                    o.orderLines.forEach((line, i) => {
                        orderContent += `${line.qty} ${line.product.name}`;
                        if(i < o.orderLines.length-1){
                            orderContent += ' + ';
                        }
                    });

                    if(orderContent.length > 25){
                        orderContent = `${orderContent.substring(0,25)}...`;
                    }

                    let orderStatus = '';
                    let statusColor = null;
                    if(o.status == 'paid' || o.status == 'pending' 
                        || o.status == 'pending-payment' || o.status == 'on-round'){
                        orderStatus = 'Pedido en curso';
                        statusColor = styles.orderStatusCurso;

                    } else if(o.status == 'delivered'){
                        orderStatus = 'Pedido entregado';
                        statusColor = styles.orderStatusEntregado;
                    
                    } else if(o.status == 'cancelled' || o.status =='timeout'){
                        orderStatus = 'Pedido anulado';
                        statusColor = styles.orderStatusCancel;

                    } else if(o.status == 'refund'){
                        orderStatus = 'Pedido devuelto';
                        statusColor = styles.orderStatusRefund;
                    }

                    return (
                        <TouchableOpacity key={i} onPress={()=>{this.enterOrder(o)}}>
                            <View style={styles.item}>
                                <View style={styles.itemTop}>
                                    <View style={styles.itemTopLeft}>
                                        <Image style={styles.iconBag} source={bag}/>
                                        <Text style={styles.itemTextBig}>{orderDateText}</Text>
                                    </View>
                                    <Text style={styles.itemTextBig}>{orderId}</Text>
                                </View>
                                <View style={styles.itemBottom}>
                                    <Text style={styles.itemTextSmall}>{orderContent}</Text>
                                    <View style={statusColor}>
                                        <Text style={styles.orderStatusText}>{orderStatus}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    );
                })}
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(OrderListScreen);


const styles = StyleSheet.create({
    noNotifications:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(1),
    },
    noOrderText:{
        color: Constants.colors.darkGray,
        fontSize: hp(Constants.fontSize.xl),
        textAlign: 'center'
    },
    mainView:{
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(5),
        paddingTop: hp(14),
        paddingHorizontal: wp(2),
        justifyContent: 'center',
    },
    item:{
        borderWidth: 2,
        borderRadius: 15,
        borderColor: Constants.colors.gray,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp(2),
        backgroundColor: Constants.colors.white,
        shadowColor: Constants.colors.textGray,
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 2,
        shadowOpacity: 0.5,
    },
    itemTop:{
        width: wp(95),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: wp(2),
        paddingHorizontal: wp(4),
        borderBottomWidth: 1,
        borderColor: Constants.colors.gray,
    },
    itemTopLeft:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconBag:{
        width: wp(10),
        height: wp(10),
        marginRight: wp(3)
    },
    itemTextBig:{
        fontSize: hp(Constants.fontSize.ll),
        color: Constants.colors.darkGray,
        textTransform: 'uppercase',
        fontWeight: '300'
    },
    itemTextSmall:{
        fontSize: hp(Constants.fontSize.ml),
        color: Constants.colors.darkGray,
        textTransform: 'uppercase',
        fontWeight: '300',
    },
    itemBottom:{
        width: wp(95),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: wp(2),
        paddingHorizontal: wp(4),
    },
    orderStatusCurso:{
        width: wp(25),
        backgroundColor: Constants.colors.orderGreen,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    orderStatusEntregado:{
        width: wp(25),
        backgroundColor: Constants.colors.orderPink,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    orderStatusCancel:{
        width: wp(25),
        backgroundColor: Constants.colors.orderRed,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    orderStatusRefund:{
        width: wp(25),
        backgroundColor: Constants.colors.orderGray,
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp(1),
        borderRadius: 100
    },
    orderStatusText:{
        textAlign:'center',
        color: Constants.colors.white,
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300'
    }
});