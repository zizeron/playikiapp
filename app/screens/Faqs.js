import React from 'react';
import Collapsible from 'react-native-collapsible';
import { View, ScrollView, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';

import Constants from '../config/styles';

export default class FaqsScreen extends React.Component{
    constructor(props){
        super(props);

        this.state={
            collapsed: -1
        }
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.main}>
                {faqs.map((f, i) => {
                    return (
                        <View key={i} style={styles.faqItem}>
                            <TouchableOpacity onPress={()=>{this.setState({collapsed: i})}}>
                                <View style={styles.faqItemTitle}>
                                    <Text style={styles.faqTitle}>{f.title.toUpperCase()}</Text>
                                    <View style={styles.faqTitleTriangle}></View>
                                </View>
                            </TouchableOpacity>
                            <Collapsible collapsed={this.state.collapsed != i}>
                                <Text style={styles.faqText}>{f.text}</Text>
                            </Collapsible>
                        </View>
                    );
                })}
                
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(5),
        paddingTop: hp(10),
        paddingHorizontal: wp(2),
        justifyContent: 'center',
    },
    faqText:{
        fontWeight: '300',
        color: Constants.colors.textGray,
        fontSize: hp(Constants.fontSize.ml),
        padding: wp(5),
    },
    faqItemTitle:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems: 'center',
        padding: wp(3),
        width: wp(96),
        backgroundColor: Constants.colors.white,
        shadowColor: Constants.colors.textGray,
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 1,
        shadowOpacity: 0.5,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Constants.colors.gray,
    },
    faqTitle:{
        fontWeight: '300',
        color: Constants.colors.textGray,
        fontSize: hp(Constants.fontSize.l)
    },
    faqTitleTriangle:{
        width: wp(4),
        height: wp(4),
        borderLeftWidth: wp(2),
        borderRightWidth: wp(2),
        borderTopWidth: wp(2),
        borderLeftColor: Constants.colors.white,
        borderRightColor: Constants.colors.white,
        borderTopColor: Constants.colors.textGray,
    },
    faqItem:{
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: hp(2),
        backgroundColor: Constants.colors.white,
        
    }
});


const faqs = [
    { title: "¿Qué es Playiki?",
      text: "Playiki es una app donde puedes elegir los mejores productos y en minutos te los entregan en mano sin que te muevas de tu lugar en la playa. Bebidas, snacks, sandwiches y bocadillos, menús, ensaladas, e incluso objetos de ocio y parafarmacia. A buen precio y con todas las garantías de frescura, sanitarias y legales. A un sólo click, pagando de manera segura a través de tu móvil. Nuestros PLAYers te entregarán tu pedido en menos de 30 minutos, frío y perfecto para consumir, directamente en tu ubicación." },
    { title: "¿Cómo funciona Playiki?",
      text: "Confirma tu localización en la playa, y navega para seleccionar tus productos. Al confirmar tu pedido, recibirás los datos de tu PLAYer, tu palabra clave y el tiempo estimado de entrega. No te muevas, tu PLAYer llegará y solicitará la palabra clave. Respóndele con la misma, y te hará entrega de tu pedido. Disfruta de tu día de playa ¡PERFECTO! con Playiki" },
    { title: "¿Quiénes son los PLAYers?",
      text: "Los PLAYers son los chicos y chicas encargados de hacerte llegar tu pedido, conectados a nuestra plataforma. Son personas que te buscarán en la playa y te entregarán tu pedido, además de ayudarte a resolver cualquier incidencia con tus pedidos del modo más rápido y eficaz." },
    { title: "¿Qué puedo pedir?",
      text: "Playiki pone a tu disposición productos orientados a tu estancia en la playa. Alimentación como snacks, sándwiches y bocadillos, ensaladas, bebidas, postres, hielo. También productos de parafarmacia como protectores solares o after sun. E incluso productos de ocio como palas de tenis. La app siempre te mostrará los productos disponibles en tu playa y cerca de ti." },
    { title: "¿Cómo hago un pedido?",
      text: "Navega y selecciona tus productos haciendo click sobre el icono “+” o el botón añadir, en la cantidad que desees. En ese momento los tendrás disponible en tu carrito de la compra por un tiempo limitado. Cuando finalices tu pedido, haz click en Finalizar tu pedido. Si estás registrado y ya tienes guardada tu información de pago, eso será todo, te informaremos del Player que te hará llegar tu pedido y el tiempo estimado. Si no te has registrado todavía, al finalizar el primer pedido te solicitará los datos necesarios para tu registro y legalidad, además del método de pago. Una vez introducidos, se confirmará tu pedido." },
    { title: "¿Cuánto cuesta el servicio?",
      text: "Los precios de los productos de Playiki son muy competitivos. Cada pedido tendrá los cargos por el precio de cada producto y las unidades solicitadas. Además se realizará un cargo adicional en concepto del envío que realizarán nuestros Players, que será variable en función de localización, playa, fecha y franja horaria. Finalmente, el coste total del pedido es la suma del coste de los productos y del servicio de envío." },
    { title: "¿Llega Playiki a mi playa?",
      text: "Nos encantaría poder estar en todas las playas, pero, al menos de momento, todavía no es posible. Al abrir la app, la Plataforma comprueba si tu playa tiene cobertura de Playiki. Si es así, podrás consultar los productos disponibles en tu playa y realizar pedidos. En caso contrario, la plataforma te indicará que no hay cobertura en tu localización, indicándote la playa más cercana que sí la tiene." },
    { title: "¿A qué horas está disponible la entrega?",
      text: "La entrega estará disponible cada día de la temporada de 11:00 AM a 17:00 PM ininterrumpidamente." },
    { title: "¿Puedo elegir la hora de entrega?",
      text: "Dado que el sistema de entrega de Playiki es muy dinámico y siempre te presenta los productos disponibles en tiempo real, de momento no podemos garantizar que estos mismos productos estén disponibles en un tiempo posterior a la hora de tu pedido. Por tanto la hora de entrega siempre será aplicada a partir del momento de confirmación del pedido." },
    { title: "¿Garantizáis la frescura de los productos?",
      text: "Por supuesto, es una de las claves de Playiki. En las playas donde Playiki tiene cobertura, toda la cadena logística de entrega está diseñada para entregarte tu pedido lo antes posible sin romper la cadena de frío. Los players recogen los productos a través de los Playibikers y lo almacenan para el transporte en bolsas isotérmicas que garantizan su frescura en la entrega. " },
    { title: "No sé ha cumplido el tiempo, ¿por qué?",
      text: "El tiempo de entrega que te indica la app tras confirmar tu pedido es orientativo, aunque los criterios que calculan ese tiempo son realistas y la inmensa mayoría de las veces tu pedido te llegará en el tiempo marcado o incluso antes. Piensa que factores como la saturación de personas en la playa o el tiempo (viento) pueden influir en estos tiempos y son imposibles de calcular." },
    { title: "¿Por qué no se puede adquirir alcohol?",
      text: "La mayor parte de las playas en el territorio del Estado español, a través de los municipios, no permite el consumo, distribución y venta de bebidas alcohólicas en la playa. Playiki se adhiere a la legalidad y por tal razón no tiene disponible ningún producto con alcohol. Sí hay disponibles bebidas que habitualmente tienen porcentaje de alcohol en versión 0 (cervezas, etc)." },
    { title: "¿Puedo pagar en efectivo?",
      text: "No, con Playiki no se puede realizar ninguna transacción en efectivo. Sólo podrás confirmar tu pedido si has realizado el pago a través de la app por medios electrónicos. Tampoco podrás pagar en efectivo ni realizar pedidos in situ en persona a Players o Playibikers. En caso de que se lo solicites, ellos se negarán en redondo a tu solicitud, advirtiéndote de que se incumplen las normas y condiciones generales de Playiki." },
    { title: "¿Puedo cancelar mi pedido?",
      text: "Mientras tu pedido está en el carrito de la compra podrás cancelarlo cuando quieras. Si el tiempo de caducidad de tu carrito se cumple sin finalizar el pedido, la cancelación se producirá automáticamente. En caso de que hayas realizado el pago y confirmado tu pedido, no será posible cancelarlo." },
    { title: "¿Puedo cambiar mi pedido?",
      text: "Mientras tu pedido está en el carrito de la compra podrás cambiarlo cuando y cuanto quieras. Si el tiempo de caducidad de tu carrito se cumple sin finalizar el pedido, el pedido será cancelado y no podrás cambiarlo, teniendo que volver a incorporar los productos con los cambios que hayas considerado. En caso de que hayas realizado el pago y confirmado tu pedido, no será posible cambiarlo." },
    { title: "¿Puedo devolver mi pedido?",
      text: "En caso de que cambies de opinión respecto a tu pedido una vez confirmado, podrás devolverlo al Player en el momento de la entrega. Por desgracia, sólo podremos reembolsarte el coste de los productos no perecederos de tu pedido. El coste de los productos perecederos y los gastos de envío no serán reembolsados." },
    { title: "¿Qué ocurre si mi pedido está mal?",
      text: "Índica al Player si tu pedido es incorrecto en el momento de la entrega. Él podrá comprobar qué productos aparecen en tu pedido y los que están en las bolsas de entrega. Si además tienes tu app de Playiki y puedes mostrarle el pedido correcto, mejor aún. En caso de error por parte de Playiki, el Player se encargará de recoger los productos erróneos y te entregará posteriormente los productos correctos, como si de un nuevo pedido se tratase, sin coste adicional para ti." },
    { title: "¿Puedo localizar al Player?",
      text: "En todo momento, una vez confirmado el pedido, podrás ver la situación del Player con tus productos, e incluso podrás contactar con él por teléfono." },
    { title: "¿Qué ocurre si no me encuentran?",
      text: "El Player estará unos minutos en la localización de entrega que especificaste en la app de Playiki para entregarte tu pedido. Al llegar, preguntará por tu nombre, solicitando la contraseña para la entrega del pedido. Si nadie le contesta, intentará llamarte por teléfono. Si aún así no logra contactar contigo, marchará de allí, considerando tu pedido una devolución, con los cargos correspondientes para envío y productos perecederos." },
    { title: "¿Qué ocurre si no estoy en mi localización en la entrega?",
      text: "El Player no te localizará y no podrá entregarte tu pedido. Ten en cuenta que preguntará por ti, y además solicitará tu identificador de pedido para asegurar que eres el usuario correcto de Playiki y entregarte tus productos. Si no vas a poder estar en tu localización para la entrega, te recomendamos que delegues en alguna persona la recogida de tu producto, indicándole la contraseña de pedido." },
    { title: "¿Qué pasa si el GPS de mi móvil no ha funcionado bien?",
      text: "Los dispositivos móviles actuales tienen un nivel de localización realmente preciso y es difícil que no realicen una localización de manera correcta, sobre todo en un lugar abierto como es el caso de la playa. Si tienes sospechas de que tu dispositivo tiene problemas para localizar vía GPS, te recomendamos que no utilices la app de Playiki en él. Cuando el usuario en la App visualiza su localización, la puede confirmar o reajustar por la real, salvando cualquier problema de sensibilidad de su dispositivo GPS. Por otra parte, en cada entrega, en caso de que el Player no te encuentre en la localización prevista, te llamará por teléfono, donde podrás subsanar la localización en lo posible (si la desviación de tu GPS es inferior a unas decenas de metros)." },
    { title: "¿Cuándo va a llegar mi pedido?",
      text: "En el momento de confirmación de tu pedido te aparecerá el tiempo estimado de entrega de tu pedido. El tiempo de entrega que te indica la app tras confirmar tu pedido es orientativo, aunque los criterios que calculan ese tiempo son realistas y la inmensa mayoría de las veces tu pedido te llegará en el tiempo marcado o incluso antes. Piensa que factores como la saturación de personas en la playa o el tiempo (viento) pueden influir en estos tiempos y son imposibles de calcular." },
    { title: "¿Qué tipo de tarjetas aceptáis?",
      text: "Tarjetas VISA y Mastercard, junto con cuenta Paypal próximamente." },
    { title: "¿Cómo se realiza el pago?",
      text: "De manera segura a través de pasarela de pago online. No es posible el pago en efectivo in situ." },
    { title: "¿Es seguro añadir mi tarjeta de crédito?",
      text: "Totalmente seguro, Playiki y su equipo no pueden acceder a los datos de tu tarjeta, estando estos almacenados en la plataforma de pasarela de pago para seguridad total del cliente." },
    { title: "¿Tengo que dar propina a los players?",
      text: "No, aunque es algo a tu elección. Una valoración de cinco estrellas tras tu pedido será especialmente recibida por ellos." },
    { title: "¿Puedo formar parte de vuestro equipo?",
      text: "Claro, Playiki es un proyecto en expansión, y estamos buscando muchos perfiles para cubrir más productos y playas. Envíanos un correo a rrhh@playiki.com con tu cv y si encajas en los perfiles que buscamos, ¡hablaremos!" },
    { title: "¿Cómo tratáis mis datos?",
      text: "En Playiki creemos que tus datos son algo a proteger como un tesoro y cumplimos estrictamente la ley. Tus datos van a estar seguros porque cumplimos el Reglamento General de Protección de Datos. Te recomendamos que consultes en el menú el apartado de Política de Privacidad para ampliar la información" }
]