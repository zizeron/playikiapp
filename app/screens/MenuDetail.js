import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { Platform, ScrollView,Modal, TouchableOpacity, View, Image, StyleSheet, Text } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

import { widthPercentage as wp, heightPercentage as hp, aspectRatio } from '../services/ScreenDimensions';
import OrderService from '../services/OrderService';
import DataService from '../services/DataService';
import Constants from '../config/styles';

import { setOrder } from '../actions'

import CartIcon from '../components/Menu/CartIcon';
import Loading from '../components/Common/Loading';
import ProductAdded from '../components/Modals/ProductAdded';
import MenuOutOfStock from '../components/Modals/MenuOutOfStock';
import Button from '../components/Buttons/Button';
import Select from '../components/Input/Select';

const iconMinus = require('../assets/images/icons/minus_red.png');
const iconPlus = require('../assets/images/icons/plus_blue.png');


class MenuDetailScreen extends React.Component{
    static navigationOptions = ({navigation})=>{
        return {
            title: navigation.state.params && navigation.state.params.menuName ? navigation.state.params.menuName : 'Menú',
            headerStyle: {
                backgroundColor: 'transparent',
                borderBottomWidth: 0
            },
            headerRight: <CartIcon />,
            headerTransparent: true,
            headerBackground: <View style={{ flex: 1, backgroundColor: Constants.colorsRGB.gray }} />
        }
    };

    constructor(props){
        super(props);

        this.state = {
            menu              : null,
            productAdded      : false,
            qty               : 1,
            infoMode          : 'desc',
            products          : [],
            selectables       : [],
            selectableProduct : null,
            maxQty            : 0,
            menuOutOfStock    : false,
            menuStock         : null
        }

        this.addingProduct = false;
        this.inputRefs = {};

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);

        this.closeMenuOutOfStock = this.closeMenuOutOfStock.bind(this);
        this.closeProductAdded = this.closeProductAdded.bind(this);
        this.goToOrder = this.goToOrder.bind(this);
        this.increaseQty = this.increaseQty.bind(this);
        this.decreaseQty = this.decreaseQty.bind(this);
        this.addToOrder = this.addToOrder.bind(this);
        this.setInfoMode = this.setInfoMode.bind(this);
        this.loadData = this.loadData.bind(this);
        this.isProductAvailable = this.isProductAvailable.bind(this);
        this.getSelectables = this.getSelectables.bind(this);
        this.addMenuToOrder = this.addMenuToOrder.bind(this);
    }

    componentDidMount(){
        const menuId = this.props.navigation.getParam('menu', '');
        
        DataService.getObject('menu', menuId).then(
            (menu) => {
                this.setState({ menu });
            },
            (error) => {
                console.log('​componentDidMountMenu -> error', error);
            }
        )

        this.loadData();
    }

    loadData(){
        const menuId = this.props.navigation.getParam('menu', '');
        let { userData } = this.props;
        let maxQty = Infinity;
        
        DataService.getMenuProducts(userData,menuId).then(
            (result) => {                
                console.log('​componentDidMount -> result', result);
                let productPromises = [];
                result.products.forEach((product) => {
                    maxQty = Math.min(maxQty, product.qty);
                    productPromises.push(DataService.getObject('product',product.id));
                });
                Promise.all(productPromises).then(products => { 
                    this.setState({ products })
                });

                let selectablePromises = [];
                result.selectable.forEach((product) => {
                    selectablePromises.push(DataService.getObject('product',product.id));
                });
                Promise.all(selectablePromises).then(selectablesProducts => { 
                    selectables = [];
                    selectablesProducts.forEach((sp) => {
                        selectables.push({
                            label: sp.name,
                            value: sp.id
                        })
                    });
                    this.setState({ selectables, selectableProduct:selectables[0].value })
                });
                
                this.setState({menuStock: result, maxQty});
            },
            (error) => {
                console.log('​componentDidMountStock -> error', error);
            }
        )
    }


    increaseQty(){
        const { qty, maxQty } = this.state;
        if(qty < maxQty){
            this.setState( { qty : qty + 1 } );
        }else{
            this.setState({menuOutOfStock: true});
        }
    }

    decreaseQty(){
        const { qty } = this.state;

        if(qty > 1){
            this.setState( { qty : qty-1 } );
        }
    }

    closeMenuOutOfStock(){
        this.setState({menuOutOfStock: false})
    }

    closeProductAdded(){
        this.setState({productAdded: false});
    }

    goToOrder(){
        this.props.navigation.navigate('Order');
        this.setState({productAdded: false});
    }

    addToOrder(){
        if(this.addingProduct) return;

        let { order, userData } = this.props;
        this.addingProduct = true;

        let { menu, selectables, selectableProduct, qty } = this.state;
        const product = selectables.filter((s) => s.value === selectableProduct)[0];
        menu.selection = product;

        if(!order){
            OrderService.createNewOrder(userData).then(
                (order) => {
                    this.addMenuToOrder(userData, order, menu, qty);
                },
                (error) => {
                    console.log('​MenusScreen -> addMenuToCart -> error', error);
                    this.addingProduct = false;
                }
            )
        }else{
            this.addMenuToOrder(userData, order, menu, qty);
        }
    }

    addMenuToOrder(userData, order, menu, qty){
        OrderService.addMenuToCart(userData, order, menu, qty).then(
            (order) => {
                this.onSetOrder(order);
                this.addingProduct = false;
                this.loadData();
                this.setState({productAdded : true})        
            },
            (error) => {
                console.log('​CategoryScreen -> addToCart -> error', error);
                if(error == 'no-stock'){
                    this.setState({menuOutOfStock: true});
                }
            }
        );
    }

    setInfoMode(infoMode){
        this.setState({ infoMode });
    }

    isProductAvailable(){
        const { menuStock, products, selectables } = this.state;
        let available = true;
        
        products.forEach((product) => {
            menuStock.products.forEach((stockProduct) => {
                if(stockProduct.id == product.id && stockProduct.qty == 0){
                    available = false;
                }
            })
        })

        let availableSelectables = false;
        selectables.forEach((product) => {
            menuStock.selectable.forEach((stockProduct) => {
                if(stockProduct.id == product.value && stockProduct.qty > 0){
                    availableSelectables = true;
                }
            })
        })

        return available && availableSelectables;
    }

    getSelectables(){
        const { selectables, menuStock } = this.state;
        let selectablesAvailable = [];

        selectables.forEach((product) => {
            menuStock.selectable.forEach((stockProduct) => {
                if(product.value == stockProduct.id && stockProduct.qty > 0){
                    selectablesAvailable.push(product);
                }
            })
        })

        return selectablesAvailable;
    }
    

    render(){
        const { 
            menu, 
            qty, 
            productAdded, 
            infoMode, 
            products,
            selectableProduct,
            menuOutOfStock,
            menuStock,
            selectables
        } = this.state;

        if(!menu || !menuStock || selectables.length == 0) return <Loading/>

        const isProductAvailable = this.isProductAvailable();
        const menuSelectables = this.getSelectables();

        return (
            <View style={styles.mainView}>
                <ScrollView contentContainerStyle={styles.productsView}>
                    <ProductAdded 
                        showModal={productAdded} 
                        onCloseModal={this.closeProductAdded}
                        onGoToOrder={this.goToOrder}
                    />
                    <MenuOutOfStock 
                        showModal={menuOutOfStock} 
                        onCloseModal={this.closeMenuOutOfStock}
                    />
                    {menu.image && menu.image[0] && <Image style={styles.productImage} source={{uri: menu.image[0]}}/>}
                    {(!menu.image || !menu.image[0]) && <View style={styles.imageSeparator}></View>}
                    <View style={styles.productButtons}>
                        <Button 
                            type={infoMode == 'desc' ? 'pink' : ''}
                            textColor={infoMode == 'desc' ? 'white' : Constants.colors.textGray}
                            onButtonPress={()=>{this.setInfoMode('desc')}} 
                            text={'Descripción'} 
                            marginLeft={0}
                            width={45}
                            textUppercase={false}
                        />
                        <Button 
                            type={infoMode == 'rating' ? 'pink' : ''}
                            textColor={infoMode == 'rating' ? 'white' : Constants.colors.textGray}
                            onButtonPress={()=>{this.setInfoMode('rating')}} 
                            text={'Opiniones'} 
                            marginLeft={0}
                            width={45}
                            textUppercase={false}
                        />
                    </View>
                    {infoMode === 'desc' ? 
                        <View style={styles.productDesc}>
                            {products.map((p, i) => {
                                return (
                                    <View style={styles.menuProduct} key={i}>
                                        <Text style={styles.menuProductTitle}>{p.name}</Text>
                                        <Text style={styles.menuProductDesc}>{p.desc}</Text>
                                    </View>
                                )
                            })}
                            {menuSelectables.length > 0 && 
                                <View style={styles.menuProduct}>
                                    <Text style={styles.menuProductTitle}>Bebida 500ml</Text>
                                    <Text style={styles.menuProductDesc}>Selecciona tu bebida</Text>
                                    <View style={styles.selectContainer}>
                                        <Select 
                                            placeholder={{}}
                                            items={menuSelectables}
                                            onValueChange={(value) => {
                                                this.setState({
                                                    selectableProduct: value,
                                                });
                                            }}
                                            value={selectableProduct}
                                            iconRight={5}
                                        />
                                    </View>
                                </View>
                            }
                        </View> :
                        <View style={styles.productRating}>
                            <Text style={styles.productRatingText}>Todavía no hay opiniones sobre este producto</Text>
                        </View>
                    }
                </ScrollView>
                {isProductAvailable ?
                    <View style={styles.orderButtons}>
                        <View style={styles.orderQty}>
                            <TouchableOpacity onPress={this.decreaseQty}>
                                <View style={styles.orderItemMinus}>
                                    <Image source={iconMinus} style={styles.orderItemIcon}/>
                                </View>
                            </TouchableOpacity>
                            <Text style={styles.orderitemQty}>{qty}</Text>
                            <TouchableOpacity onPress={this.increaseQty}>
                                <View style={styles.orderItemPlus}>
                                    <Image source={iconPlus} style={styles.orderItemIcon}/>
                                </View> 
                            </TouchableOpacity>
                        </View>
                        <Button 
                            type="pink"
                            onButtonPress={this.addToOrder} 
                            text={`Añadir - ${qty * menu.totalPVP} €`} 
                            marginLeft={0}
                            width={50}
                        />
                    </View> :
                    <View style={styles.orderButtons}>
                        <Text style={styles.outOfStockText}>Alguno de los productos que conforman este menú no está disponible de momento. Vuelve en unos minutos.</Text>
                    </View>
                }
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
export default connect(mapStateToProps)(MenuDetailScreen);


const styles = StyleSheet.create({
    mainView:{
        flex: 1,
        justifyContent: 'space-between'
    },
    productsView:{
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(2),
        justifyContent: 'flex-start',
    },
    productImage:{
        width: wp(100),
        height: hp(40),
        resizeMode: 'cover'
    },
    imageSeparator:{
        width: wp(100),
        height: hp(40),
    },
    productButtons:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingTop: wp(5),
        paddingBottom: wp(5),
    },
    menuProduct:{
        paddingLeft: wp(4),
        paddingRight: wp(4),
        marginBottom: hp(1.8)
    },
    menuProductTitle:{
        fontSize: hp(Constants.fontSize.ml),
        fontWeight: '300',
        marginBottom: hp(0.5),
        color: Constants.colors.darkGray
    },
    menuProductDesc:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        color: Constants.colors.textGray
    },
    selectContainer:{
        marginTop: hp(1),
    },
    productRating:{

    },
    productRatingText:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.textGray,
        textAlign: 'center'
    },
    orderButtons:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: wp(4),
        paddingRight: wp(4),
        paddingTop: wp(5),
        paddingBottom: wp(5),
        backgroundColor: Constants.colors.white
    },
    orderQty:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    orderItemMinus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightRed,
        borderRadius: wp(4)
    },
    orderitemQty:{
        fontSize: hp(Constants.fontSize.l),
        marginRight: wp(5),
        marginLeft: wp(5),
    },
    orderItemPlus:{
        padding: wp(2),
        backgroundColor: Constants.colors.lightBlue,
        borderRadius: wp(4)
    },
    outOfStockText:{
        fontSize: hp(Constants.fontSize.l),
        color: Constants.colors.textGray
    },
});
