import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { View, StyleSheet, Text } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import firebase from 'react-native-firebase';

import { setOrderFollowing } from '../actions';
import OrderService from '../services/OrderService';
import DataService from '../services/DataService';
import { widthPercentage as wp, heightPercentage as hp, aspectRatio } from '../services/ScreenDimensions';
import Constants from '../config/styles';
import Settings from '../config/settings';

import MenuIcon from '../components/Menu/MenuIcon';
import Repartidor from '../components/Common/Repartidor';

const mapMarker = require('../assets/images/icons/localiza/localiza.png');
const mapUserDefault = require('../assets/images/other/map-default-user.png');

class FollowOrderScreen extends React.Component {
    static navigationOptions = {
        title: 'Seguimiento pedido',
        headerLeft: MenuIcon,
        headerStyle: {backgroundColor: Constants.colors.gray},
    };
    constructor(props){
        super(props);

        this.state = {
            location: props.userData.location,
            order: this.props.navigation.getParam('order', null),
            deliver: null,
            secondsLeft: 0,
            minutesLeft: 0,
            deliverLocation: null,
            deliverImage: {}
        }

        this.interval = null;
        this.deliverHandler = null;

        this.onSetOrderFollowing = bindActionCreators(setOrderFollowing, props.dispatch);

        this.startTimer = this.startTimer.bind(this);
        this.locateDeliver = this.locateDeliver.bind(this);
        
    }

    componentDidMount(){
        const { userData } = this.props;
        const { order } = this.state;

        this.onSetOrderFollowing(order);

        DataService.getUserDeliver(userData).then(
            (deliver) => {
                this.setState({deliver})
                this.locateDeliver(deliver);
            },
            (error) => {
                console.log('​PaymentConfirmScreen -> componentDidMount -> error', error);
                
            }
        )
        this.startTimer();
    }

    locateDeliver(deliver){        
        this.deliverHandler = firebase.firestore()
            .collection('users')
            .doc(deliver.deliverId)
            .onSnapshot((doc) => {
                const data = doc.data();
                if(data && data.location){
                    this.setState({
                        deliverLocation: {
                            latitude: parseFloat(data.location.latitude),
                            longitude: parseFloat(data.location.longitude)
                        }
                    })
                }
            });
    }


    startTimer(){
        const { order } = this.state;

        this.interval = setInterval(()=>{
            const orderPaidAt = order.paidAt;
            const afterTwentyMins = orderPaidAt + (60000 * 20);
            const now = +(new Date());
    
            const secondsLeft = Math.max(0, parseInt(afterTwentyMins - now) / 1000);
    
            this.setState({
                minsLeft: Math.floor(secondsLeft / 60),
                secondsLeft: Math.floor(secondsLeft % 60)
            });

        },1000)
    }

    

    render() {
        const { location, order, deliver, minsLeft, secondsLeft, deliverLocation } = this.state;  

        if(!order){
            return (
                <View style={styles.noOrder}>
                    <Text  style={styles.noOrderText}>Error order inválida</Text>
                </View>
            );
        }
        
        let deliverImageMap = mapUserDefault;
        if(deliver){
            if(deliver.imageMap){
                deliverImageMap = {uri: deliver.imageMap}
            }
        }
        
        const { charge } = order;  
        const repartidor = {
            deliver: deliver ? deliver.deliver : '', 
            image: deliver ? deliver.image : '',
            phone: deliver ? deliver.phone : '', 
            items: OrderService.getOrderItems(order), 
            playikid: charge && charge.playId ? charge.playId : ''
        }

        return (
            <View style={styles.mainView}>
                <View style={styles.deliverEst}>
                    <Text style={styles.deliverEstText}>La entrega está prevista en</Text>
                    <Text style={styles.deliverEstTime}>{minsLeft} : {secondsLeft < 10 ? `0${secondsLeft}` : secondsLeft}</Text>
                </View>
                <View style={styles.repartidorContainer}>
                    <Repartidor repartidor={repartidor} />
                </View>
                <MapView
                    style={styles.map}
                    initialRegion={{
                        latitude: location.latitude,
                        longitude: location.longitude,
                        latitudeDelta: Settings.mapDelta,
                        longitudeDelta: Settings.mapDelta * aspectRatio(),
                    }}
                    mapType="hybrid"
                >
                    <Marker
                        image={mapMarker}
                        key={'user-location'}
                        coordinate={location}
                        style={{ width: 40, height: 40 }}
                    />
                    {deliverLocation && 
                        <Marker
                        image={deliverImageMap}
                        key={'deliver-location'}
                        coordinate={deliverLocation}
                        style={{ width: 40, height: 40, overflow: 'hidden' }}
                    />}
                </MapView>
                
            </View>
        );
    }

    componentWillUnmount(){
        clearInterval(this.interval);
        if(this.deliverHandler) this.deliverHandler();
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(FollowOrderScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        flex:1,
        height:hp(100),
        paddingTop: hp(13),
    },
    map: {
        width: wp(100),
        height: hp(65),
        marginTop: hp(3),
    },
    noOrder: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(10),
    },
    deliverEst:{
        marginTop: hp(2),
        marginBottom: hp(2),
    },
    deliverEstText:{
        fontSize: hp(Constants.fontSize.m),
        fontWeight: '300',
        textAlign: 'center',
        color: Constants.colors.textGray,
    },
    deliverEstTime:{
        fontSize: hp(Constants.fontSize.xxl),
        fontWeight: '300',
        textAlign: 'center',
        color: Constants.colors.pink,
    },
    repartidorContainer:{
        marginLeft: wp(5),
        width:wp(90)
    }

});