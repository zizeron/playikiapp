import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import { ScrollView, ActivityIndicator, View, Image, StyleSheet, Text,ImageBackground  } from 'react-native';
import stripe from 'tipsi-stripe'

import { setOrder } from '../actions';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import DataService from '../services/DataService';
import OrderService from '../services/OrderService';
import UserService from '../services/UserService';

import Constants from '../config/styles';
import Input from '../components/Input/Input';
import Select from '../components/Input/Select';
import Button from '../components/Buttons/Button';
import { months, years } from '../config/data';
import PaymentConfirm from './PaymentConfirm';

const creditCard = require('../assets/images/icons/creditCardLogos.png');
const payPal = require('../assets/images/icons/paypalLogo.png');
const ccBg = require('../assets/images/other/card.png');

class PaymentScreen extends React.Component {
    static navigationOptions = {
        title: 'Forma de pago',
        headerStyle: {backgroundColor: Constants.colors.gray},
    };

    constructor(props){
        super(props);
        
        this.state = {
            paymentMethod  : 'cc',
            ccName         : props.userData && props.userData.name ? props.userData.name : '',
            ccNumber       : '',
            ccExpireMonth  : '',
            ccExpireYear   : '',
            ccCCV          : '',
            paymentError   : false,
            customer       : null,
            token          : '',
            loadingPayment : false
        }

        this.order = Object.assign({},props.order);

        this.onSetOrder = bindActionCreators(setOrder, props.dispatch);

        this.setPaymentMethod = this.setPaymentMethod.bind(this);
        this.updatePaymentField = this.updatePaymentField.bind(this);
        this.makePayment = this.makePayment.bind(this);
        this.createCharge = this.createCharge.bind(this);
    }


    componentDidMount(){
        const { userData } = this.props;
        DataService.getObject('stripe_customers', userData.uid).then(
            (result) => {
                let newState = {
                    customer: result
                }

                UserService.getPaymentSources(userData).then(
                    (sources) => {
                        if(sources && sources.length > 0){
                            sources.forEach((src) => {
                                if(src.card && !src.error){
                                    newState.token = src.id;
                                    const card = src.card;
                                    newState.ccName = card.name;
                                    newState.ccNumber = `**** **** **** ${card.last4}`;
                                    newState.ccExpireMonth = card.exp_month > 9 ? String(card.exp_month) : `0${String(card.exp_month)}`;
                                    newState.ccExpireYear = String(card.exp_year);
                                    newState.ccCCV = '***';
                                }
                            })   
                        }
                        console.log('​PaymentScreen -> componentDidMount -> newState', newState);
                        this.setState(Object.assign(this.state, newState));
                    },
                    (error) => {
                    }
                )
            },
            (error) => {
                console.log('​componentDidMount -> error', error);
            }
        )
    }

    setPaymentMethod(paymentMethod){
        this.setState({ paymentMethod });
    }

    async makePayment(){
        this.setState({paymentError: false});
        const { userData } = this.props;
        const { ccNumber, ccCCV, ccName, ccExpireMonth, ccExpireYear } = this.state;
        let { customer, token } = this.state;
        let status, statusMessage = '';

        if(!ccNumber || !ccCCV || !ccName || !ccExpireMonth || !ccExpireYear){
            this.setState({paymentError: true});
            return;
        }

        this.setState({loadingPayment : true});

        console.log('​PaymentScreen -> asyncmakePayment -> customer', customer);
        if(!customer){
            customer = {
                email: userData.email
            }
            DataService.saveObject('stripe_customers', userData.uid, customer);
        }

        console.log('​PaymentScreen -> asyncmakePayment -> token', token);
        if(!token){
            const tokenParams = {
                number: ccNumber,
                expMonth: parseInt(ccExpireMonth),
                expYear: parseInt(ccExpireYear),
                ccv: ccCCV,
                name: ccName,
                currency: 'eur'
            };

            try{
                // const newToken = await stripe.createTokenWithCard(tokenParams);
                const newToken = await fetch('https://api.stripe.com/v1/tokens',{
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization':'Bearer pk_live_Xs7011XMPZMbl3kuqXF9uA2g' //pk_test_1iMKCEQzySXYGXUHcx651lvg
                    },
                    body: `card[name]=${ccName}&card[number]=${ccNumber}&card[exp_month]=${parseInt(ccExpireMonth)}&card[exp_year]=${parseInt(ccExpireYear)}&card[cvc]=${ccCCV}`
                    })
                    .then((response) => response.json())
                    .then((responseJson) => responseJson)
                    .catch((error) => {
                        console.error(error);
                    });
                console.log('​PaymentScreen -> asyncmakePayment -> newToken', newToken);
                if(newToken){
                    newToken.token = newToken.tokenId;
                    token = newToken.id;
    
                    UserService.addPaymentSource(userData, token, newToken)
                    
                }
            } catch(error) {
                console.log('​PaymentScreen -> catch -> error', error);
                status = 'error';
                this.setState({loadingPayment : false});
                statusMessage = 'La tarjeta que has introducido no es inválida, por favor revisa los datos o vuelve a intentarlo con otra.'
            }

            console.log('​PaymentScreen -> status', status);
            if(status !== 'error'){
                firebase.firestore()
                    .collection('stripe_customers')
                    .doc(userData.uid)
                    .collection('sources')
                    .doc(token)
                    .onSnapshot((doc) => {
                        const data = doc.data();
                        console.log('​PaymentScreen -> asyncmakePayment 1');
                        console.log('​PaymentScreen -> data', data);
                        if(data && data.card && data.object == 'card' && !this.chargeCreated){
                            this.chargeCreated = true;
                            this.createCharge(userData);
                        }
                    });
            }else{
                this.props.navigation.navigate('PaymentConfirm',{status, statusMessage, order: this.order})
                this.setState({loadingPayment : false});
            }
        } else {
            this.createCharge(userData);
        }   
    }

    createCharge(userData){
        const totalOrder = this.props.navigation.getParam('total', 0);
        const couponId = this.props.navigation.getParam('couponId', '');

        console.log('​PaymentScreen -> totalOrder', totalOrder);
        if(totalOrder > 0){
            const chargeId = String(+(new Date()));
            const playId = UserService.calculatePlayId(userData);
            const newCharge = {
                chargeId,
                amount: totalOrder,
                status: 'pending',
                playId: playId,
                coupon: couponId
            };

            UserService.addPaymentCharge(userData, chargeId, newCharge).then(
                (result) => {
                    console.log('​PaymentScreen -> createCharge -> result', result);
                    if(result){
                        status = 'success';
                    }
                    this.props.navigation.navigate('PaymentConfirm',{status, chargeId, order: this.order})
                },
                (error) => {
                    this.setState({loadingPayment : false});
                    console.log('​PaymentScreen -> asyncmakePayment ERROR -> ', error);
                }
            );

        }else{
            status = 'error';
            this.setState({loadingPayment : false});
        }
    }

    getCCForm(){
        const { ccName, ccNumber, ccExpireMonth, ccExpireYear, ccCCV, paymentError, loadingPayment } = this.state;

        return(
            <View>
                <ImageBackground style={styles.ccbg} source={ccBg}> 
                    <Text style={styles.ccNumberText}>{ccNumber}</Text>
                    <Text style={styles.ccExpireText}>{ccExpireMonth}-{ccExpireYear}</Text>
                    <Text style={styles.ccNameText}>{ccName}</Text>
                </ImageBackground>
                <View style={styles.ccFields}>
                    <View style={styles.formRow}>
                        <Input 
                            value={ccName} 
                            onInputChange={this.updatePaymentField} 
                            field="ccName"
                            placeholder="Titular de la tarjeta"
                        />
                    </View>
                    <View style={styles.formRow}>
                        <Input 
                            value={ccNumber} 
                            onInputChange={this.updatePaymentField} 
                            field="ccNumber"
                            placeholder="Número de la tarjeta"
                        />
                    </View>
                    <View style={styles.formRow}>
                        <Select 
                            placeholder={{}}
                            items={months}
                            onValueChange={(value) => {
                                this.updatePaymentField('ccExpireMonth', value);
                            }}
                            value={ccExpireMonth}
                        />
                    </View>
                    <View style={styles.formRow}>
                        <Select 
                            placeholder={{}}
                            items={years}
                            onValueChange={(value) => {
                                this.updatePaymentField('ccExpireYear', value);
                            }}
                            value={ccExpireYear}
                        />
                    </View>
                    <View style={styles.formRow}>
                        <Input 
                            value={ccCCV} 
                            onInputChange={this.updatePaymentField} 
                            field="ccCCV"
                            placeholder="CCV"
                        />
                    </View>
                    <View style={styles.formRowError}>
                        {paymentError && 
                            <Text style={styles.formErrorText}>Todos los campos son obligatorios</Text>}
                    </View>
                    <View style={styles.formRow}>
                        {loadingPayment ? 
                            <ActivityIndicator size="small" color={Constants.colors.pink}/>:
                            <Button 
                                type="pink"
                                onButtonPress={this.makePayment} 
                                text={'REALIZAR PAGO'} 
                            />
                        }
                    </View>
                </View>
            </View>
        )
    }

    getPaypalForm(){

    }

    updatePaymentField(field, value){
        console.log('​PaymentScreen -> updatePaymentField -> field', field);
        let prevState = this.state;
        console.log('​PaymentScreen -> updatePaymentField -> prevState', prevState[field], field, value);
        if(prevState[field] == value){
            return;
        }

        if(prevState.token && prevState.token !== ''){
            prevState.token = '';
            prevState.ccNumber = '';
            prevState.ccExpireMonth = '';
            prevState.ccExpireYear = '';
            prevState.ccCCV = '';
            prevState[field] = value.slice(-1);
        }else{
            prevState[field] = value;
        }

        this.setState(prevState);
    }

    render(){
        const { paymentMethod } = this.state;

        return(
            <ScrollView contentContainerStyle={styles.mainView}>
                {/* <View style={styles.methodLogos}>
                    <TouchableOpacity style={paymentMethod == 'cc' ? styles.methodHighlight : {}} onPress={()=>this.setPaymentMethod('cc')}>
                        <Image style={styles.methodLogosItem} source={creditCard} />
                    </TouchableOpacity>
                    <TouchableOpacity style={paymentMethod == 'paypal' ? styles.methodHighlight : {}} onPress={()=>this.setPaymentMethod('paypal')}>
                        <Image style={styles.methodLogosItem} source={payPal} />
                    </TouchableOpacity>
                </View> */}
                <View style={styles.paymentForm}>
                    {paymentMethod == 'cc' ? this.getCCForm() : this.getPaypalForm()}
                </View>
            </ScrollView>
        );
    }
}


function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData,
        order   : state.orderReducer.order
    }
  }
  
  export default connect(mapStateToProps)(PaymentScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: Constants.colors.white,
        paddingBottom: hp(5),
        paddingTop: hp(14),
        justifyContent: 'center',
    },
    methodLogos:{
        width: wp(100),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wp(5),
        paddingVertical: wp(5)
    },
    methodHighlight:{
        borderBottomWidth: 1,
        borderColor: Constants.colors.pink
    },
    methodLogosItem:{
        width: 105,
        height: 35,
        resizeMode: 'cover',
        marginBottom: hp(1)
    },
    paymentForm:{

    },
    ccbg:{
        width: wp(98),
        height: 0.6314 * wp(98),
        marginLeft: wp(1),
        padding: wp(10),
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },
    ccFields:{
        marginTop: hp(3)
    },
    formRow:{
        marginBottom: hp(3)
    },
    formRowError:{
        height: hp(3)
    },
    formErrorText:{
        fontSize: hp(Constants.fontSize.m),
        color: Constants.colors.pink,
        textAlign: 'center',
    },
    ccNumberText:{
        fontSize: hp(Constants.fontSize.xll),
        color: Constants.colors.lightGray,
        marginBottom: hp(3),
    },
    ccExpireText:{
        fontSize: hp(Constants.fontSize.xl),
        color: Constants.colors.lightGray,
        marginBottom: hp(2),
    },
    ccNameText:{
        fontSize: hp(Constants.fontSize.xl),
        color: Constants.colors.lightGray,
    },

})