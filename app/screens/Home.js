import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, View, Image, ImageBackground, StyleSheet, Text } from 'react-native';

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import Constants from '../config/styles';

import Button from '../components/Buttons/Button';

const homeBg = require('../assets/images/bg-images/home.jpg');
const homeImage = require('../assets/images/other/home_logo.png');

class HomeScreen extends React.Component {
    constructor(props){
        super(props);

        this.goToRegister = this.goToRegister.bind(this);
        this.goToLogin = this.goToLogin.bind(this);
        this.enter = this.enter.bind(this);
    }

    enter(){
        this.props.navigation.navigate('Welcome');
    }

    goToRegister(){
        this.props.navigation.navigate('Signup');
    }

    goToLogin(){
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <ImageBackground style={styles.bgImage} source={homeBg}>
                <Image style={styles.mainImage} source={homeImage}></Image>
                {/* <View style={styles.buttonTop}>
                    <Button 
                        type="light"
                        onButtonPress={this.goToLogin} 
                        text={'Identificarme'}
                    />
                </View> */}
                <View style={styles.buttonDown}>
                    <Button 
                        type="dark"
                        onButtonPress={this.enter} 
                        text={'entrar'} 
                    />
                </View>
                {/* <View style={styles.bottomLink}>
                    <TouchableOpacity onPress={this.goToRegister}>
                        <Text style={styles.bottomLinkText}>¿No te has registrado aún?</Text>
                    </TouchableOpacity>
                </View> */}
            </ImageBackground>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
  export default connect(mapStateToProps)(HomeScreen);


const styles = StyleSheet.create({
    bgImage: {
        backgroundColor: '#ccc',
        flex: 1,
        resizeMode: 'cover',
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    },
    mainImage:{
        width: wp(44),
        height: wp(65.1),
        resizeMode: 'contain',
        marginLeft:wp(28),
        marginBottom: hp(5),
    },
    // buttonTop:{
    //     marginTop:hp(8),
    //     marginBottom: hp(3),
    // },
    // bottomLink:{
    //     marginTop: hp(10),
    // },
    // bottomLinkText:{
    //     fontSize:hp(Constants.fontSize.l),
    //     color: Constants.colors.pink,
    //     textAlign:'center'
    // }
});