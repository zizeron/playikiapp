
import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { TouchableOpacity, View, Image, ImageBackground, StyleSheet, Text } from 'react-native';

import { disableMenuGesture, setUserData } from '../actions'; 

import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';
import Constants from '../config/styles';
import Settings from '../config/settings';

import Button from '../components/Buttons/Button';
import UserService from '../services/UserService';


const welcomeBg = require('../assets/images/bg-images/welcome.png');
const userDefault = require('../assets/images/other/default-user.jpg');

class WelcomeScreen extends React.Component {
    static navigationOptions = ({navigation})=>{
        return {
            title: 'Bienvenido',
            headerLeft: null
            // headerLeft: (
            //     <TouchableOpacity onPress={navigation.getParam('disableMenuGesture')}><Text>Menu</Text></TouchableOpacity>
            // )
        }
    };

    constructor(props){
        super(props);

        this.state  = {
            loadingPosition: true
        }

        this.beaches = [];

        this.onSetUserData = bindActionCreators(setUserData, props.dispatch);
        this.onDisableMenuGesture = bindActionCreators(disableMenuGesture, props.dispatch);
        
        this.setUserLocation = this.setUserLocation.bind(this);
        this.getUserLocation = this.getUserLocation.bind(this);
    }

    componentDidMount(){
        // this.props.navigation.setParams({disableMenuGesture:()=>{
        //     this.onDisableMenuGesture(true)
        // }});
        // this.onDisableMenuGesture(false)

        this.getUserLocation();
    }

    getUserLocation(){
        this.setState({loadingPosition:true, error: ''});
        // console.log('​getUserLocation -> start' );

        navigator.geolocation.getCurrentPosition(
            (position) => {
                // console.log('​getUserLocation -> position', position);
                const today = +(new Date());
                const { userData } = this.props;
                const { latitude, longitude } = position.coords;

                if(latitude && longitude){
                    const newLocation = {
                        latitude, 
                        longitude, 
                        date: +(new Date())
                    }
                    // console.log('userData: ', userData);
                    // console.log('newLocation: ', newLocation);

                    if(!userData || !userData.location){
                        this.setUserLocation(newLocation);
                    }else{
                        const dateDiff = (today - userData.location.date) / 1000 / 60;
                        const locationDiff = {
                            latitude: Math.abs(newLocation.latitude - userData.location.latitude),
                            longitude: Math.abs(newLocation.longitude - userData.location.longitude),
                        }
                        // console.log('locationDiff: ', locationDiff);
                        // console.log('dateDiff: ', dateDiff);

                        if(locationDiff.longitude > Settings.locationDriftThreshold || 
                            locationDiff.latitude > Settings.locationDriftThreshold || 
                            dateDiff > 30
                        ){
                            this.setUserLocation(newLocation);
                        }else{
                            this.setState({loadingPosition:false});
                        }
                    }
                }else{
                    this.setState({error: 'Error obteniendo su posición, por favor vuelva a intentarlo'});
                }
                

            },
            (error) => {
                this.setState({ error: "No se ha podido obtener la ubicación" })
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10*60000 },
          );
    }

    setUserLocation(newLocation){
        const userData = Object.assign(this.props.userData || {} , { location : newLocation });
        this.onSetUserData(userData);
        UserService.saveUserInfo(userData)
        this.props.navigation.navigate('SetLocation')
    }

    render() {
        const { userData } = this.props;        
        const { error, loadingPosition } = this.state;
        const userPicture = UserService.getUserPicture(userData);

        return (
            <View style={styles.mainView}>
                <Image style={styles.bgImage} source={welcomeBg} />
                <View style={styles.mainImageContainer}>
                    <Image style={styles.mainImage} source={userPicture} />
                </View>
                <View style={styles.welcome}>
                    <Text style={styles.welcomeText}>Bienvenido</Text>
                    {userData && userData.name && <Text style={styles.welcomeName}>{userData.name}</Text>}
                </View>
                <View style={styles.welcomeTwo}>
                    {userData && userData.name && <Text style={styles.welcomeTwoText}>¡Qué bueno tenerte de vuelta!</Text>}
                </View>
                <View style={styles.button}>
                    {loadingPosition ? 
                        (error ? 
                            (<View>
                                <Text style={styles.loadingText}>{error}</Text>
                                <View style={styles.errorButton}>
                                    <Button 
                                        type="pink"
                                        onButtonPress={this.getUserLocation} 
                                        text={'VOLVER A INTENTARLO'} 
                                    />
                                </View>
                            </View>
                            ) :
                            <Text style={styles.loadingText}>Obteniendo posición</Text>
                        ) :
                        <Button 
                            type="pink"
                            onButtonPress={()=>{this.props.navigation.navigate('Products');}} 
                            text={'¡OK!'} 
                        />
                    }
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.userDataReducer.loading,
        userData: state.userDataReducer.userData
    }
  }
  
export default connect(mapStateToProps)(WelcomeScreen);


const styles = StyleSheet.create({
    mainView:{
        backgroundColor: '#fff',
        justifyContent: 'center',
        flex:1,
        height:hp(100)
    },
    bgImage: {
        resizeMode: 'stretch',
        position: 'absolute',
        top: 0,
        width: '100%',
        height: '60%'
    },
    mainImageContainer:{
        width: wp(50),
        height: wp(50),
        marginLeft:wp(25),
    },
    mainImage:{
        width: wp(50),
        height: wp(50),
        resizeMode: 'cover',
        overflow: 'hidden',
        borderRadius: wp(25),
        borderWidth: 0,
    },
    welcome:{
        marginTop:hp(8),
        marginBottom: hp(2),
        
    },
    welcomeText:{
        fontSize: hp(Constants.fontSize.ll),
        marginBottom: hp(0.6),
        color: Constants.colors.darkGray,
        textAlign: 'center'
    },
    welcomeName:{
        fontSize: hp(Constants.fontSize.xl),
        color: Constants.colors.darkGray,
        textAlign: 'center',
        textTransform: 'uppercase',
    },
    welcomeTwo:{
        marginTop:hp(3),
        marginBottom: hp(7),
    },
    welcomeTwoText: {
        fontSize: hp(Constants.fontSize.xl),
        fontWeight: '300',
        color: Constants.colors.textGray,
        textAlign: 'center'
    },
    loadingText: {
        fontSize: hp(Constants.fontSize.ml),
        color: Constants.colors.darkGray,
        textAlign: 'center'
    },
    errorButton:{
        marginTop: hp(2)
    }
});