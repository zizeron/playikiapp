import React from 'react';
import firebase from 'react-native-firebase';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { TouchableOpacity, View, Text, Image, StyleSheet, ActivityIndicator, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import OrderService from '../services/OrderService';
import { widthPercentage as wp, heightPercentage as hp } from '../services/ScreenDimensions';

import Constants from '../config/styles';
import phonePrefixes from '../config/phonePrefixes';

import {setSettings, setUserData, setOrder} from '../actions/index';

import Input from '../components/Input/Input';
import Select from '../components/Input/Select';
import Button from '../components/Buttons/Button';
import FacebookButton from '../components/Buttons/FacebookButton';
import ModalBack from '../components/Menu/ModalBack';
import ConfirmPhone from '../components/Modals/ConfirmPhone'
import CodeAutoconfirmed from '../components/Modals/CodeAutoconfirmed';

const loginBg = require('../assets/images/bg-images/login.png');

class LoginScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({ 
        title: 'Identifícate',
        headerLeft: null,
        headerRight: <ModalBack navigation={navigation}/>,
        headerTransparent: true,
        headerStyle:{
            borderBottomWidth: 0
        },
        gesturesEnabled: false,
        headerBackTitle: null,
        headerTitleStyle: {
          fontSize: hp(Constants.fontSize.l),
          fontWeight: '300',
        },
    })

    constructor(props){
        super(props);

        this.state = {
            phone             : '',
            phonePrefix       : '+34',
            password          : '',
            loading           : false,
            showModal         : false,
            showModalAutoconf : false,
            formError         : ''
        }

        this.newUserData = null;

        this.onSetOrder    = bindActionCreators(setOrder , props.dispatch);
        this.onSetUserData = bindActionCreators(setUserData, props.dispatch);
        this.onSetSettings = bindActionCreators(setSettings, props.dispatch);

        this.updateLoginField = this.updateLoginField.bind(this);
        this.goToRegister = this.goToRegister.bind(this);
        this.enter =  this.enter.bind(this);
        this.onCloseConfirmModal = this.onCloseConfirmModal.bind(this);
        this.showConfirmModal = this.showConfirmModal.bind(this);
        this.onCloseAutoconfModal = this.onCloseAutoconfModal.bind(this);
    }

    componentDidMount(){
        this.onSetSettings({redirectAfterlogin: false});
    }

    updateLoginField(field, value){
        let prevState = this.state;
        prevState[field] = value;

        this.setState(prevState);
    }

    goToRegister(){
        const { navigation } = this.props;
        const prevScreen = navigation.getParam('prevScreen', false);
        navigation.navigate('Signup',{prevScreen});
    }

    enter(){
        const {phone, phonePrefix} = this.state;

        if(!phone || !phonePrefix){
            this.setState({formError: 'El teléfono y su prefijo son obligatorios' });
            return;
        }

        this.setState({loading: true, formError: ''});

        firebase.firestore()
            .collection('users')
            .where('phone', '==', phone)
            .where('phonePrefix', '==', phonePrefix)
            .get()
            .then((docs) => {  
                console.log('​enter -> docs', docs, docs.size);
                if (docs.size != 1) {
                    this.setState({ loading: false, formError: 'Usuario no encontrado, por favor regístrate' })
                } else {
                    let userId = '';
                    docs.forEach((d) => {
                        userId = d.data().uid;
                    })

                    firebase.auth()
                    .verifyPhoneNumber(phonePrefix+phone)
                    .on('state_changed', (phoneAuthSnapshot) => {
                        console.log('​enter -> phoneAuthSnapshot', phoneAuthSnapshot);
                        switch (phoneAuthSnapshot.state) {
                            case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
                                console.log('code sent');
                                if(Platform.OS === 'ios'){
                                    this.showConfirmModal(phonePrefix+phone);
                                }
                                break;
                            case firebase.auth.PhoneAuthState.ERROR: // or 'error'
                                console.log('verification error');
                                console.log(phoneAuthSnapshot.error);
                                break;
                            case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
                                console.log('auto verify on android timed out');
                                this.showConfirmModal(phonePrefix+phone);
                                break;
                            case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
                                console.log('auto verified on android');
                                const { verificationId, code } = phoneAuthSnapshot;
                                const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);

                                firebase.auth().signInAndRetrieveDataWithCredential(credential).then(() => {
                                    this.onCloseConfirmModal(true, userId);
                                })
                                
                                break;
                        }
                    }, (error) => {
                        console.log(error);
                        console.log(error.verificationId);
                    }, (phoneAuthSnapshot) => {
                        console.log(phoneAuthSnapshot);
                    });
                }
        });
    }

    showConfirmModal(phone){
        firebase.auth()
            .signInWithPhoneNumber(phone)
            .then((confirmationResult) => {
                this.setState({showModal: true, loading: false, confirmationResult})
            }).catch((error) => {
                console.log('NO SMS SENT', error)
                this.setState({loading: false});
            });
    }

    onCloseConfirmModal(confirm, userId) {
        const { userData } = this.props;

        if(confirm){
            firebase.firestore().collection('users').doc(userId).get().then((doc) => {
                this.newUserData = Object.assign({}, doc.data(), userData);
                this.setState({loading: false, showModal: false, showModalAutoconf: true});
                
            });
        }else{
            this.setState({showModal: false});
        }
    }

    onCloseAutoconfModal(){
        const { navigation } = this.props;
        this.setState({showModalAutoconf: false});
        this.onSetUserData(this.newUserData);
        
        const prevScreen = navigation.getParam('prevScreen', false);
        if (prevScreen) {
            navigation.navigate(prevScreen);
        } else {
            navigation.pop();
        }
    }

    render() {
        const { loading, confirmationResult, showModal, formError, showModalAutoconf } = this.state;
        const { navigation } = this.props;
        const message = navigation.getParam('message', '');

        return (
            <KeyboardAwareScrollView contentContainerStyle={styles.mainView}>
                <Image style={styles.bgImage} source={loginBg}></Image>
                {message != '' && <View style={styles.loginMessage}>
                    <Text style={styles.loginMessageText}>{message}</Text>
                </View>}
                <View style={styles.formRow}>
                    <Select 
                        placeholder={{}}
                        items={phonePrefixes}
                        onValueChange={(value) => {
                            this.setState({ phonePrefix: value });
                        }}
                        value={this.state.phonePrefix}
                    />
                </View>
                <View style={styles.formRow}>
                    <Input 
                        value={this.state.phone} 
                        onInputChange={this.updateLoginField} 
                        field="phone"
                        placeholder="Teléfono móvil"
                    />
                </View>
                <View style={styles.formRow}>
                    {formError != '' && <Text style={styles.formErrorText}>{formError}</Text>}
                </View>
                <View style={styles.formRow}>
                    {loading
                        ? <ActivityIndicator size="small" color={Constants.colors.pink}/>
                        : <Button 
                            type="pink"
                            onButtonPress={this.enter} 
                            text={'ACCEDER'} 
                          />
                    }
                </View>
                <View style={styles.formRow}>
                    <TouchableOpacity onPress={this.goToRegister}>
                        <Text style={styles.bottomLinkText}>¿No te has registrado aún? <Text style={styles.bottomLinkTextPink}>Pulsa aquí</Text></Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.bottomLinkText}>O si lo prefieres</Text>
                </View>
                <View style={styles.formRowLast}>
                    <FacebookButton
                        onLoginSucess={()=>{ 
                            console.log('​Login Facebook');
                            navigation.pop();
                        }}
                    />
                </View>
                <ConfirmPhone 
                    showModal={showModal} 
                    confirmationResult={confirmationResult}
                    onCloseModal={this.onCloseConfirmModal}
                />
                <CodeAutoconfirmed
                    showModal={showModalAutoconf} 
                    onCloseModal={this.onCloseAutoconfModal}
                />
            </KeyboardAwareScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        order   : state.orderReducer.order,
        userData: state.userDataReducer.userData,
    }
}
  
export default connect(mapStateToProps)(LoginScreen);

const styles = StyleSheet.create({
    mainView:{
        backgroundColor: '#fff',
        justifyContent: 'flex-end',
        height:hp(100)
    },
    bgImage: {
        resizeMode: 'stretch',
        position: 'absolute',
        top: 0,
        width: '100%',
        height: '60%'
    },
    formRow:{
        marginBottom: hp(3)
    },
    formRowLast:{
        marginBottom: hp(8)
    },
    bottomLinkText:{
        fontSize:hp(Constants.fontSize.l),
        color: Constants.colors.darkGray,
        textAlign:'center'
    },
    bottomLinkTextPink:{
        color : Constants.colors.pink
    },
    loginMessage:{
        paddingVertical: hp(3),
        paddingHorizontal: wp(10)
    },
    loginMessageText:{
        fontSize: hp(Constants.fontSize.l),
        fontWeight: '300',
        textAlign: 'center'
    },
    formErrorText:{
        color: Constants.colors.pink,
        textAlign:'center'
    }
});