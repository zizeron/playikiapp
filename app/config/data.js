const months = [
    {value:'', label:"Mes"},
    {value:"01", label:"Enero"},
    {value:"02", label:"Febrero"},
    {value:"03", label:"Marzo"},
    {value:"04", label:"Abril"},
    {value:"05", label:"Mayo"},
    {value:"06", label:"Junio"},
    {value:"07", label:"Julio"},
    {value:"08", label:"Agosto"},
    {value:"09", label:"Septiembre"},
    {value:"10", label:"Octubre"},
    {value:"11", label:"Noviembre"},
    {value:"12", label:"Diciembre"},
]

let years = [{value:'', label:"Año"},];
const thisYear = (new Date()).getFullYear();
for(let i=0; i<10 ; i++){
    years.push({value: String(thisYear+i), label: String(thisYear+i)});
}

export {
    months,
    years
}