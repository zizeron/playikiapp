const Settings = {
    locationDriftThreshold: 0.0003,
    mapDelta: 0.001,
    mapDefaulLatitude: 41.2693596,
    mapDefaulLongitude: 1.9345297,
    orderMaxTimeAvailable: 10 * 60000, //5 mins
}

export default Settings;