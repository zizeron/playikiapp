const Constants = {
    colors: {
        pink       : '#E7236C',
        lightGray  : '#F2F2F2',
        gray       : '#EEF3F6',
        textGray   : '#9B9B9B',
        midGray    : '#4C515B',
        darkGray   : '#1F2532',
        white      : '#FFFFFF',
        lightBlue  : '#AFE0F5',
        lightRed   : '#F9BCC3',
        fbBlue     : '#4472fd',
        red        : '#D0021B',
        orderGreen : '#417505',
        orderPink  : '#E7236C',
        orderRed   : '#D0021B',
        orderGray  : '#657075',
        yellow     : '#F5A623'
    },
    colorsRGB:{
        gray : 'rgba(238,243,246,0.9)',
        red : 'rgba(255,0,0,0.2)',
        darkGray : 'rgba(74,74,74,0.7)',
    },
    fontSize:{
        xxl: 4.5,//5    .5
        xll: 3.5,//4    .5
        xlm: 3,  // new
        xl: 2.5, //3    .5
        ll: 2.3, //2.8  .5
        l: 2,    //2.5  .3
        ml: 1.7, //2.2  .5
        m: 1.5,  //2    .5
        s: 1.8,
        xs: 1.3  //1.5  .2
    },
    lineHeight: {
        xxl: 4.7,
        xll: 3.7,
        xlm: 3.2,  
        xl: 2.7, 
        ll: 3.3, 
        l: 2.2,    
        ml: 1.9, 
        m: 1.7,  
        s: 2.1,
        xs: 1.5  
    }
}

export default Constants;

/*
    fontSize:{
        xxl: 5,
        xll: 4,
        xl: 3,
        ll: 2.8,
        l: 2.5,
        ml: 2.2,
        m: 2,
        s: 1.8,
        xs: 1.5
    }
 */