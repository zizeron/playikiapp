import { createStackNavigator } from 'react-navigation';
import { heightPercentage as hp } from '../services/ScreenDimensions';
import Constants from '../config/styles';

import HomeScreen from '../screens/Home';
import SignupScreen from '../screens/Signup';
import LoginScreen from '../screens/Login';
import WelcomeScreen from '../screens/Welcome';
import SetLocationScreen from '../screens/SetLocation';
import ProductsScreen from '../screens/Products';
import MenusScreen from '../screens/Menus';
import CategoryScreen from '../screens/Category';
import OrderScreen from '../screens/Order';

import PaymentScreen from '../screens/Payment';

import MenuDetailScreen from '../screens/MenuDetail';
import ProductDetailScreen from '../screens/ProductDetail';

import ConfirmPositionScreen from '../screens/ConfirmPosition';
import BeachNotAvailableScreen from '../screens/BeachNotAvailable';
import PaymentConfirmScreen from '../screens/PaymentConfirm';
import FollowOrderScreen from '../screens/FollowOrder'

import OrderListScreen from '../screens/OrderList';
import ViewOrderScreen from '../screens/ViewOrder';
import NotificationsScreen from '../screens/Notifications';

import ConfigurationScreen from '../screens/Configuration';

import PrivacidadScreen from '../screens/Privacidad';
import CondicionesScreen from '../screens/Condiciones';

import FaqsScreen from '../screens/Faqs';

const MainStack = createStackNavigator({
    Home: {
      screen: HomeScreen
    },
    Welcome: {
        screen: WelcomeScreen
    },
    SetLocation: {
        screen: SetLocationScreen
    },
    Products: {
        screen: ProductsScreen
    },
    Menus: {
      screen: MenusScreen
    },
    Category: {
      screen: CategoryScreen
    },
    Order:{
      screen: OrderScreen
    },
    MenuDetail: {
      screen: MenuDetailScreen
    },
    ProductDetail: {
      screen: ProductDetailScreen
    },
    Payment: {
      screen: PaymentScreen
    },
    PaymentConfirm:{
      screen: PaymentConfirmScreen
    },
    FollowOrder:{
      screen: FollowOrderScreen
    },
    OrderList:{
      screen: OrderListScreen
    },
    Notifications: {
      screen: NotificationsScreen
    },
    Privacidad: {
      screen: PrivacidadScreen
    },
    Condiciones:{
      screen: CondicionesScreen
    },
    Configuration:{
      screen: ConfigurationScreen
    },
    ViewOrder:{
      screen: ViewOrderScreen
    },
    Faqs:{
      screen: FaqsScreen
    },
    BeachNotAvailable:{
      screen: BeachNotAvailableScreen
    }
  },{
    initialRouteName: 'Home',
    navigationOptions: {
        headerTransparent: true,
        headerStyle:{
            borderBottomWidth: 0
        },
        gesturesEnabled: false,
        headerBackTitle: null,
        headerTitleStyle: {
          fontSize: hp(Constants.fontSize.l),
          fontWeight: '300',
        },
    }
  }
);


const LoginModals = createStackNavigator(
  {
    Login: {
        screen: LoginScreen
    },
    Signup: {
      screen: SignupScreen
    }
  }
);

export default RootStack = createStackNavigator(
    {
      Main: {
        screen: MainStack,
      },
      ConfirmPosition: {
        screen: ConfirmPositionScreen,
      },
      LoginStack: {
        screen: LoginModals
      }
    },
    {
      mode: 'modal',
      headerMode: 'none',
    }
  );