import { combineReducers } from 'redux';
 
let userDataState = { userData: {}, loading:true };
 
const userDataReducer = (state = userDataState, action) => {
    switch (action.type) {
        case 'SET_USER_DATA':
            return Object.assign({}, state, { userData: action.userData, loading:false });

        default:
            return state;
    }
};

const disableMenuReducer = (state = {menuDisabled: true}, action) => {
    switch (action.type) {
        case 'DISABLE_MENU_GESTURE':
            return Object.assign({}, state, { menuDisabled: action.menuDisabled });

        default:
            return state;
    }
};

const toggleMenuReducer = (state = {menuOpen: false}, action) => {
    switch (action.type) {
        case 'SET_MENU_STATE':
            return Object.assign({}, state, { menuOpen: action.menuOpen });

        default:
            return state;
    }
};

const orderReducer = (state = { order: null }, action) => {
    switch (action.type) {
        case 'SET_ORDER':
            return Object.assign({}, state, {order: action.order});

        default:
            return state;
    }
}
 
const orderFollowingReducer = (state = { order: null }, action) => {
    switch (action.type) {
        case 'SET_ORDER_FOLLOWING':
            return Object.assign({}, state, {order: action.order});

        default:
            return state;
    }
}
 

const settingsReducer = (state = { redirectAfterlogin: true} , action) => {
    switch (action.type) {
        case 'SET_SETTINGS':
            return Object.assign({}, state, action.settings);

        default:
            return state;
    }
}

// Combine all the reducers
const rootReducer = combineReducers({
    userDataReducer,
    disableMenuReducer,
    toggleMenuReducer,
    orderReducer,
    orderFollowingReducer,
    settingsReducer
})
 
export default rootReducer;